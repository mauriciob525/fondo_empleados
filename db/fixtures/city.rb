City.seed do |s|
  s.id = 1
  s.name = 'Aguachica'
  s.country_id = 40
  s.departament_id = 12
end
City.seed do |s|
  s.id = 2
  s.name = 'Apartado'
  s.country_id = 40
  s.departament_id = 2 
end
City.seed do |s|
  s.id = 3
  s.name = 'Armenia'
  s.country_id = 40
  s.departament_id = 25
end
City.seed do |s|
  s.id = 4
  s.name = 'Barrancabermeja'
  s.country_id = 40
  s.departament_id = 28 
end
City.seed do |s|
  s.id = 5
  s.name = 'Barranquilla'
  s.country_id = 40
  s.departament_id = 4 
end
City.seed do |s|
  s.id = 6
  s.name = 'Bello'
  s.country_id = 40
  s.departament_id = 2 
end
City.seed do |s|
  s.id = 7
  s.name = 'Bogotá'
  s.country_id = 40
  s.departament_id = 5 
end
City.seed do |s|
  s.id = 8
  s.name = 'Bucaramanga'
  s.country_id = 40
  s.departament_id = 28 
end
City.seed do |s|
  s.id = 9
  s.name = 'Buenaventura'
  s.country_id = 40
  s.departament_id = 31 
end
City.seed do |s|
  s.id = 10
  s.name = 'Buga'
  s.country_id = 40
  s.departament_id = 11 
end
City.seed do |s|
  s.id = 11
  s.name = 'Cali'
  s.country_id = 40
  s.departament_id = 31 
end
City.seed do |s|
  s.id = 12
  s.name = 'Cartago'
  s.country_id = 40
  s.departament_id = 31 
end
City.seed do |s|
  s.id = 13
  s.name = 'Cartagena de Indias'
  s.country_id = 40
  s.departament_id = 6 
end
City.seed do |s|
  s.id = 14
  s.name = 'Caucasia'
  s.country_id = 40
  s.departament_id = 2 
end
City.seed do |s|
  s.id = 15
  s.name = 'Cereté'
  s.country_id = 40 
  s.departament_id = 14 
end
City.seed do |s|
  s.id = 16
  s.name = 'Chia'
  s.country_id = 40 
  s.departament_id = 15 
end
City.seed do |s|
  s.id = 17
  s.name = 'Ciénaga'
  s.country_id = 40
  s.departament_id = 20 
end
City.seed do |s|
  s.id = 18
  s.name = 'Cúcuta'
  s.country_id = 40 
  s.departament_id = 23 
end
City.seed do |s|
  s.id = 19
  s.name = 'Dosquebradas'
  s.country_id = 40
  s.departament_id = 26 
end
City.seed do |s|
  s.id = 20
  s.name = 'Duitama'
  s.country_id = 40
  s.departament_id = 7 
end
City.seed do |s|
  s.id = 21
  s.name = 'Envigado'
  s.country_id = 40
  s.departament_id = 2 
end
City.seed do |s|
  s.id = 22
  s.name = 'Facatativá'
  s.country_id = 40
  s.departament_id = 15 
end
City.seed do |s|
  s.id = 23
  s.name = 'Florencia'
  s.country_id = 40
  s.departament_id = 9 
end
City.seed do |s|
  s.id = 24
  s.name = 'Floridablanca'
  s.country_id = 40
  s.departament_id = 28 
end
City.seed do |s|
  s.id = 25
  s.name = 'Fusagasugá'
  s.country_id = 40
  s.departament_id = 15 
end
City.seed do |s|
  s.id = 26
  s.name = 'Girardot'
  s.country_id = 40
  s.departament_id = 15 
end
City.seed do |s|
  s.id = 27
  s.name = 'Giron'
  s.country_id = 40
  s.departament_id = 28 
end
City.seed do |s|
  s.id = 28
  s.name = 'Ibagué'
  s.country_id = 40
  s.departament_id = 30 
end
City.seed do |s|
  s.id = 29
  s.name = 'Ipiales'
  s.country_id = 40
  s.departament_id = 22 
end
City.seed do |s|
  s.id = 30
  s.name = 'Itagüí'
  s.country_id = 40
  s.departament_id = 2 
end
City.seed do |s|
  s.id = 31
  s.name = 'Jamundí'
  s.country_id = 40
  s.departament_id = 31 
end
City.seed do |s|
  s.id = 32
  s.name = 'Lorica'
  s.country_id = 40 
  s.departament_id = 14 
end
City.seed do |s|
  s.id = 33
  s.name = 'Los patios'
  s.country_id = 40
  s.departament_id = 23 
end
City.seed do |s|
  s.id = 34
  s.name = 'Magangué'
  s.country_id = 40 
  s.departament_id = 6 
end
City.seed do |s|
  s.id = 35
  s.name = 'Maicaio'
  s.country_id = 40
  s.departament_id = 19 
end
City.seed do |s|
  s.id = 36
  s.name = 'Malambo'
  s.country_id = 40 
  s.departament_id = 4 
end
City.seed do |s|
  s.id = 37
  s.name = 'Manizales'
  s.country_id = 40
  s.departament_id = 8 
end
City.seed do |s|
  s.id = 38
  s.name = 'Medellín'
  s.country_id = 40
  s.departament_id = 2 
end
City.seed do |s|
  s.id = 39
  s.name = 'Melgar'
  s.country_id = 40
  s.departament_id = 30 
end
City.seed do |s|
  s.id = 40
  s.name = 'Montería'
  s.country_id = 40
  s.departament_id = 14 
end
City.seed do |s|
  s.id = 41
  s.name = 'Neiva'
  s.country_id = 40
  s.departament_id = 18 
end
City.seed do |s|
  s.id = 42
  s.name = 'Ocaña'
  s.country_id = 40
  s.departament_id = 23 
end
City.seed do |s|
  s.id = 43
  s.name = 'Paipa'
  s.country_id = 40
  s.departament_id = 7 
end
City.seed do |s|
  s.id = 44
  s.name = 'Palmira'
  s.country_id = 40
  s.departament_id = 31 
end
City.seed do |s|
  s.id = 45
  s.name = 'Pamplona'
  s.country_id = 40
  s.departament_id = 23 
end
City.seed do |s|
  s.id = 46
  s.name = 'Pasto'
  s.country_id = 40
  s.departament_id = 22 
end
City.seed do |s|
  s.id = 47
  s.name = 'Pereira'
  s.country_id = 40
  s.departament_id = 26 
end
City.seed do |s|
  s.id = 48
  s.name = 'Piedecuesta'
  s.country_id = 40
  s.departament_id = 28 
end
City.seed do |s|
  s.id = 49
  s.name = 'Pitalito'
  s.country_id = 40
  s.departament_id = 18 
end
City.seed do |s|
  s.id = 50
  s.name = 'Popayán'
  s.country_id = 40
  s.departament_id = 11 
end
City.seed do |s|
  s.id = 51
  s.name = 'Quibdo'
  s.country_id = 40
  s.departament_id = 13 
end
City.seed do |s|
  s.id = 52
  s.name = 'Riohacha'
  s.country_id = 40
  s.departament_id = 19 
end
City.seed do |s|
  s.id = 53
  s.name = 'Rionegro'
  s.country_id = 40
  s.departament_id = 2 
end
City.seed do |s|
  s.id = 54
  s.name = 'Sabanalarga'
  s.country_id = 40
  s.departament_id = 4 
end
City.seed do |s|
  s.id = 55
  s.name = 'Sahagún'
  s.country_id = 40
  s.departament_id = 14 
end
City.seed do |s|
  s.id = 56
  s.name = 'San Andrés'
  s.country_id = 40
  s.departament_id = 27 
end
City.seed do |s|
  s.id = 57
  s.name = 'Santa Marta'
  s.country_id = 40
  s.departament_id = 20 
end
City.seed do |s|
  s.id = 58
  s.name = 'Sincelejo'
  s.country_id = 40
  s.departament_id = 29 
end
City.seed do |s|
  s.id = 59
  s.name = 'Soacha'
  s.country_id = 40
  s.departament_id = 15 
end
City.seed do |s|
  s.id = 60
  s.name = 'Sogamoso'
  s.country_id = 40
  s.departament_id = 7 
end
City.seed do |s|
  s.id = 61
  s.name = 'Soledad'
  s.country_id = 40
  s.departament_id = 4 
end
City.seed do |s|
  s.id = 62
  s.name = 'Tibú'
  s.country_id = 40
  s.departament_id = 23 
end
City.seed do |s|
  s.id = 63
  s.name = 'Tuluá'
  s.country_id = 40
  s.departament_id = 31 
end
City.seed do |s|
  s.id = 64
  s.name = 'Tumaco'
  s.country_id = 40
  s.departament_id = 22 
end
City.seed do |s|
  s.id = 65
  s.name = 'Tunja'
  s.country_id = 40
  s.departament_id = 7 
end
City.seed do |s|
  s.id = 66
  s.name = 'Turbo'
  s.country_id = 40
  s.departament_id = 2 
end
City.seed do |s|
  s.id = 67
  s.name = 'Valledupar'
  s.country_id = 40 
  s.departament_id = 12 
end
City.seed do |s|
  s.id = 68
  s.name = 'Villa de leyva'
  s.country_id = 40 
  s.departament_id = 7 
end
City.seed do |s|
  s.id = 69
  s.name = 'Villa del Rosario'
  s.country_id = 40 
  s.departament_id = 23 
end
City.seed do |s|
  s.id = 70
  s.name = 'Villavicencio'
  s.country_id = 40
  s.departament_id = 21 
end
City.seed do |s|
  s.id = 71
  s.name = 'Yopal'
  s.country_id = 40 
  s.departament_id = 10 
end
City.seed do |s|
  s.id = 72
  s.name = 'Yumbo'
  s.country_id = 40 
  s.departament_id = 31 
end
City.seed do |s|
  s.id = 73
  s.name = 'Zipaquirá'
  s.country_id = 40
  s.departament_id = 15 
end
City.seed do |s|
  s.id = 74
  s.name = 'Arauca'
  s.country_id = 40
  s.departament_id = 3
end
City.seed do |s|
  s.id = 75
  s.name = 'Tokio'
  s.country_id = 89
  s.departament_id = nil
end
City.seed do |s|
  s.id = 76
  s.name = 'Nueva York'
  s.country_id = 57
  s.departament_id = nil
end
City.seed do |s|
  s.id = 77
  s.name = 'Los Angeles'
  s.country_id = 57
  s.departament_id = nil
end
City.seed do |s|
  s.id = 78
  s.name = 'Seúl'
  s.country_id = 146 
  s.departament_id = nil
end
City.seed do |s|
  s.id = 79
  s.name = 'Londres'
  s.country_id = 139
  s.departament_id = nil
end
City.seed do |s|
  s.id = 80
  s.name = 'Paris'
  s.country_id = 63
  s.departament_id = nil
end
City.seed do |s|
  s.id = 81
  s.name = 'Osaka'
  s.country_id = 89
  s.departament_id = nil
end
City.seed do |s|
  s.id = 82
  s.name = 'Shanghái'
  s.country_id = 38
  s.departament_id = nil
end
City.seed do |s|
  s.id = 83
  s.name = 'Chicago'
  s.country_id = 57
  s.departament_id = nil
end
City.seed do |s|
  s.id = 84
  s.name = 'Moscú'
  s.country_id = 150
  s.departament_id = nil
end
City.seed do |s|
  s.id = 85
  s.name = 'Pekin'
  s.country_id = 38 
  s.departament_id = nil
end
City.seed do |s|
  s.id = 86
  s.name = 'Colonia'
  s.country_id = 3
  s.departament_id = nil
end
City.seed do |s|
  s.id = 87
  s.name = 'Houston'
  s.country_id = 57
  s.departament_id = nil
end
City.seed do |s|
  s.id = 88       
  s.name = 'Washington D. C.'
  s.country_id = 57
  s.departament_id = nil
end
City.seed do |s|
  s.id = 89
  s.name = 'São Paulo'
  s.country_id = 25
  s.departament_id = nil
end
City.seed do |s|
  s.id = 90
  s.name = 'Hong Kong'
  s.country_id = 38
  s.departament_id = nil
end
City.seed do |s|
  s.id = 91
  s.name = 'Dallas'
  s.country_id = 57
  s.departament_id = nil
end
City.seed do |s|
  s.id = 92
  s.name = 'Ciudad de México'
  s.country_id = 177
  s.departament_id = nil
end
City.seed do |s|
  s.id = 93
  s.name = 'Cantón'
  s.country_id = 38
  s.departament_id = nil
end
City.seed do |s|
  s.id = 94
  s.name = 'Singapur'
  s.country_id = 162
  s.departament_id = nil
end
City.seed do |s|
  s.id = 95
  s.name = 'Nagoya'
  s.country_id = 89
  s.departament_id = nil
end
City.seed do |s|
  s.id = 96
  s.name = 'Boston'
  s.country_id = 57
  s.departament_id = nil
end
City.seed do |s|
  s.id = 97
  s.name = 'Estambul'
  s.country_id = 180
  s.departament_id = nil
end
City.seed do |s|
  s.id = 98
  s.name = 'Filadelfia'
  s.country_id = 57
  s.departament_id = nil
end
City.seed do |s|
  s.id = 99
  s.name = 'San Francisco'
  s.country_id = 57
  s.departament_id = nil
end

City.seed do |s|
  s.id = 101
  s.name = 'Yakarta'
  s.country_id = 79
  s.departament_id = nil
end
City.seed do |s|
  s.id = 102
  s.name = 'Ámsterdam'
  s.country_id = 134 
  s.departament_id = nil
end
City.seed do |s|
  s.id = 103
  s.name = 'Buenos Aires'
  s.country_id = 9
  s.departament_id = nil
end
City.seed do |s|
  s.id = 104
  s.name = 'Milán'
  s.country_id = 87
  s.departament_id = nil
end
City.seed do |s|
  s.id = 105
  s.name = 'Bangkok'
  s.country_id = 172
  s.departament_id = nil
end
City.seed do |s|
  s.id = 106
  s.name = 'Busan'
  s.country_id = 146
  s.departament_id = nil
end
City.seed do |s|
  s.id = 107
  s.name = 'Atlanta'
  s.country_id = 57
  s.departament_id = nil
end
City.seed do |s|
  s.id = 108
  s.name = 'Delhi'
  s.country_id = 78
  s.departament_id = nil
end
City.seed do |s|
  s.id = 109
  s.name = 'Toronto'
  s.country_id = 34
  s.departament_id = nil
end
City.seed do |s|
  s.id = 110
  s.name = 'Miami'
  s.country_id = 57
  s.departament_id = nil
end
City.seed do |s|
  s.id = 111
  s.name = 'Fráncfort del Meno'
  s.country_id = 3
  s.departament_id = nil
end
City.seed do |s|
  s.id = 112
  s.name = 'Madrid'
  s.country_id = 56
  s.departament_id = nil
end
City.seed do |s|
  s.id = 113
  s.name = 'Sídney'
  s.country_id = 11
  s.departament_id = nil
end
City.seed do |s|
  s.id = 114
  s.name = 'Múnich'
  s.country_id = 3
  s.departament_id = nil
end
City.seed do |s|
  s.id = 115
  s.name = 'Rio de Janeiro'
  s.country_id = 25
  s.departament_id = nil
end
City.seed do |s|
  s.id = 116
  s.name = 'San Diego'
  s.country_id = 57 
  s.departament_id = nil
end
City.seed do |s|
  s.id =  117
  s.name = 'Viena'
  s.country_id = 12
  s.departament_id = nil
end
City.seed do |s|
  s.id = 118
  s.name = 'Manila'
  s.country_id = 61
  s.departament_id = nil
end
City.seed do |s|
  s.id = 119
  s.name = 'Melbourne'
  s.country_id = 11
  s.departament_id = nil
end
City.seed do |s|
  s.id = 120
  s.name = 'Abu Dabi'
  s.country_id = 52
  s.departament_id = nil
end
City.seed do |s|
  s.id = 121
  s.name = 'Kuala Lumpur'
  s.country_id = 104 
  s.departament_id = nil
end
City.seed do |s|
  s.id = 122
  s.name = 'Santiago de Chile'
  s.country_id = 37
  s.departament_id = nil
end
City.seed do |s|
  s.id = 123
  s.name = 'Barcelona'
  s.country_id = 56
  s.departament_id = nil
end
City.seed do |s|
  s.id = 124
  s.name = 'Kuwait'
  s.country_id = 95
  s.departament_id = nil
end
City.seed do |s|
  s.id = 125
  s.name = 'Riad'
  s.country_id = 7
  s.departament_id = nil
end
City.seed do |s|
  s.id = 126
  s.name = 'Roma'
  s.country_id = 87
  s.departament_id = nil
end
City.seed do |s|
  s.id = 127
  s.name = 'Hamburgo'
  s.country_id = 3
  s.departament_id = nil
end
City.seed do |s|
  s.id = 128
  s.name = 'Berlín'
  s.country_id = 3
  s.departament_id = nil
end
City.seed do |s|
  s.id = 129
  s.name = 'Montreal'
  s.country_id = 34
  s.departament_id = nil
end
City.seed do |s|
  s.id = 130
  s.name = 'Tel Aviv'
  s.country_id = 86
  s.departament_id = nil
end
City.seed do |s|
  s.id = 131
  s.name = 'Bombay'
  s.country_id = 78
  s.departament_id = nil
end
City.seed do |s|
  s.id = 132
  s.name = 'Estocolmo'
  s.country_id = 168
  s.departament_id = nil
end
City.seed do |s|
  s.id = 133
  s.name = 'Brasilia'
  s.country_id = 25
  s.departament_id = nil
end
City.seed do |s|
  s.id = 134
  s.name = 'Caracas'
  s.country_id = 188
  s.departament_id = nil
end
City.seed do |s|
  s.id = 135
  s.name = 'Varsovia'
  s.country_id = 136
  s.departament_id = nil
end
City.seed do |s|
  s.id = 136
  s.name = 'Pittsburgh'
  s.country_id = 57
  s.departament_id = nil
end
City.seed do |s|
  s.id = 137
  s.name = 'Atenas'
  s.country_id = 69
  s.departament_id = nil
end
City.seed do |s|
  s.id = 138
  s.name = 'Monterrey'
  s.country_id = 117
  s.departament_id = nil
end
City.seed do |s|
  s.id = 139
  s.name = 'Birmingham'
  s.country_id = 139
  s.departament_id = nil
end
City.seed do |s|
  s.id = 140
  s.name = 'San Petersburgo'
  s.country_id = 150
  s.departament_id = nil
end
