Country.seed do |s|
	s.id = 1
	s.name = 'Afganistán'
	s.code = 'AF'	
end	
Country.seed do |s|
	s.id = 2
	s.name = 'Albania'
	s.code = 'AL'	
end	
Country.seed do |s|
	s.id = 3
	s.name = 'Alemania'
	s.code = 'DE'	
end
Country.seed do |s|
	s.id = 4
	s.name = 'Andorra'
	s.code = 'AD'	
end
Country.seed do |s|
	s.id = 5
	s.name = 'Angola'
	s.code = 'AO'
end
Country.seed do |s|
	s.id = 6
	s.name = 'Antigua y Barbuda'
	s.code = 'AG'	
end
Country.seed do |s|
	s.id = 7
	s.name = 'Arabia Saudita'
	s.code = 'SA'	
end
Country.seed do |s|
	s.id = 8
	s.name = 'Argelia'
	s.code = 'DZ'	
end
Country.seed do |s|
	s.id = 9
	s.name = 'Argentina'
	s.code = 'AR'	
end
Country.seed do |s|
	s.id = 10
	s.name = 'Armenia'
	s.code = 'AM'	
end
Country.seed do |s|
	s.id = 11
	s.name = 'Australia'
	s.code = 'AU'	
end
Country.seed do |s|
	s.id = 12
	s.name = 'Austria'
	s.code = 'AT'	
end
Country.seed do |s|
	s.id = 13
	s.name = 'Azerbaiyán'
	s.code = 'AZ'	
end
Country.seed do |s|
	s.id = 14
	s.name = 'Bahamas'
	s.code = 'BS'	
end
Country.seed do |s|
	s.id = 15
	s.name = 'Barein'
	s.code = 'BH'	
end
Country.seed do |s|
	s.id = 16
	s.name = 'Bangladesh'
	s.code = 'BD'	
end
Country.seed do |s|
	s.id = 17
	s.name = 'Barbados'
	s.code = 'BB'	
end
Country.seed do |s|
	s.id = 18
	s.name = 'Bielorrusia'
	s.code = 'BY'	
end
Country.seed do |s|
	s.id = 19
	s.name = 'Belice'
	s.code = 'BZ'	
end
Country.seed do |s|
	s.id = 20
	s.name = 'Benin'
	s.code = 'BJ'	
end
Country.seed do |s|
	s.id = 21
	s.name = 'Bután'
	s.code = 'BT'	
end
Country.seed do |s|
	s.id = 22
	s.name = 'Bolivia'
	s.code = 'BO'	
end
Country.seed do |s|
	s.id = 23
	s.name = 'Bosnia'
	s.code = 'BA'	
end
Country.seed do |s|
	s.id = 24
	s.name = 'Botsuana'
	s.code = 'BW'	
end
Country.seed do |s|
	s.id = 25
	s.name = 'Brasil'
	s.code = 'BR'	
end
Country.seed do |s|
	s.id = 26
	s.name = 'Brunei'
	s.code = 'BN'	
end
Country.seed do |s|
	s.id = 27
	s.name = 'Bulgaria'
	s.code = 'BG'	
end
Country.seed do |s|
	s.id = 28
	s.name = 'Burkina Faso'
	s.code = 'BF'	
end
Country.seed do |s|
	s.id = 29
	s.name = 'Burundi'
	s.code = 'BI'	
end
Country.seed do |s|
	s.id = 30
	s.name = 'Bélgica'
	s.code = 'BE'	
end
Country.seed do |s|
	s.id = 31
	s.name = 'Cabo Verde'
	s.code = 'CV'	
end
Country.seed do |s|
	s.id = 32
	s.name = 'Camboya'
	s.code = 'KH'	
end
Country.seed do |s|
	s.id = 33
	s.name = 'Camerún'
	s.code = 'CN'	
end
Country.seed do |s|
	s.id = 34
	s.name = 'Canadá'
	s.code = 'AL'	
end
Country.seed do |s|
	s.id = 35
	s.name = 'Chad'
	s.code = 'TD'	
end
Country.seed do |s|
	s.id = 36
	s.name = 'Checa'
	s.code = 'CZ'	
end
Country.seed do |s|
	s.id = 37
	s.name = 'Chile'
	s.code = 'CL'	
end
Country.seed do |s|
	s.id = 38
	s.name = 'China'
	s.code = 'CN'	
end
Country.seed do |s|
	s.id = 39
	s.name = 'Chipre'
	s.code = 'CY'	
end
Country.seed do |s|
	s.id = 40
	s.name = 'Colombia'
	s.code = 'CO'	
end
Country.seed do |s|
	s.id = 41
	s.name = 'Comoras'
	s.code = 'KM'	
end
Country.seed do |s|
	s.id = 42
	s.name = 'Congo'
	s.code = 'CG'	
end
Country.seed do |s|
	s.id = 43
	s.name = 'Costa Rica'
	s.code = 'CR'	
end
Country.seed do |s|
	s.id = 44
	s.name = 'Croacia'
	s.code = 'HR'	
end
Country.seed do |s|
	s.id = 45
	s.name = 'Cuba'
	s.code = 'CU'	
end
Country.seed do |s|
	s.id = 46
	s.name = 'Costa de Marfil'
	s.code = 'CI'	
end
Country.seed do |s|
	s.id = 47
	s.name = 'Dinamarca'
	s.code = 'DK'	
end
Country.seed do |s|
	s.id = 48
	s.name = 'Dominica'
	s.code = 'DM'	
end
Country.seed do |s|
	s.id = 49
	s.name = 'Ecuador'
	s.code = 'EC'	
end
Country.seed do |s|
	s.id = 50
	s.name = 'Egipto'
	s.code = 'EG'	
end
Country.seed do |s|
	s.id = 51
	s.name = 'El Salvador'
	s.code = 'SV'	
end
Country.seed do |s|
	s.id = 52
	s.name = 'Emiratos Árabes Unidos'
	s.code = 'AE'	
end
Country.seed do |s|
	s.id = 53
	s.name = 'Eritrea'
	s.code = 'ER'	
end
Country.seed do |s|
	s.id = 54
	s.name = 'Eslovaquia'
	s.code = 'SK'	
end
Country.seed do |s|
	s.id = 55
	s.name = 'Eslovenia'
	s.code = 'SI'	
end
Country.seed do |s|
	s.id = 56
	s.name = 'España'
	s.code = 'ES'	
end
Country.seed do |s|
	s.id = 57
	s.name = 'Estados Unidos de América'
	s.code = 'US'	
end
Country.seed do |s|
	s.id = 58
	s.name = 'Estonia'
	s.code = 'EE'	
end
Country.seed do |s|
	s.id = 59
	s.name = 'Etiopía'
	s.code = 'ET'	
end
Country.seed do |s|
	s.id = 60
	s.name = 'Fiji'
	s.code = 'FJ'	
end
Country.seed do |s|
	s.id = 61
	s.name = 'Filipinas'
	s.code = 'PH'	
end
Country.seed do |s|
	s.id = 62
	s.name = 'Finlandia'
	s.code = 'FI'	
end
Country.seed do |s|
	s.id = 63
	s.name = 'Francia'
	s.code = 'FR'	
end
Country.seed do |s|
	s.id = 64
	s.name = 'Gabón'
	s.code = 'GA'	
end
Country.seed do |s|
	s.id = 65
	s.name = 'Gambia'
	s.code = 'GM'	
end
Country.seed do |s|
	s.id = 66
	s.name = 'Georgia'
	s.code = 'GE'	
end
Country.seed do |s|
	s.id = 67
	s.name = 'Ghana'
	s.code = 'GH'	
end
Country.seed do |s|
	s.id = 68
	s.name = 'Granada'
	s.code = 'GD'	
end
Country.seed do |s|
	s.id = 69
	s.name = 'Grecia'
	s.code = 'GR'	
end
Country.seed do |s|
	s.id = 70
	s.name = 'Guatemala'
	s.code = 'GT'	
end
Country.seed do |s|
	s.id = 71
	s.name = 'Guinea'
	s.code = 'GN'	
end
Country.seed do |s|
	s.id = 72
	s.name = 'Guinea Ecuatorial'
	s.code = 'GQ'	
end
Country.seed do |s|
	s.id = 73
	s.name = 'Guinea-Bissau'
	s.code = 'GW'	
end
Country.seed do |s|
	s.id = 74
	s.name = 'Guyana'
	s.code = 'GY'	
end
Country.seed do |s|
	s.id = 75
	s.name = 'Haití'
	s.code = 'HT'	
end
Country.seed do |s|
	s.id = 76
	s.name = 'Honduras'
	s.code = 'HN'	
end
Country.seed do |s|
	s.id = 77
	s.name = 'Hungría'
	s.code = 'HU'	
end
Country.seed do |s|
	s.id = 78
	s.name = 'India'
	s.code = 'IN'	
end
Country.seed do |s|
	s.id = 79
	s.name = 'Indonesia'
	s.code = 'ID'	
end
Country.seed do |s|
	s.id = 80
	s.name = 'Iraq'
	s.code = 'IQ'	
end
Country.seed do |s|
	s.id = 81
	s.name = 'Irlanda'
	s.code = 'IE'	
end
Country.seed do |s|
	s.id = 82
	s.name = 'Irán'
	s.code = 'IR'	
end
Country.seed do |s|
	s.id = 83
	s.name = 'Islandia'
	s.code = 'IS'	
end
Country.seed do |s|
	s.id = 84
	s.name = 'Islas Marshall'
	s.code = 'MH'	
end
Country.seed do |s|
	s.id = 85
	s.name = 'Islas Salomón'
	s.code = 'SB'	
end
Country.seed do |s|
	s.id = 86
	s.name = 'Israel'
	s.code = 'IL'	
end
Country.seed do |s|
	s.id = 87
	s.name = 'Italia'
	s.code = 'IT'	
end
Country.seed do |s|
	s.id = 88
	s.name = 'Jamaica'
	s.code = 'JM'	
end
Country.seed do |s|
	s.id = 89
	s.name = 'Japón'
	s.code = 'JP'	
end
Country.seed do |s|
	s.id = 90
	s.name = 'Jordania'
	s.code = 'JO'	
end
Country.seed do |s|
	s.id = 91
	s.name = 'Kazajistán'
	s.code = 'KZ'	
end
Country.seed do |s|
	s.id = 92
	s.name = 'Kenya'
	s.code = 'KE'	
end
Country.seed do |s|
	s.id = 93
	s.name = 'Kirguistán'
	s.code = 'KG'	
end
Country.seed do |s|
	s.id = 94
	s.name = 'Kiribati'
	s.code = 'KI'	
end
Country.seed do |s|
	s.id = 95
	s.name = 'Kuwait'
	s.code = 'KI'	
end
Country.seed do |s|
	s.id = 96
	s.name = 'Lesoto'
	s.code = 'LS'	
end
Country.seed do |s|
	s.id = 97
	s.name = 'Letonia'
	s.code = 'LV'	
end
Country.seed do |s|
	s.id = 98
	s.name = 'Liberia'
	s.code = 'LR'	
end
Country.seed do |s|
	s.id = 99
	s.name = 'Libia'
	s.code = 'LY'	
end
Country.seed do |s|
	s.id = 100
	s.name = 'Lituania'
	s.code = 'LT'	
end
Country.seed do |s|
	s.id = 101
	s.name = 'Luxemburgo'
	s.code = 'LU'
end
Country.seed do |s|
	s.id = 102
	s.name = 'Líbano'
	s.code = 'LB'	
end
Country.seed do |s|
	s.id = 103
	s.name = 'Madagascar'
	s.code = 'MG'	
end
Country.seed do |s|
	s.id = 104
	s.name = 'Malasia'
	s.code = 'MY'	
end
Country.seed do |s|
	s.id = 105
	s.name = 'Malawi'
	s.code = 'MV'	
end
Country.seed do |s|
	s.id = 106
	s.name = 'Maldivas'
	s.code = 'MV'	
end
Country.seed do |s|
	s.id = 107
	s.name = 'Malta'
	s.code = 'MT'	
end
Country.seed do |s|
	s.id = 108
	s.name = 'Malí'
	s.code = 'ML'	
end
Country.seed do |s|
	s.id = 109
	s.name = 'Marruecos'
	s.code = 'MA'	
end
Country.seed do |s|
	s.id = 110
	s.name = 'Mauricio'
	s.code = 'MU'	
end
Country.seed do |s|
	s.id = 111
	s.name = 'Mauritania'
	s.code = 'MR'	
end
Country.seed do |s|
	s.id = 112
	s.name = 'Micronesia'
	s.code = 'FM'	
end
Country.seed do |s|
	s.id = 113
	s.name = 'Mongolia'
	s.code = 'MN'	
end
Country.seed do |s|
	s.id = 114
	s.name = 'Montenegro'
	s.code = 'ME'	
end
Country.seed do |s|
	s.id = 115
	s.name = 'Mozambique'
	s.code = 'MZ'	
end
Country.seed do |s|
	s.id = 116
	s.name = 'Birmania'
	s.code = 'MM'	
end
Country.seed do |s|
	s.id = 117
	s.name = 'México'
	s.code = 'MX'	
end
Country.seed do |s|
	s.id = 118
	s.name = 'Mónaco'
	s.code = 'MC'	
end
Country.seed do |s|
	s.id = 119
	s.name = 'Namibia'
	s.code = 'NA'	
end
Country.seed do |s|
	s.id = 120
	s.name = 'Nauru'
	s.code = 'NR'	
end
Country.seed do |s|
	s.id = 121
	s.name = 'Nepal'
	s.code = 'NP'	
end
Country.seed do |s|
	s.id = 122
	s.name = 'Nicaragua'
	s.code = 'NI'	
end
Country.seed do |s|
	s.id = 123
	s.name = 'Nigeria'
	s.code = 'NG'	
end
Country.seed do |s|
	s.id = 124
	s.name = 'Niue'
	s.code = 'NU'	
end
Country.seed do |s|
	s.id = 125
	s.name = 'Noruega'
	s.code = 'NO'	
end
Country.seed do |s|
	s.id = 126
	s.name = 'Nueva Zelanda'
	s.code = 'NZ'	
end
Country.seed do |s|
	s.id = 127
	s.name = 'Níger'
	s.code = 'NE'	
end
Country.seed do |s|
	s.id = 128
	s.name = 'Omán'
	s.code = 'OM'	
end
Country.seed do |s|
	s.id = 129
	s.name = 'Pakistán'
	s.code = 'PK'	
end
Country.seed do |s|
	s.id = 130
	s.name = 'Palaos'
	s.code = 'PW'	
end
Country.seed do |s|
	s.id = 131
	s.name = 'Panamá'
	s.code = 'PA'	
end
Country.seed do |s|
	s.id = 132
	s.name = 'Papua Nueva Guinea'
	s.code = 'PG'	
end
Country.seed do |s|
	s.id = 133
	s.name = 'Paraguay'
	s.code = 'PY'	
end
Country.seed do |s|
	s.id = 134
	s.name = 'Países Bajos'
	s.code = 'NL'	
end
Country.seed do |s|
	s.id = 135
	s.name = 'Perú'
	s.code = 'PE'	
end
Country.seed do |s|
	s.id = 136
	s.name = 'Polonia'
	s.code = 'PL'	
end
Country.seed do |s|
	s.id = 137
	s.name = 'Portugal'
	s.code = 'PT'	
end
Country.seed do |s|
	s.id = 138
	s.name = 'Qatar'
	s.code = 'QA'	
end
Country.seed do |s|
	s.id = 139
	s.name = 'Reino Unido'
	s.code = 'GB'	
end
Country.seed do |s|
	s.id = 140
	s.name = 'República Centroafricana'
	s.code = 'CF'	
end
Country.seed do |s|
	s.id = 141
	s.name = 'República Democrática Popular Lao'
	s.code = 'LA'	
end
Country.seed do |s|
	s.id = 142
	s.name = 'República Democrática del Congo'
	s.code = 'CD'	
end
Country.seed do |s|
	s.id = 143
	s.name = 'República Dominicana'
	s.code = 'DO'	
end
Country.seed do |s|
	s.id = 144
	s.name = 'República Popular Democrática de Corea del Norte'
	s.code = 'KP'	
end
Country.seed do |s|
	s.id = 145
	s.name = 'República Unida de Tanzanía'
	s.code = 'TZ'	
end
Country.seed do |s|
	s.id = 146
	s.name = 'República de Corea del sur'
	s.code = 'KR'	
end
Country.seed do |s|
	s.id = 147
	s.name = 'República de Moldavia'
	s.code = 'MD'	
end
Country.seed do |s|
	s.id = 148
	s.name = 'República Árabe Siria'
	s.code = 'SY'	
end
Country.seed do |s|
	s.id = 149
	s.name = 'Rumania'
	s.code = 'RO'	
end
Country.seed do |s|
	s.id = 150
	s.name = 'Rusia'
	s.code = 'RU'	
end
Country.seed do |s|
	s.id = 151
	s.name = 'Ruanda'
	s.code = 'RW'	
end
Country.seed do |s|
	s.id = 152
	s.name = 'San Cristóbal y Nieves'
	s.code = 'KN'	
end
Country.seed do |s|
	s.id = 153
	s.name = 'Samoa'
	s.code = 'WS'	
end
Country.seed do |s|
	s.id = 154
	s.name = 'San Marino'
	s.code = 'SM'	
end
Country.seed do |s|
	s.id = 155
	s.name = 'San Vicente y las Granadinas'
	s.code = 'VC'	
end
Country.seed do |s|
	s.id = 156
	s.name = 'Santa Lucía'
	s.code = 'LC'	
end
Country.seed do |s|
	s.id = 157
	s.name = 'Santo Tomé y Príncipe'
	s.code = 'ST'	
end
Country.seed do |s|
	s.id = 158
	s.name = 'Senegal'
	s.code = 'SN'	
end
Country.seed do |s|
	s.id = 159
	s.name = 'Serbia'
	s.code = 'RS'	
end
Country.seed do |s|
	s.id = 160
	s.name = 'Seychelles'
	s.code = 'SC'	
end
Country.seed do |s|
	s.id = 161
	s.name = 'Sierra Leona'
	s.code = 'SL'	
end
Country.seed do |s|
	s.id = 162
	s.name = 'Singapur'
	s.code = 'SG'	
end
Country.seed do |s|
	s.id = 163
	s.name = 'Somalia'
	s.code = 'SO'	
end
Country.seed do |s|
	s.id = 164
	s.name = 'Sri Lanka'
	s.code = 'LK'	
end
Country.seed do |s|
	s.id = 165
	s.name = 'Sudáfrica'
	s.code = 'ZA'	
end
Country.seed do |s|
	s.id = 166
	s.name = 'Sudán'
	s.code = 'SD'	
end
Country.seed do |s|
	s.id = 167
	s.name = 'Sudán del Sur'
	s.code = 'SS'	
end
Country.seed do |s|
	s.id = 168
	s.name = 'Suecia'
	s.code = 'SE'	
end
Country.seed do |s|
	s.id = 169
	s.name = 'Suiza'
	s.code = 'CH'	
end
Country.seed do |s|
	s.id = 170
	s.name = 'Surinam'
	s.code = 'SR'	
end
Country.seed do |s|
	s.id = 171
	s.name = 'Suazilandia'
	s.code = 'SZ'	
end
Country.seed do |s|
	s.id = 172
	s.name = 'Tailandia'
	s.code = 'TH'	
end
Country.seed do |s|
	s.id = 173
	s.name = 'Tayikistán'
	s.code = 'TJ'	
end
Country.seed do |s|
	s.id = 174
	s.name = 'Timor Oriental'
	s.code = 'TL'	
end
Country.seed do |s|
	s.id = 175
	s.name = 'Togo'
	s.code = 'TG'	
end
Country.seed do |s|
	s.id = 176
	s.name = 'Tokelau'
	s.code = 'TK'	
end
Country.seed do |s|
	s.id = 177
	s.name = 'Tonga'
	s.code = 'TO'	
end
Country.seed do |s|
	s.id = 178
	s.name = 'Trinidad y Tabago'
	s.code = 'TT'	
end
Country.seed do |s|
	s.id = 179
	s.name = 'Turkmenistán'
	s.code = 'TM'	
end
Country.seed do |s|
	s.id = 180
	s.name = 'Turquía'
	s.code = 'TR'	
end
Country.seed do |s|
	s.id = 181
	s.name = 'Tuvalu'
	s.code = 'TV'	
end
Country.seed do |s|
	s.id = 182
	s.name = 'Túnez'
	s.code = 'TN'	
end
Country.seed do |s|
	s.id = 183
	s.name = 'Ucrania'
	s.code = 'UA'	
end
Country.seed do |s|
	s.id = 184
	s.name = 'Uganda'
	s.code = 'UG'	
end
Country.seed do |s|
	s.id = 185
	s.name = 'Uruguay'
	s.code = 'UY'	
end
Country.seed do |s|
	s.id = 186
	s.name = 'Uzbekistán'
	s.code = 'UZ'	
end
Country.seed do |s|
	s.id = 187
	s.name = 'Vanuatu'
	s.code = 'VU'	
end
Country.seed do |s|
	s.id = 188
	s.name = 'Venezuela'
	s.code = 'VE'	
end
Country.seed do |s|
	s.id = 189
	s.name = 'Vietnam'
	s.code = 'VN'	
end
Country.seed do |s|
	s.id = 190
	s.name = 'Yemen'
	s.code = 'YE'	
end
Country.seed do |s|
	s.id = 191
	s.name = 'Yibuti'
	s.code = 'DJ'	
end
Country.seed do |s|
	s.id = 192
	s.name = 'Zambia'
	s.code = 'ZM'	
end
Country.seed do |s|
	s.id = 193
	s.name = 'Zimbabue'
	s.code = 'ZW'	
end
Country.seed do |s|
	s.id = 194
	s.name = 'Puerto Rico'
	s.code = 'PR'	
end
Country.seed do |s|
	s.id = 195
	s.name = 'Taiwán'
	s.code = 'TW'	
end