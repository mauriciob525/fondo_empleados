Document.seed do |s|
  s.id = 1
  s.name = 'Autorización Central de Riesgos'
end
Document.seed do |s|
  s.id = 2
  s.name = 'Autorización para el Tratamiento de Datos Personales'
end
Document.seed do |s|
  s.id = 3
  s.name = 'Exención GMF'
end
Document.seed do |s|
  s.id = 5
  s.name = 'Asegurabilidad seguro de vida deudores'
end
Document.seed do |s|
  s.id = 6
  s.name = 'Solicitud y Autorización de Giro a Terceros'
end
Document.seed do |s|
  s.id = 7
  s.name = 'Formato Balance'
end
Document.seed do |s|
  s.id = 8
  s.name = 'Formato para Crédito Ordinario'
end
Document.seed do |s|
  s.id = 9
  s.name = 'Formato para Crédito Extraordinario'
end