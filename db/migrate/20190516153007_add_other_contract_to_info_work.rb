class AddOtherContractToInfoWork < ActiveRecord::Migration[5.2]
  def change
    add_column :info_works, :other_contract, :text
  end
end
