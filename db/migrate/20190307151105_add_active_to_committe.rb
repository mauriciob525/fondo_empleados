class AddActiveToCommitte < ActiveRecord::Migration[5.2]
  def change
    add_column :committes, :active, :boolean, :default => 1
  end
end
