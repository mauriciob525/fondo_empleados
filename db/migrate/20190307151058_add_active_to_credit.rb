class AddActiveToCredit < ActiveRecord::Migration[5.2]
  def change
    add_column :credits, :active, :boolean, :default => 1
  end
end
