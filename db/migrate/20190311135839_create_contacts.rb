class CreateContacts < ActiveRecord::Migration[5.2]
  def change
    create_table :contacts do |t|
      t.string :title
      t.string :email
      t.string :telephone
      t.boolean :active, :default => 1
      t.timestamps
    end
  end
end
