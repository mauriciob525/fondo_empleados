class AddTypeOccupationToFamily < ActiveRecord::Migration[5.2]
  def change
    add_reference :families, :type_occupation, foreign_key: true
  end
end