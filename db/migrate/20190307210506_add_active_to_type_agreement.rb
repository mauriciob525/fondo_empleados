class AddActiveToTypeAgreement < ActiveRecord::Migration[5.2]
  def change
    add_column :type_agreements, :active, :boolean, :default => 1
  end
end
