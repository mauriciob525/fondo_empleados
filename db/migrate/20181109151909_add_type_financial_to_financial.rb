class AddTypeFinancialToFinancial < ActiveRecord::Migration[5.2]
  def change
    add_reference :financials, :type_financial, foreign_key: true
  end
end