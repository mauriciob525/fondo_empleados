class CreateDataAssociateds < ActiveRecord::Migration[5.2]
  def change
    create_table :data_associateds do |t|
      t.date :date_filling
      t.text :name
      t.text :lastname
      t.date :birthdate
      t.text :email
      t.text :education
      t.text :profession
      t.text :address
      t.string :phone
      t.string :mobile
      t.boolean :discount
      t.boolean :source

      t.timestamps
    end
  end
end
