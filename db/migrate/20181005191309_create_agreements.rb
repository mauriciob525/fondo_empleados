class CreateAgreements < ActiveRecord::Migration[5.2]
  def change
    create_table :agreements do |t|
      t.string :title
      t.references :type_agreement, foreign_key: {on_delete: :cascade, on_update: :cascade}
      t.string :short_description
      t.text :description
      t.integer :discount
      t.string :image

      t.timestamps
    end
  end
end
