class AddImageToTypeContent < ActiveRecord::Migration[5.2]
  def change
    add_column :type_contents, :image, :string
  end
end
