class CreateTypeFiles < ActiveRecord::Migration[5.2]
  def change
    create_table :type_files do |t|
      t.string :name

      t.timestamps
    end
  end
end
