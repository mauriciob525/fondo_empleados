class CreateInfoWorks < ActiveRecord::Migration[5.2]
  def change
    create_table :info_works do |t|
      t.date :date_start
      t.text :economic_act
      t.string :cod_ciiu
      t.text :company
      t.string :area_of
      t.string :cod_area
      t.text :region
      t.string :phone_laboral
      t.string :ext
      t.text :position
      t.text :email_laboral
      t.text :salary
      t.string :num
      t.text :bank

      t.timestamps
    end
  end
end
