class AddActiveToAgreement < ActiveRecord::Migration[5.2]
  def change
    add_column :agreements, :active, :boolean, :default => 1
  end
end
