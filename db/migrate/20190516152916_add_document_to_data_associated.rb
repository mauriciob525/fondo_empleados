class AddDocumentToDataAssociated < ActiveRecord::Migration[5.2]
  def change
    add_column :data_associateds, :document, :text
  end
end
