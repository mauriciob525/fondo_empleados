class CreateForeigns < ActiveRecord::Migration[5.2]
  def change
    create_table :foreigns do |t|
      t.boolean :op_foreign
      t.text :description_ext_operation
      t.boolean :account
      t.string :num_ext
      t.text :bank_ext
      t.string :coin
      t.boolean :affirmation

      t.timestamps
    end
  end
end
