class AddTypePaymentToInfoWork < ActiveRecord::Migration[5.2]
  def change
    add_reference :info_works, :type_payment, foreign_key: true
  end
end