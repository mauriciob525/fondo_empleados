class AddTypeCreditEmployedToTypeCredit < ActiveRecord::Migration[5.2]
  def change
    add_reference :type_credits, :type_credit_employed, foreign_key: true
  end
end
