class CreateCredits < ActiveRecord::Migration[5.2]
  def change
    create_table :credits do |t|
      t.string :title
      t.string :short_description
      t.string :description      
      t.string :image
      t.references :type_credit, foreign_key: {on_delete: :cascade, on_update: :cascade}

      t.timestamps
    end
  end
end
