class AddTypeGenderToDataAssociated < ActiveRecord::Migration[5.2]
  def change
    add_reference :data_associateds, :type_gender, foreign_key: true
  end
end
