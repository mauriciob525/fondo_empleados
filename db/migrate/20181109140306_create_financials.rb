class CreateFinancials < ActiveRecord::Migration[5.2]
  def change
    create_table :financials do |t|
      t.string :ingress
      t.string :egress
      t.string :assets
      t.string :liabilities
      t.string :other_ingress
      t.text :description

      t.timestamps
    end
  end
end
