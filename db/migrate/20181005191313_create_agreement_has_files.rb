class CreateAgreementHasFiles < ActiveRecord::Migration[5.2]
  def change
    create_table :agreement_has_files do |t|
      t.string :url
      t.references :type_file, foreign_key: {on_delete: :cascade, on_update: :cascade}
      t.references :agreement, foreign_key: {on_delete: :cascade, on_update: :cascade}

      t.timestamps
    end
  end
end
