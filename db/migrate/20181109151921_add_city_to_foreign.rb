class AddCityToForeign < ActiveRecord::Migration[5.2]
  def change
    add_reference :foreigns, :city, foreign_key: true
  end
end