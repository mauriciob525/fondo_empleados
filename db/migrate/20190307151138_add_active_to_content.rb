class AddActiveToContent < ActiveRecord::Migration[5.2]
  def change
    add_column :contents, :active, :boolean, :default => 1
  end
end
