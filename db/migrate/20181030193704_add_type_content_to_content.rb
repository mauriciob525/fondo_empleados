class AddTypeContentToContent < ActiveRecord::Migration[5.2]
  def change
    add_reference :contents, :type_content, foreign_key: true
  end
end
