class CreateFamilies < ActiveRecord::Migration[5.2]
  def change
    create_table :families do |t|
      t.text :name_familiar
      t.date :birthdate
      t.text :education_familiar
      t.string :phone_familiar

      t.timestamps
    end
  end
end
