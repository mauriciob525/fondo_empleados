class CreateTypeCreditEmployeds < ActiveRecord::Migration[5.2]
  def change
    create_table :type_credit_employeds do |t|
      t.string :name
 
      t.timestamps
    end
  end
end
