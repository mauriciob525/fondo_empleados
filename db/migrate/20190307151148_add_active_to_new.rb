class AddActiveToNew < ActiveRecord::Migration[5.2]
  def change
    add_column :news, :active, :boolean, :default => 1
  end
end
