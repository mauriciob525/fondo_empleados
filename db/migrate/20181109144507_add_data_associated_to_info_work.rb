class AddDataAssociatedToInfoWork < ActiveRecord::Migration[5.2]
  def change
    add_reference :info_works, :data_associated, foreign_key: true
  end
end
