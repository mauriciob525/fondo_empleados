class AddDateExpeditionToDataAssociated < ActiveRecord::Migration[5.2]
  def change
    add_column :data_associateds, :date_expedition, :date
  end
end
