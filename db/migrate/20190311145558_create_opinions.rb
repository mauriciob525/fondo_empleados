class CreateOpinions < ActiveRecord::Migration[5.2]
  def change
    create_table :opinions do |t|
      t.text :opinion
      t.string :name
      t.string :position
      t.boolean :active, :default => 1

      t.timestamps
    end
  end
end
