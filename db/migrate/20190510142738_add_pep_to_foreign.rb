class AddPepToForeign < ActiveRecord::Migration[5.2]
  def change
    add_column :foreigns, :public_resources, :boolean
    add_column :foreigns, :public_recognition, :boolean
    add_column :foreigns, :public_grade, :boolean
  end
end
