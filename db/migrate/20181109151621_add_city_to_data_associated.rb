class AddCityToDataAssociated < ActiveRecord::Migration[5.2]
  def change
    add_reference :data_associateds, :city, foreign_key: true
  end
end
