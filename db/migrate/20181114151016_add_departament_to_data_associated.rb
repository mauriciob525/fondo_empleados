class AddDepartamentToDataAssociated < ActiveRecord::Migration[5.2]
  def change
    add_reference :data_associateds, :departament, foreign_key: true
  end
end
