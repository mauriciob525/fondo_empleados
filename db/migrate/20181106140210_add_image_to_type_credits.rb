class AddImageToTypeCredits < ActiveRecord::Migration[5.2]
  def change
    add_column :type_credits, :image, :string
  end
end
