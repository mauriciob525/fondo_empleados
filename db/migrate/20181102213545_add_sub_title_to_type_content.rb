class AddSubTitleToTypeContent < ActiveRecord::Migration[5.2]
  def change
    add_column :type_contents, :sub_title, :string
  end
end
