class AddActiveToTypeCredit < ActiveRecord::Migration[5.2]
  def change
    add_column :type_credits, :active, :boolean, :default => 1
  end
end
