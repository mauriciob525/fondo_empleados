class AddTypeFamilyToFamily < ActiveRecord::Migration[5.2]
  def change
    add_reference :families, :type_family, foreign_key: true
  end
end