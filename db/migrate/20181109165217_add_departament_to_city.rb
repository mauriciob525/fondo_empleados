class AddDepartamentToCity < ActiveRecord::Migration[5.2]
  def change
    add_reference :cities, :departament, foreign_key: true
  end
end
