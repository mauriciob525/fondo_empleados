class AddSubTitleToCredits < ActiveRecord::Migration[5.2]
  def change
    add_column :credits, :sub_title, :text
  end
end
