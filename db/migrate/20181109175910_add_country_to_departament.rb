class AddCountryToDepartament < ActiveRecord::Migration[5.2]
  def change
    add_reference :departaments, :country, foreign_key: true
  end
end
