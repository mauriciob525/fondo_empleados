class CreateNews < ActiveRecord::Migration[5.2]
  def change
    create_table :news do |t|
      t.string :image
      t.string :title
      t.string :short_description
      t.text :description
      t.integer :position
      t.boolean :is_principal
      t.string :link

      t.timestamps
    end
  end
end
