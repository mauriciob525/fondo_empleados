class AddTypeDealToInfoWork < ActiveRecord::Migration[5.2]
  def change
    add_reference :info_works, :type_deal, foreign_key: true
  end
end