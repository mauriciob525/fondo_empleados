class CreateCommittes < ActiveRecord::Migration[5.2]
  def change
    create_table :committes do |t|
      t.text :title
      t.text :sub_title
      t.text :description

      t.timestamps
    end
  end
end
