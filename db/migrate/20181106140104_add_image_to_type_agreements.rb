class AddImageToTypeAgreements < ActiveRecord::Migration[5.2]
  def change
    add_column :type_agreements, :image, :string
  end
end
