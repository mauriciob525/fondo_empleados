class AddDataAssociatedToForeign < ActiveRecord::Migration[5.2]
  def change
    add_reference :foreigns, :data_associated, foreign_key: true
  end
end
