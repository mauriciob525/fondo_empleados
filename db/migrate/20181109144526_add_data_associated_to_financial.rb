class AddDataAssociatedToFinancial < ActiveRecord::Migration[5.2]
  def change
    add_reference :financials, :data_associated, foreign_key: true
  end
end
