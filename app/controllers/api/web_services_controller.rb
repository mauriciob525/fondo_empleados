class Api::WebServicesController < ApplicationController
	skip_before_action :verify_authenticity_token

	def getNovedades
	    novedades = New.where(active:1).limit(8).order(position: :asc)
	    render json: novedades, status: :ok     
  end

	def getConvenios
    tipo_convenio = TypeAgreement.all.where(active: 1)

    convenios = Agreement.all.where(active: 1)
    convenios = convenios.as_json
    convenios.each do |c|
      c["carrousel"] = AgreementHasFile.where(agreement_id: c["id"].to_i)
    end

    response = { 
      :convenios => convenios,
      :tipo_convenio => tipo_convenio
      }

    render json: response, status: :ok
	end

	def getConvenio
    convenio = Agreement.find(params[:id]).where(active:1)
    archivos = AgreementHasFile.where(type_file_id: @convenio.id)

    response = { 
      :convenio => convenio, 
      :archivos => archivos 
    }
    render json: response, status: :ok
	end

  def getCreditos
    credito = Credit.where(active:1)
    render json: credito, status: :ok
  end

	def getComites
		comites = Committe.where(active:1)  
		render json: comites, status: :ok		
	end  

  def getServicios
    ahorro = Content.where(type_content_id:1).where(active:1)
    funerarios = Content.where(type_content_id:2).where(active:1)    
    tipo_creditos = TypeCredit.where(type_credit_employed_id:2).where(active:1)
    creditos = Credit.getCreditosByType(2)

    response = { 
      :ahorro => ahorro, 
      :funerarios => funerarios,
      :tipo_creditos => tipo_creditos,
      :creditos => creditos
    }
    render json: response, status: :ok      
  end

  def getBeneficios
    afiliacion = Content.where(type_content_id:3).where(active:1)
    serv_convenios = Content.where(type_content_id:4).where(active:1)
    
    response = {
      :afiliacion => afiliacion,
      :serv_convenios => serv_convenios
    }
    render json: response, status: :ok 
  end
  def getContactos
    contactos = Contact.where(active:1)
    
    response = {
      :contactos => contactos
    }
    render json: response, status: :ok 
  end
  def getOpiniones
    opiniones = Opinion.where(active:1)
    response = {
      :opiniones => opiniones
    }
    render json: response, status: :ok 
  end
   def getVinculate
    informacion = Content.where(type_content_id:5).where(active:1)
    tarifas = Content.where(type_content_id:6).where(active:1)
    tipo_creditos = TypeCredit.where(type_credit_employed_id:1).where(active:1)
    creditos = Credit.getCreditosByType(1)

    response = {
      :informacion => informacion,
      :tarifas => tarifas,
      :tipo_creditos => tipo_creditos,
      :creditos => creditos
    }
    render json: response, status: :ok 
  end
  def getDocumentos
    documents = Document.all

    response = {
      :documents => documents
    }
    render json: response, status: :ok 
  end
  def getTypes
    

    response = {
      
    }
    render json: response, status: :ok 
  end

  def getFormulario
    info_asociados = DataAssociated.all
    info_familiar = Family.all
    info_laboral = InfoWork.all
    info_financiera = Financial.all
    moneda_extranjera = Foreign.all

    response = {
      :info_asociados => info_asociados,
      :info_familiar => info_familiar,
      :info_laboral => info_laboral,
      :info_financiera => info_financiera,
      :moneda_extranjera => moneda_extranjera
    }
    render json: response, status: :ok
  end

  def getInfo
        tipo_formato = TypeFormat.all
        tipo_estado = TypeStatus.all
        tipo_genero = TypeGender.all
        tipo_vivienda = TypeHome.all
        tipo_ocupacion = TypeOccupation.all
        tipo_de_salario = TypeSalary.all
        tipo_contrato = TypeDeal.all
        tipo_abono = TypePayment.all
        tipo_familiar = TypeFamily.all
        pais = Country.all
        departamento = Departament.all
        tipo_financiera = TypeFinancial.all
        ciudad = City.all

      response = {
        :tipo_formato => tipo_formato,
        :tipo_estado => tipo_estado,
        :tipo_genero => tipo_genero,
        :tipo_vivienda => tipo_vivienda,
        :tipo_ocupacion => tipo_ocupacion,
        :tipo_de_salario => tipo_de_salario,
        :tipo_contrato => tipo_contrato,
        :tipo_abono => tipo_abono,
        :tipo_familiar => tipo_familiar,
        :pais => pais,
        :departamento => departamento,
        :tipo_financiera => tipo_financiera,
        :ciudad => ciudad        
      }
      render json: response, status: :ok
  end

    def form
      errors = []
      link_pdf=nil
      #DATOS ASOCIADO
       data_associated = DataAssociated.new        
       data_associated.date_filling = params[:date_filling]   
       data_associated.type_format_id = params[:type_format_id]
       data_associated.name = params[:name]
       data_associated.lastname = params[:lastname]
       data_associated.type_status_id = params[:type_status_id]
       data_associated.type_gender_id = params[:type_gender_id]
       data_associated.place_birth = params[:place_birth]
       data_associated.birthdate = params[:birthdate]
       data_associated.email = params[:email]
       data_associated.education = params[:education]
       data_associated.profession = params[:profession]
       data_associated.address = params[:address]
       data_associated.city_id = params[:city_id]
       data_associated.departament_id = params[:departament_id]
       data_associated.phone = params[:phone]
       data_associated.mobile = params[:mobile]
       data_associated.type_home_id = params[:type_home_id]
       data_associated.document = params[:document]
       data_associated.date_expedition = params[:date_expedition]
       data_associated.barrio = params[:barrio]
       data_associated.estrato = params[:estrato]
       data_associated.lugar_expedicion = params[:lugar_expedicion]
       if params[:discount]
          data_associated.discount = params[:discount]
        end
       if params[:source]
          data_associated.source = params[:source]
        end
      data_associated.valid?
      if data_associated.errors.any?
        errors.push(data_associated.errors.messages.values)
      else
        data_associated.save
         #INFO LABORAL  
        info_work =InfoWork.new
        info_work.type_occupation_id = params[:type_occupation_id]
        info_work.date_start = params[:date_start]
        info_work.economic_act = params[:economic_act]
        info_work.cod_ciiu = params[:cod_ciiu]
        info_work.company = params[:company]
        info_work.area_of = params[:area_of]
        info_work.cod_area = params[:cod_area]
        info_work.region = params[:region]
        info_work.phone_laboral = params[:phone_laboral]
        info_work.ext = params[:ext]
        info_work.position = params[:position]
        info_work.email_laboral = params[:email_laboral]
        info_work.salary = params[:salary]
        info_work.type_salary_id = params[:type_salary_id]
        info_work.type_deal_id = params[:type_deal_id]
        info_work.type_payment_id = params[:type_payment_id]
        info_work.num = params[:num]
        info_work.bank = params[:bank]
        info_work.other_contract = params[:other_contract]
        info_work.data_associated_id = data_associated.id
        info_work.valid?
        if info_work.errors.any?
          errors.push(info_work.errors.messages.values)
        else
          info_work.save
        end
        #INFO FAMILIAR
        error_familiares = []
        familiares=params[:familiares]
        familiares.each do |a|
            family = Family.new
            family.name_familiar = (a['name_familiar']) 
            family.birthdate = (a['birthdate_familiar'])
            family.education_familiar = (a['education_familiar'])
            family.type_occupation_id = (a['type_occupation_id_familiar'])
            family.phone_familiar = (a['phone_familiar'])
            family.type_family_id = (a['type_family_id'])
            family.data_associated_id = data_associated.id
            family.valid?
            if family.errors.any?
              errors.push(family.errors.messages.values)  
            else
              family.save
            end
        end
        #INFO FINANCIERA LABORAL
        financial = Financial.new
        financial.ingress = params[:ingress]
        financial.egress = params[:egress]
        financial.assets = params[:assets]
        financial.liabilities = params[:liabilities]
        financial.other_ingress = params[:other_ingress]
        financial.description = params[:description]  
        financial.type_financial_id = 1
        financial.data_associated_id = data_associated.id
        financial.valid?
        if financial.errors.any?
          errors.push(financial.errors.messages.values) 
        else
          financial.save
        end
        #MONEDA EXTRANJERA #Declaracion de persona expuesta
       foreign = Foreign.new
       foreign.description_ext_operation = params[:description_ext_operation]
       foreign.num_ext = params[:num_ext]
       foreign.bank_ext = params[:bank_ext]
       foreign.coin = params[:coin_ext]
       foreign.city_id = params[:city_id_ext]          
       foreign.country_id = params[:country_id_ext]
         if params[:ext_operation].to_i==1
          foreign.op_foreign = true
         else
          foreign.op_foreign = false
         end 
         if params[:have_ext_accouts].to_i==1
            foreign.account = true
         else
          foreign.account = false
         end 
         if params[:affirmation]         
            foreign.affirmation = true
          else
            foreign.affirmation = false
         end
        foreign.public_resources = params[:recursos_publicos].to_i == 1 ? true : false
        foreign.public_recognition = params[:reconocimiento_publico].to_i == 1 ? true : false
        foreign.public_grade = params[:grado_publico].to_i == 1 ? true : false
        foreign.consistency_degree = params[:grado_consanguinidad].to_i == 1 ? true : false
        foreign.data_associated_id = data_associated.id
        foreign.valid?
        if foreign.errors.any?
          errors.push(foreign.errors.messages.values)  
        else
          foreign.save
          name_pdf = getPdfFormulario(data_associated.id)
          link_pdf = name_pdf
        end
      end
      if !errors.empty?
        data_associated.destroy
      end

      response = { 
        :errors => errors, 
        :mgs => '',
        :link_pdf => link_pdf,
        }
      render json: response
   end

end
