class AdminController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :authenticate_admin!
  
  def index
  	@novedades = New.limit(8).order(position: :desc)
  	@convenios = Agreement.all
    @credito_servicio = Credit.creditType(2)
  	@credito_vinculate = Credit.creditType(1)
    @servicios = Content.where("type_content_id IN (1,2)")
    @beneficios = Content.where("type_content_id IN (3,4)")
    @vinculate = Content.where("type_content_id IN (5,6)") 
    @comites = Committe.all
    @asociado = DataAssociated.all
    agreement = TypeAgreement.getTipos
    credit = TypeCredit.getTipos
    @tipos = agreement + credit
    @contacts = Contact.all
    @opinions = Opinion.all
    @files = Document.all
  end 
  #NOVEDADES
  def crear_novedad
  	@posiciones_ocupadas = New.where.not(position: nil).select("position")
    if params[:id]
      @novedad = New.find(params[:id])
      @posiciones_ocupadas  = @posiciones_ocupadas.where.not(id: params[:id])
    end
  end

  def new_novedad
    if params[:id].blank?
        new_ = New.new
      else
        new_ = New.find(params[:id])
      end 
  	new_.title = params[:title]
  	new_.short_description = params[:short_description]
  	new_.description = params[:description]
  	new_.link = params[:link]
    if params[:image]
       new_.image = params[:image]
    end
    new_.position = params[:position]
    new_.valid?
    if new_.errors.any?
       flash[:msg] = new_.errors.messages.values 
       redirect_to "/crear_novedad"
    else
    	new_.save
      redirect_to "/administrador"
    end
  end

  def delete_novedad
      new_ = New.find(params[:id])
      updated_at = new_.updated_at
      new_.active = 0
      new_.save
      redirect_to "/administrador"
  end

  def active_novedad
      new_ = New.find(params[:id])
      updated_at = new_.updated_at
      new_.active = 1
      new_.save
      redirect_to "/administrador"
  end

  #CONVENIOS
  def crear_convenio
    @type_agreement = TypeAgreement.all
    if params[:id]
      @agreement = Agreement.find(params[:id])
      @agreement_has_file  = AgreementHasFile.where(agreement_id: params[:id])
    end
  end
  def new_convenio
    if params[:id].blank?
       agreement = Agreement.new
    else
       agreement = Agreement.find(params[:id])
    end 
    agreement.title = params[:title]
    agreement.description = params[:description]
    agreement.discount = params[:discount]
    agreement.type_agreement_id = params[:type_agreement_id]
    if params[:image]
      agreement.image = params[:image]
    end
    agreement.valid?
    if agreement.errors.any?
        flash[:msg] = agreement.errors.messages.values
        redirect_to "/crear_convenio"
    else   
      agreement.save
      params[:images].each do |img|
        agr_has_file = AgreementHasFile.new
        agr_has_file.type_file_id = 1
        agr_has_file.agreement_id = agreement.id
        agr_has_file.url = img
        agr_has_file.save
      end
      redirect_to "/administrador"  
    end  
  end

  def delete_convenio
      agreement = Agreement.find(params[:id])
      updated_at = agreement.updated_at
      agreement.active = 0
      agreement.save
      redirect_to "/administrador"
  end
  def delete_img_convenios
    AgreementHasFile.find(params[:id]).destroy

    response = { 
      :error => false
    }
    render json: response, status: :ok
  end
  def active_convenio
      agreement = Agreement.find(params[:id])
      updated_at = agreement.updated_at
      agreement.active = 1
      agreement.save
      redirect_to "/administrador"
  end

  #CREDITOS
  def crear_credito
    @type_credit = TypeCredit.where(type_credit_employed_id: params[:tipo])
    if params[:id]
      @credit = Credit.find(params[:id])
    end
  end
  def new_credito
    if params[:id].blank?
      credit = Credit.new
    else
      credit = Credit.find(params[:id])
    end    
    credit.description = params[:description]
    credit.type_credit_id = params[:type_credit_id]  
    credit.sub_title = params[:sub_title]  
    if params[:image]
       credit.image = params[:image]
    end
    credit.valid?
    if credit.errors.any?
        flash[:msg] = credit.errors.messages.values
        redirect_to "/crear_credito"
    else   
     credit.save
     redirect_to "/administrador"
    end
  end

  def delete_credito
      credit = Credit.find(params[:id])
      updated_at = credit.updated_at
      credit.active = 0
      credit.save
      redirect_to "/administrador"
  end

  def active_credito
    credit = Credit.find(params[:id])
    updated_at = credit.updated_at
    credit.active = 1
    credit.save
    redirect_to "/administrador"
  end

  #CONTENIDO
  def crear_contenido
    sql_in = ''
    case params[:tipo].to_i
      when 1
        sql_in = [1,2]
      when 2
        sql_in = [3,4]
      when 3
        sql_in = [5,6]
    end
      @type_content = TypeContent.where("id IN (?)",sql_in)
      if params[:id]
        @content = Content.find params[:id]
      end
  end 
  def new_contenido
      if params[:id].blank?
        content = Content.new
      else
        content = Content.find(params[:id])
      end    
      content.description = params[:description]
      content.type_content_id = params[:type_content_id]
      if params[:image]
        content.image = params[:image]
      end  
      content.valid?
      if content.errors.any?
          flash[:msg] = content.errors.messages.values
          redirect_to "/crear_contenido/"+params[:type_content_id]
      else
        content.save
          redirect_to "/administrador"
      end
  end 
  def delete_contenido
      content = Content.find(params[:id])
      updated_at = content.updated_at
      content.active = 0
      content.save
      redirect_to "/administrador" 
  end

  def active_contenido
      content = Content.find(params[:id])
      updated_at = content.updated_at
      content.active = 1
      content.save
      redirect_to "/administrador"
  end
  def editar_documento
    @document = Document.find(params[:id])
  end
  def update_file
    document = Document.find(params[:id])
    document.file = !params[:file].nil? ? params[:file] : document.file
    document.save
    redirect_to "/administrador"
  end
  #JUNTA Y COMITE
  def crear_comite
    if params[:id]
       @committe = Committe.find(params[:id])
    end  
  end

  def new_comite
    if params[:id].blank?
       committe = Committe.new
      else
       committe = Committe.find(params[:id])
    end         
      committe.title = params[:title]
      committe.sub_title = params[:sub_title]
      committe.description = params[:description]

      committe.valid?
    if committe.errors.any?
       flash[:msg] = committe.errors.messages.values
       redirect_to "/crear_comite"   
    else
       committe.save
       redirect_to "/administrador"   
    end
  end    
  def delete_comite
      committe = Committe.find(params[:id])
      updated_at = committe.updated_at
      committe.active = 0
      committe.save
      redirect_to "/administrador" 
  end

  def active_comite
      committe = Committe.find(params[:id])
      updated_at = committe.updated_at
      committe.active = 1
      committe.save
      redirect_to "/administrador"
  end
  #CONTACTOS
  def crear_contacto
    if params[:id]
       @contact = Contact.find(params[:id])
    end  
  end
  def new_contacto
    if params[:id].blank?
       contact = Contact.new
      else
       contact = Contact.find(params[:id])
    end         
      contact.title = params[:title]
      contact.email = params[:email]
      contact.telephone = params[:telephone]
      contact.valid?
    if contact.errors.any?
       flash[:msg] = contact.errors.messages.values
       redirect_to "/crear_contacto"   
    else
       contact.save
       redirect_to "/administrador"   
    end
  end    
  def delete_contacto
      contact = Contact.find(params[:id])
      contact.active = contact.active ? 0 : 1
      contact.save
      redirect_to "/administrador" 
  end
  #OPINIONES
  def crear_opinion
    if params[:id]
       @opinion = Opinion.find(params[:id])
    end  
  end
  def new_opinion
    if params[:id].blank?
       opinion = Opinion.new
      else
       opinion = Opinion.find(params[:id])
    end         
      opinion.opinion = params[:opinion]
      opinion.name = params[:name]
      opinion.position = params[:position]
      opinion.valid?
    if opinion.errors.any?
       flash[:msg] = opinion.errors.messages.values
       redirect_to "/crear_opinion"   
    else
       opinion.save
       redirect_to "/administrador"   
    end
  end    
  def delete_opinion
      opinion = Opinion.find(params[:id])
      opinion.active = opinion.active ? 0 : 1
      opinion.save
      redirect_to "/administrador" 
  end
  #TIPOS DE CREDITOS Y CONVENIOS
  def crear_tipos
   @types = [{"id" => 1, "name" => "Convenio"},{"id" => 2, "name" => "Crédito Vinculate"},{"id" => 3, "name" => "Crédito Servicio"}]
   if params[:id]
    if params[:tipo].to_i == 1
      @type_conv = TypeAgreement.find params[:id]
    else
      @type_conv = TypeCredit.find params[:id]
    end
   end
  end

  def new_tipos
    if params[:tipo].to_i == 1
      type_agreement = params[:id] ? TypeAgreement.find(params[:id]) : TypeAgreement.new
      type_agreement.name = params[:title]
      type_agreement.image = params[:image].nil? ? type_agreement.image : params[:image]
      type_agreement.save
    else
      type_credit = params[:id] ? TypeCredit.find(params[:id]) : TypeCredit.new
      type_credit.name = params[:title]
      type_credit.image = params[:image].nil? ? type_credit.image : params[:image]
      type_credit.type_credit_employed_id = params[:tipo].to_i == 2 ? 1 : 2
      type_credit.save
    end
    redirect_to "/administrador"
  end

  def active_tipo
    if params[:tipo].to_i == 1
      type = TypeAgreement.find(params[:id])
    else
      type = TypeCredit.find(params[:id])
    end
    type.active = type.active ? 0 : 1
    type.save
    redirect_to "/administrador"
  end

  def view_asociado
      data_associated = DataAssociated.find(params[:id])
      data_associated.name = params[:name]
      data_associated.birthdate = params[:birthdate]
      data_associated.email = params[:email]
      data_associated.type_gender_id = params[:type_gender_id]
      data_associated.education = params[:education]
      data_associated.profession = params[:profession]
      data_associated.address = params[:address]
      data_associated.mobile = params[:mobile]
  end
end
