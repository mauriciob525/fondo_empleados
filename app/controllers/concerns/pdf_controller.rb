class PdfController < ApplicationController

	def index
	end
	
	def getPdfFormulario
		require 'prawn'
		id = 76
		data = DataAssociated.find id
		laboral = InfoWork.where(data_associated_id: id).take
		familiar = Family.where(data_associated_id: id).take
		familiar_conyugue = Family.where(type_family_id: 4, data_associated_id: id).take
		familiar_madre = Family.where(type_family_id: 1, data_associated_id: id).take
		familiar_padre = Family.where(type_family_id: 2, data_associated_id: id).take
		familiar_hijo_1 = Family.where(type_family_id: 3, data_associated_id: id).first
		familiar_hijo_2 = Family.where(type_family_id: 3, data_associated_id: id).second
		familiar_hijo_3 = Family.where(type_family_id: 3, data_associated_id: id).third		
		financiera = Financial.where(type_financial_id: 1, data_associated_id: id).take
		extranjera = Foreign.where(data_associated_id: id).take

		respond_to do |format|
			format.pdf do
				pdf = Prawn::Document.new(:margin => [15, 15, 0]) do

					image 'app/assets/images/descarga.jpeg', :height => 100, :at => [-6, 810]

					bounding_box [475, 767], :width => 40, :align => :right, :height => 14 do
						text "dd/mm/aa", :size => 7, :color => "808B96"
					end
					bounding_box [335, 750], :width => 110, :align => :right, :height => 14 do
					 	text "FECHA DE DILIGENCIAMIENTO", :size => 7, :style => :bold
					end		
					stroke_line [545, 745], [445, 745] 
					bounding_box [435, 755], :width => 110, :align => :right, :height => 14 do
						text (data.date_filling.strftime('%d-%m-%y')), :align => :center, :size => 8
					end		
					bounding_box [395, 730], :width => 110, :align => :right, :height => 14 do
					 	text "SOLICITUD DE AFILIACION", :size => 7, :style => :bold
					end
					bounding_box [395, 710], :width => 110, :align => :right, :height => 14 do
						text "ACTUALIZACION DE DATOS", :size => 7	, :style => :bold
					end
					draw_text "Por favor diligenciar con letra imprenta ó en computador", :size => 6.5, :at => [30, 695]
					move_down 13
						stroke do	  					
		  					#ENCABEZADO
		  					rectangle [520, 735], 13, 13
		  					rectangle [520, 715], 13, 13
		  				end	

		  			stroke do
			  			line [460, 349], [350, 349]
			  			line [460, 366], [350, 366]	
			  		end
			  		
			  		place_birth = City.find data.place_birth


					table [ 
						[{:content => "DATOS DEL ASOCIADO", :colspan => 8, :height => 16, :width => 550, :align => :center, :padding => 3,
						  :size => 9, :font_style => :bold}],

						[{:content => " Nombres y apellidos", :colspan => 4, :size => 8, :padding => 1, :height => 14, :borders =>[:top,:left,:right]},
						 {:content => " Cedula", :size => 8, :padding => 1, :height => 14, :borders =>[:top,:left,:right]}, 
						 {:content => " Lugar y fecha de espedicion", :colspan => 2, :size => 8, :padding => 1, :height => 14, :borders =>[:top,:left]},
						 {:content => " (dd/mm/aa)", :size => 8, :text_color => "808B96", :padding => 1, :height => 14, :align => :center, :borders =>[:top,:right]}],

						[{:content => data.name, :colspan => 2, :size => 8, :padding => 2, :height => 14, :borders =>[:left]},
						 {:content => data.lastname, :colspan => 2, :size => 8, :padding => 2, :height => 14, :borders =>[:right]},
						 {:content => " ", :size => 8, :padding => 2, :height => 14, :borders =>[:right,:left]}, 
						 {:content => " ", :colspan => 2, :padding => 2, :size => 8, :height => 14, :borders =>[:right,:left]}, 
						 {:content => " ", :size => 8, :padding => 2, :height => 14, :borders =>[:right,:left]}], 

						[{:content => " Lugar y fecha de Nacimiento", :colspan => 2, :size => 8, :padding => 1, :height => 14, :borders =>[:top,:left]},
						 {:content => "  (dd/mm/aa)", :size => 8, :text_color => "808B96", :padding => 1, :height => 14, :align => :center, :borders =>[:top,:right]},
						 {:content => " Estado Civil", :size => 8, :padding => 1, :height => 14, :borders =>[:top,:left,:right]},
						 {:content => " Genero", :colspan => 2, :size => 8, :padding => 1, :height => 14, :borders =>[:top,:left,:right]}, 
						 {:content => " E-mail Personal", :colspan => 2, :size => 8, :padding => 1, :height => 14, :borders =>[:top,:left,:right]}],

						[{:content => place_birth.name , :colspan => 2, :size => 8, :padding => 1, :height => 14, :borders =>[:right,:left]}, 
						 {:content => (data.birthdate.strftime('%d-%m-%y')), :size => 8, :padding => 1, :align => :center, :height => 14, :borders =>[:right,:left]},
						 {:content => data.type_status.name, :size => 8, :padding => 1, :height => 14, :borders =>[:right,:left]}, 
						 {:content => "M", :size => 8, :padding => 2, :font_style => :bold, :align => :center, :height => 14, :borders =>[:left]},
						 {:content => "F", :size => 8, :padding => 2, :font_style => :bold, :height => 14, :borders =>[:right]}, 
						 {:content => data.email, :colspan => 2, :size => 8, :padding => 1, :height => 14, :borders =>[:right,:left]}],	

						[{:content => "Nivel de estudios", :colspan => 2, :size => 8, :padding => 1, :height => 14, :borders =>[:top,:right,:left]},
						 {:content => "Profesion",:colspan => 2, :size => 8,:height => 14, :padding => 1, :borders =>[:top,:right,:left]},
						 {:content => "Direccion de residencia", :colspan => 4, :size => 8, :padding => 1, :height => 14,:borders =>[:top,:right,:left]}],

						[{:content => data.education, :colspan => 2, :size => 8, :padding => 1, :height => 14, :borders =>[:right,:left]}, 
						 {:content => data.profession, :colspan => 2, :size => 8, :padding => 1, :height => 14, :borders =>[:right,:left]},
						 {:content => data.address, :colspan => 4, :size => 8, :padding => 1, :height => 14, :borders =>[:right,:left]}], 

						[{:content => "Barrio", :colspan => 3, :size => 8, :padding => 1, :height => 14, :borders =>[:top,:right,:left]},
						 {:content => "Ciudad",:colspan => 2, :size => 8, :padding => 1, :height => 14, :borders =>[:top,:right,:left]},
						 {:content => "Departamento", :colspan => 3, :size => 8, :padding => 1, :height => 14,:borders =>[:top,:right,:left]}],

						[{:content => " ", :colspan => 3, :size => 8, :padding => 1, :height => 14, :borders =>[:right,:left]}, 
						 {:content => data.city.name , :colspan => 2, :size => 8, :padding => 1, :height => 14, :borders =>[:right,:left]},
						 {:content => data.departament.name, :colspan => 3, :size => 8, :padding => 1, :height => 14, :borders =>[:right,:left]}],  

						[{:content => "Telefono", :colspan => 2, :size => 8, :padding => 1, :height => 14, :borders =>[:top,:right,:left]},
						 {:content => "Telefono celular",:colspan => 2, :size => 8, :padding => 1, :height => 14, :borders =>[:top,:right,:left]},
						 {:content => "Tipo de Vivienda", :colspan => 4, :size => 8, :padding => 1, :height => 14,:borders =>[:top,:right,:left]}],

						[{:content => data.phone, :colspan => 2, :size => 8, :padding => 1, :height => 14, :borders =>[:right,:left]}, 
						 {:content => data.mobile, :colspan => 2, :size => 8, :padding => 1, :height => 14, :borders =>[:right,:left]},
						 {:content => "Arriendo", :size => 8, :padding => 2, :height => 14, :borders =>[:left]},
						 {:content => "Familiar", :size => 8, :padding => 2, :height => 14, :borders =>[]},
						 {:content => "Propia", :size => 8, :padding => 2, :height => 14, :borders =>[]},
						 {:content => "Estrato  _______", :size => 8, :padding => 0, :height => 14, :borders =>[:right]}],  

						[{:content => "INFORMACION LABORAL", :colspan => 8, :height => 16, :align => :center, :size => 9, :padding => 2,
						  :font_style => :bold}],  

						[{:content => "Ocupacion", :colspan => 2, :size => 8, :padding => 1, :height => 14, :borders =>[:top,:right,:left]},
						 {:content => "Fecha Ingreso", :size => 8, :padding => 1, :height => 14, :borders =>[:top,:left]},
						 {:content => "(dd/mm/aa)", :size => 8, :text_color => "808B96", :align => :center,  :padding => 1, :height => 14, :borders =>[:top,:right]},
						 {:content => "Actividad economica", :colspan => 3, :size => 8, :padding => 1, :height => 14,:borders =>[:top,:right,:left]},
						 {:content => "Codigo CIIU", :size => 8,  :padding => 1, :height => 14,:borders =>[:top,:right,:left]}],  

						[{:content => "Asalariado", :size => 8, :padding => 2, :height => 14, :borders =>[:left]},
						 {:content => "Pensionado", :size => 8, :padding => 2, :height => 14, :borders =>[:right]}, 	 
						 {:content => (laboral.date_start.strftime('%d-%m-%y')), :colspan => 2, :size => 8, :align => :center, :padding => 1, :height => 14, :borders =>[:right,:left]}, 
						 {:content => laboral.economic_act, :colspan => 3, :size => 8, :padding => 1, :height => 14, :borders =>[:right,:left]},
						 {:content => laboral.cod_ciiu, :size => 8, :height => 14, :padding => 1, :borders =>[:right,:left]}],

						[{:content => "Nombre Empresa", :colspan => 3, :size => 8, :padding => 1, :height => 14, :borders =>[:top,:right,:left]},
						 {:content => "Area/Seccion/Of.", :colspan => 2, :size => 8, :padding => 1, :height => 14, :borders =>[:top,:right,:left]}, 
						 {:content => "Codigo area/Of.", :size => 8, :padding => 1, :height => 14, :borders =>[:top,:right,:left]}, 
						 {:content => "Region", :colspan => 2, :size => 8, :padding => 1, :height => 14, :borders =>[:top,:right,:left]}],  

						[{:content => laboral.company, :colspan => 3, :size => 8, :padding => 1, :height => 14, :borders =>[:right,:left]}, 
						 {:content => laboral.area_of, :colspan => 2, :size => 8, :padding => 1, :height => 14, :borders =>[:right,:left]},
						 {:content => laboral.cod_area, :size => 8, :padding => 1, :height => 14, :borders =>[:right,:left]}, 
						 {:content => laboral.region, :colspan => 2, :size => 8, :padding => 1, :height => 14, :borders =>[:right,:left]}],

						[{:content => "Tel./Ext", :colspan => 2, :size => 8, :padding => 1, :height => 14, :borders =>[:top,:right,:left]},
						 {:content => "Cargo", :colspan => 2, :size => 8, :padding => 1, :height => 14, :borders =>[:top,:right,:left]}, 
						 {:content => "E-mail corporativo", :colspan => 3, :size => 8, :padding => 1, :height => 14, :borders =>[:top,:right,:left]}, 
						 {:content => "Sueldo Basico", :size => 8, :padding => 1, :height => 14, :borders =>[:top,:right,:left]}],  

						[{:content => laboral.phone_laboral, :size => 8, :padding => 1, :height => 14, :borders =>[:left]},
						 {:content => laboral.ext, :size => 8, :padding => 1, :height => 14, :borders =>[:right]}, 
						 {:content => laboral.position, :colspan => 2, :size => 8, :padding => 1, :height => 14, :borders =>[:right,:left]},
						 {:content => laboral.email_laboral, :colspan => 3, :size => 8, :padding => 1, :height => 14, :borders =>[:right,:left]}, 
						 {:content => laboral.salary, :size => 8, :padding => 1, :height => 14, :borders =>[:right,:left]}],  

						[{:content => "Tipo de salario", :colspan => 2, :padding => 2, :height => 14, :size => 8, :borders =>[:top,:right,:left], :font_style => :bold},
						 {:content => "Tipo de contrato", :colspan => 2, :padding => 2, :height => 14, :size => 8, :borders =>[:top,:right,:left], :font_style => :bold},
						 {:content => "Forma de abono", :colspan => 3, :padding => 2, :height => 14, :size => 8, :borders =>[:top,:right,:left], :font_style => :bold},
						 {:content => "Descuento ahorros", :padding => 2, :height => 14, :size => 8, :align => :center, :borders =>[:top,:right,:left], :font_style => :bold}], 

						[{:content => "Integral ", :colspan => 2, :size => 8, :padding => 2, :height => 18, :borders =>[:right,:left]}, 
						 {:content => "Indefinido", :colspan => 2, :size => 8, :padding => 2, :height => 18, :borders =>[:right,:left]},
						 {:content => "Cta de ahorros                       Cta corriente", :colspan => 3, :size => 8, :align => :center, :padding => 2, :height => 18, :borders =>[:right,:left]}, 
						 {:content => "Porcentaje ", :size => 8, :font_style => :bold, :align => :center, :padding => 2, :height => 18, :borders =>[:right,:left]}], 

						[{:content => "Ley 50", :colspan => 2, :size => 8, :padding => 2, :height => 17, :borders =>[:right,:left]}, 
						 {:content => "Término Fijo", :colspan => 2, :size => 8, :padding => 2, :height => 17, :borders =>[:right,:left]},
						 {:content => "(000-000000000) ", :colspan => 3, :size => 8, :text_color => "808B96", :padding => 2, :align => :center, :height => 17, :borders =>[:right,:left]}, 
						 {:content => "5% /salario basico ", :size => 8, :align => :center, :padding => 2, :height => 17, :borders =>[:right,:left]}],
						 
						[{:content => "Retroactividad  ", :colspan => 2, :size => 8, :padding => 2, :height => 17, :borders =>[:right,:left]}, 
						 {:content => "Otro", :colspan => 2, :size => 8, :padding => 2, :height => 17, :borders =>[:right,:left]},
						 {:content => "Número", :size => 8, :padding => 2, :height => 17, :borders =>[:left]},
						 {:content => (laboral.num), :colspan => 2, :size => 8, :padding => 2, :height => 17, :borders =>[:right]},
						 {:content => "Periodicidad", :size => 8, :align => :center, :font_style => :bold, :padding => 2, :height => 17, :borders =>[:right,:left]}],
						 
						[{:content => " ", :colspan => 2, :size => 8, :padding => 2, :height => 17, :borders =>[:right,:left]}, 
						 {:content => "Cual?      ______________ ", :colspan => 2, :size => 8, :padding => 2, :height => 17, :borders =>[:right,:left]},
						 {:content => "Banco ", :size => 8, :padding => 2, :height => 17, :borders =>[:left]}, 
						 {:content => laboral.bank, :colspan => 2, :size => 8, :padding => 2, :height => 17, :borders =>[:right]},
						 {:content => "Mensual", :size => 8, :align => :center, :padding => 2, :height => 17, :borders =>[:right,:left]}],

						[{:content => "DECLARACION DE PERSONA EXPUESTA PUBLICAMENTE (PEP)", :colspan => 8, :height => 16, :align => :center, :size => 9, :font_style => :bold, :padding => 2}], 

						[{:content => "¿Usted desempeña en la actualidad o ha desempeñado en los ultimos veinticuatro (24) meses, cargos o actividades en los cuales?",
						  :colspan => 8, :padding => 3, :height => 16, :align => :center, :size => 8.5, :font_style => :bold_italic}],   

						[{:content => "¿Maneje recursos públicos o tenga poder de disposición sobre estos?", :rowspan => 3, :size => 7.5, :padding => 3, :height => 14, :borders =>[:left]},
						 {:content => "Si ", :size => 7.5, :font_style => :bold, :padding => 4, :height => 14, :borders =>[:right]},
						 {:content => "¿Tiene o goza de reconocimiento público?", :rowspan => 3, :size => 7.5, :padding => 3, :height => 14, :borders =>[:left]},
						 {:content => "Si ", :size => 7.5, :font_style => :bold,  :padding => 3, :height => 14, :borders =>[:right]},
						 {:content => "¿Tiene grado de poder público o desempeña una funcion pública prominente o destaca en el estado, relacionada con alguno de los cargos descritos en el decreto 1674 de 2016",
						  :rowspan => 3, :colspan => 3, :size => 7.5, :padding => 3, :height => 14, :borders =>[:left]}, 
						 {:content => "Si ", :size => 7.5, :font_style => :bold, :padding => 4, :height => 14, :borders =>[:right]}], 
						
						[{:content => " ", :size => 7, :padding => 1, :height => 14, :borders =>[:right]},
						 {:content => " ", :size => 7, :padding => 1, :height => 14, :borders =>[:right]},
						 {:content => " ", :size => 7, :padding => 1, :height => 14, :borders =>[:right]}],

						[{:content => "No ", :size => 8, :font_style => :bold, :padding => 3, :height => 14, :borders =>[:right]},
						 {:content => "No ", :size => 8, :font_style => :bold, :padding => 3, :height => 14, :borders =>[:right]},
						 {:content => "No ", :size => 8, :font_style => :bold, :padding => 3, :height => 14, :borders =>[:right]}],

						[{:content => "¿Tiene familiares, hasta el segundo grado de consaguinidad, que encajen en los escenarios descritos previamente?",
						  :colspan => 6, :padding => 3, :height => 19, :align => :center, :size => 8, :font_style => :bold, :borders => [:top,:left]}, 
						 {:content => "Si", :size => 8, :padding => 4, :font_style => :bold, :height => 19, :borders =>[:top]},
						 {:content => "No", :size => 8, :padding => 4, :font_style => :bold, :height => 19, :borders =>[:top,:right]}], 
						
						[{:content => "INFORMACION FINANCIERA", :colspan => 8, :height => 16, :align => :center, :size => 9, :padding => 3, :font_style => :bold}], 

						[{:content => "Total ingresos mensuales", :colspan => 2, :size => 8, :align => :center, :padding => 2, :height => 14, :borders =>[:right,:left]}, 
						 {:content => "Total egresos mensuales", :colspan => 2, :size => 8, :align => :center, :padding => 2, :height => 14, :borders =>[:right,:left]},
						 {:content => "Total activos", :colspan => 2, :size => 8, :align => :center, :padding => 2, :height => 14,   :borders =>[:right,:left]}, 
						 {:content => "Total pasivos", :colspan => 2, :size => 8, :align => :center, :padding => 2, :height => 14, :borders =>[:right,:left]}],

						[{:content => financiera.ingress, :colspan => 2, :size => 8, :padding => 1, :height => 14, :borders =>[:right,:left]}, 
						 {:content => financiera.egress, :colspan => 2, :size => 8, :padding => 1, :height => 14, :borders =>[:right,:left]},
						 {:content => financiera.assets, :colspan => 2, :size => 8, :padding => 1, :height => 14, :borders =>[:right,:left]}, 
						 {:content => financiera.liabilities, :colspan => 2, :size => 8, :padding => 1, :height => 14, :borders =>[:right,:left]}], 

						[{:content => "Otros ingresos o ingresos no operacionales", :colspan => 3, :size => 8, :align => :center, :padding => 2, :height => 14, :borders =>[:top,:right,:left]}, 
						 {:content => "descripcion de otros ingresos o ingresos no operacionales", :colspan => 5, :size => 8, :padding => 2, :height => 14, :borders =>[:top,:right,:left]}],

						[{:content => financiera.other_ingress, :colspan => 3, :size => 8, :padding => 1, :height => 14, :borders =>[:right,:left]}, 
						 {:content => financiera.description, :colspan => 5, :size => 8, :padding => 1, :height => 14, :borders =>[:right,:left]}], 						 
 
 						[{:content => "OPERACIONES MONEDA EXTRANJERA", :colspan => 8, :height => 16, :align => :center, :size => 9, :padding => 3, :font_style => :bold}], 

 						[{:content => "¿Realiza operaciones en moneda extranjera?", :colspan => 2, :size => 8, :align => :center, :padding => 1, :height => 18, :borders =>[:left]},
 						 {:content => "¿Posee cuentas en moneda extranjera?", :colspan => 2, :size => 8, :align => :center, :padding => 1,:height => 18, :borders =>[]},
						 {:content => "N°. cuenta", :size => 8, :padding => 5, :align => :right, :height => 18, :borders =>[]},
						 {:content => extranjera.num, :colspan => 3, :size => 8, :padding => 5, :align => :center, :height => 18, :borders =>[:right]}],

 						[{:content => " ", :colspan => 2, :size => 8, :padding => -2, :align => :center, :height => 15, :borders =>[:left]},
 						 {:content => " ", :colspan => 2, :size => 8, :padding => -2, :align => :center, :height => 15, :borders =>[]},
						 {:content => "Banco", :size => 8, :padding => 3, :align => :right, :height => 15, :borders =>[]},
						 {:content => extranjera.bank, :colspan => 3, :size => 8, :padding => 3, :align => :center, :height => 15, :borders =>[:right]}], 
						 
						[{:content => "SI", :size => 8, :align => :center, :padding => 2, :height => 15, :borders =>[:left]}, 
						 {:content => "NO", :size => 8, :align => :center, :padding => 2, :height => 15, :borders =>[]},
						 {:content => "SI", :size => 8, :align => :center, :padding => 2, :height => 15, :borders =>[]}, 
						 {:content => "NO", :size => 8, :align => :center, :padding => 2, :height => 15, :borders =>[]},
						 {:content => "Moneda ", :size => 8, :align => :right, :padding => 3, :height => 15, :borders =>[]},
						 {:content => extranjera.coin, :colspan => 3, :size => 8, :align => :center, :padding => 3, :height => 15, :borders =>[:right]}],

						[{:content => " ", :colspan => 2, :size => 8, :padding => 1, :height => 15, :borders =>[:left]},
 						 {:content => "", :colspan => 2, :size => 8, :align => :center, :padding => 1, :height => 15, :borders =>[]},
						 {:content => "Ciudad", :size => 8, :align => :right, :padding => 3, :height => 15, :borders =>[]},
						 {:content => extranjera.city.name, :colspan => 3, :size => 8, :align => :center, :padding => 3, :height => 15, :borders =>[:right]}], 

						[{:content => "¿Cuales?", :size => 8, :align => :center, :padding => 0, :height => 15, :borders =>[:left]},
						 {:content => extranjera.description_ext_operation, :size => 8, :align => :left, :padding => 0, :height => 15, :borders =>[]},	
 						 {:content => "", :colspan => 2, :size => 8, :align => :center, :padding => 1, :height => 15, :borders =>[]},
						 {:content => "Pais", :size => 8, :align => :right, :padding => 3, :height => 15, :borders =>[]},
						 {:content => extranjera.country.name, :colspan => 3, :size => 8, :align => :center, :padding => 3, :height => 15, :borders =>[:right]}],

						[{:content => " ", :colspan => 2, :size => 6, :align => :center, :height => 15, :borders =>[:bottom,:left]},
 						 {:content => " ", :colspan => 2, :size => 6, :align => :center, :height => 15, :borders =>[:bottom]},
						 {:content => " ", :colspan => 4, :size => 6, :align => :center, :height => 15, :borders =>[:bottom,:right]}]  
					]
						#MONEDA EXTRANJERA
						stroke do
				  			line [550, 148], [410, 148]
				  			line [550, 132], [410, 132]	
				  			line [550, 117], [410, 117]
				  			line [550, 102], [410, 102]
				  			line [550, 87], [410, 87]
				  			line [165, 90], [65, 90]
			  			end	

						stroke do	  					
	  						#GENERO
	  						rectangle [333, 627], 13, 13
	  						rectangle [374, 627], 13, 13
	  						#TIPO DE VIVIENDA
	  						rectangle [335, 542], 12, 12
	  						rectangle [405, 542], 12, 12
	  						rectangle [475, 542], 12, 12
	  						#OCUPACION
	  						rectangle [43, 498], 12, 12
	  						rectangle [123, 498], 12, 12
	  						#TIPO DE SALARIO 
	  						rectangle [100, 411], 13, 13
	  						rectangle [100, 396], 13, 13
	  						rectangle [100, 381], 13, 13
	  						#TIPO DE CONTRATO
	  						rectangle [250, 411], 13, 13
	  						rectangle [250, 396], 13, 13
	  						rectangle [250, 381], 13, 13
	  						#FORMA DE ABONO
	  						rectangle [386, 411], 13, 13
	  						rectangle [485, 411], 13, 13
	  						#DECLARACION 
	  						rectangle [105, 309], 13, 13
	  						rectangle [105, 287], 13, 13
	  						rectangle [250, 309], 13, 13
	  						rectangle [250, 287], 13, 13
	  						rectangle [540, 309], 13, 13
	  						rectangle [540, 287], 13, 13
	  						rectangle [455, 268], 12.5, 12.5
	  						rectangle [535, 268], 12.5, 12.5
	  						#MONEDA EXTRANJERA
	  						rectangle [55, 130], 13, 13
	  						rectangle [130, 130], 13, 13
	  						rectangle [201, 130], 13, 13
	  						rectangle [273, 130], 13, 13
	  					end	
	  					
	  					stroke do 
	  						if data.type_format_id == 1
	  							#AFILIACION
		  						line [532, 733], [522, 723] 
	  							line [532, 723], [522, 733]
	  						else
	  							#ACTUALIZACION
	  							line [532, 713], [522, 703] 
	  							line [532, 703], [522, 713]
	  						end

	  						if data.type_gender_id == 1
		  						#GENERO M
		  						line [344, 625], [335, 615] 
		  						line [344, 615], [335, 625]
	  						else
	  							#GENERO F
	  							line [385, 625], [376, 615] 
	  							line [385, 615], [376, 625]
	  						end	  

	  						#TIPO DE VIVIENDA
	  						if data.type_home_id == 1
		  						#ARRIENDO
		  						line [345, 531], [336, 541] 
		  						line [345, 541], [336, 531]
		  					elsif	data.type_home_id == 2
		  						#FAMILIAR
		  						line [415, 531], [406, 541] 
		  						line [415, 541], [406, 531]
		  					else	
		  						#PROPIA
		  						line [485, 531], [476, 541] 
		  						line [485, 541], [476, 531]
		  					end	

		  					#OCUPACION
		  					if laboral.type_occupation_id == 1
		  						#ASALARIADO
		  						line [53, 497], [44, 487] 
		  						line [53, 487], [44, 497]
		  					else	
		  						#PENSIONADO
		  						line [133, 497], [124, 487] 
		  						line [133, 487], [124, 497]
		  					end	

	  						#TIPO SALARIO
	  						if laboral.type_salary_id == 1
		  						#INTEGRAL
		  						line [112, 409], [102, 399] 
		  						line [112, 399], [102, 409]
		  					elsif laboral.type_salary_id == 2	
		  						#LEY50
		  						line [112, 394], [102, 385] 
		  						line [112, 385], [102, 394]
		  					else	
		  						#RETRO
		  						line [112, 378], [102, 369] 
		  						line [112, 369], [102, 378]
		  					end	

	  						#TIPOCONTRATO
	  						if laboral.type_deal_id == 1
		  						#INDEFINIDO
		  						line [262, 409], [252, 399] 
		  						line [262, 399], [252, 409]
		  					elsif laboral.type_deal_id == 2	
		  						#FIJO
		  						line [262, 394], [252, 385] 
		  						line [262, 385], [252, 394]
		  					else	
		  						#OTRO
		  						line [262, 378], [252, 369] 
		  						line [262, 369], [252, 378]
		  					end	

	  						#ABONO
	  						if laboral.type_payment_id == 1
		  						#CTAAHORROS
		  						line [398, 409], [388, 399] 
		  						line [398, 399], [388, 409]	 
		  					else	 						
		  						#CTACORRIENTE
		  						line [497, 409], [487, 399] 
		  						line [497, 399], [487, 409]
		  					end	
	  						
		  					if extranjera.op_foreign == true
		  					   #MONEDA EXTRANJERA
		  						line [66, 128], [56, 118] 
		  						line [66, 118], [56, 128]	
		  					else
		  						line [142, 128], [132, 118] 
		  						line [142, 118], [132, 128]
					        end 

	  						if extranjera.account  == true
		  						#CUENTAS
		  						line [212, 128], [202, 118] 
		  						line [212, 118], [202, 128]
		  					else
		  						line [285, 128], [275, 118] 
		  						line [285, 118], [275, 128]
		  					end		  						
	  					end	

					start_new_page
					table [
						 [{:content => " ", :colspan => 8, :height => 5, :width => 580, :borders =>[]}],	
						 [{:content => "Declaro que no realizo transacciones en moneda extranjera", :colspan => 5, :size => 8, :padding => 8, :font_style => :bold,
						   :align => :right, :height => 25, :borders =>[:left,:top]}, 
						 {:content => " ", :colspan => 3, :height => 25, :borders =>[:top,:right]}],

						[{:content => " ", :colspan => 8, :size => 7, :height => 25, :borders =>[:left,:right]}], 
						[{:content => " ", :colspan => 8, :size => 7, :height => 25, :borders =>[:left,:right]}],
						[{:content => " ", :colspan => 8, :size => 7, :height => 25, :borders =>[:left,:right]}],

						[{:content => " ", :size => 7, :height => 25, :padding => 5, :borders =>[:left]},
						 {:content => "_________________________________________________________", :colspan => 4, :size => 7, :padding => 5, 
						  :align => :center, :height => 25, :borders =>[]},
					     {:content => " ", :colspan => 3, :size => 7, :padding => 5, :height => 25, :borders =>[:right]}],

						[{:content => " ", :size => 7, :height => 25, :padding => -10, :borders =>[:left]},
						 {:content => "FIRMA Y CEDULA DEL ASOCIADO", :colspan => 4, :size => 7, :padding => -10, :font_style => :bold, :align => :center, :height => 25, 
						  :borders =>[]},
						 {:content => "Huella", :size => 7, :align => :right, :padding => -10, :font_style => :bold, :height => 25, :borders =>[]}, 
						 {:content => " ", :colspan => 3, :size => 7, :padding => -10, :height => 25, :borders =>[:right]} 
					     ],

					    [{:content => "INFORMACION FAMILIAR", :colspan => 8, :height => 16, :align => :center, :size => 9, :padding => 3, :font_style => :bold}], 

					    [{:content => " ", :colspan => 3, :size => 7, :padding => 1, :font_style => :bold, :height => 15, :borders =>[:left]},
						 {:content => "Fecha de nacimiento", :size  => 7, :padding => 1, :font_style => :bold, :align => :center, :height => 15, :borders => []}, 
						 {:content => "Nivel de educacion", :colspan => 2, :size  => 7, :padding => 1, :font_style => :bold, :align => :center, :height => 15, :borders => []},
						 {:content => "Ocupacion", :size  => 7, :padding => 1, :font_style => :bold, :align => :center, :height => 15, :borders => []},
						 {:content => "Teléfono", :size  => 7, :padding => 1, :font_style => :bold, :align => :center, :height => 15, :borders => [:right]}],

					
						[{:content => "Conyugue o Compañero(a)", :colspan => 3, :size  => 7, :padding => 1, :font_style => :bold, :height => 14, :borders =>[:left]},
						 {:content => "(dd/mm/aa)", :size  => 6, :text_color => "808B96", :padding => -5, :align => :center, :height => 14, :borders =>[]},
						 {:content => " ", :colspan => 2, :size  => 7, :padding => 1, :height => 14, :borders =>[]},
						 {:content => " ", :size  => 7, :padding => 1, :height => 14, :borders =>[]},
						 {:content => " ", :size  => 7, :padding => 1, :height => 14, :borders =>[:right]}], 

						[{:content => !familiar_conyugue.nil? ? familiar_conyugue.name_familiar : '', :colspan => 3, :size  => 7, :padding => 1, :height => 14, :borders =>[:left,:right,:bottom]},
						 {:content => !familiar_conyugue.nil? ? (familiar_conyugue.birthdate.strftime('%d-%m-%y')) : '', :size  => 7, :padding => 1, :align => :center, :height => 14, :borders =>[:left,:right,:bottom]},
						 {:content => !familiar_conyugue.nil? ? familiar_conyugue.education_familiar : '', :colspan => 2, :size  => 7, :padding => 1, :height => 14, :borders =>[:left,:right,:bottom]},
						 {:content => !familiar_conyugue.nil? ? familiar_conyugue.type_occupation.name : '', :size  => 7, :padding => 1, :height => 14, :borders =>[:left,:right,:bottom]},
						 {:content => !familiar_conyugue.nil? ? familiar_conyugue.phone_familiar : '', :size  => 7, :padding => 1, :height => 14, :borders =>[:left,:right,:bottom]}], 
					
						[{:content => "Madre", :colspan => 3, :size  => 7, :padding => 1, :font_style => :bold, :height => 14, :borders =>[:left]},
						 {:content => " ", :size  => 7, :padding => 1, :height => 14, :borders =>[]},
						 {:content => " ", :colspan => 2, :size  => 7, :padding => 1, :height => 14, :borders =>[]},
						 {:content => " ", :size  => 7, :padding => 1, :height => 14, :borders =>[]},
						 {:content => " ", :size  => 7, :padding => 1, :height => 14, :borders =>[:right]}], 

						[{:content => !familiar_madre.nil? ? familiar_madre.name_familiar : '', :colspan => 3, :size  => 7, :padding => 1, :height => 14, :borders =>[:left,:right,:bottom]},
						 {:content => !familiar_madre.nil? ? (familiar_madre.birthdate.strftime('%d-%m-%y')) : '', :size  => 7, :padding => 1, :align => :center, :height => 14, :borders =>[:left,:right,:bottom]},
						 {:content => !familiar_madre.nil? ? familiar_madre.education_familiar : '', :colspan => 2, :size  => 7, :padding => 1, :height => 14, :borders =>[:left,:right,:bottom]},
						 {:content => !familiar_madre.nil? ? familiar_madre.type_occupation.name : '', :size  => 7, :padding => 1, :height => 14, :borders =>[:left,:right,:bottom]},
						 {:content => !familiar_madre.nil? ? familiar_madre.phone_familiar : '', :size  => 7, :padding => 1, :height => 14, :borders =>[:left,:right,:bottom]}], 
					
						[{:content => "Padre", :colspan => 3, :size  => 7, :padding => 1, :font_style => :bold, :height => 14, :borders =>[:left]},
						 {:content => " ", :size  => 7, :padding => 1, :height => 14, :borders =>[]},
						 {:content => " ", :colspan => 2, :size  => 7, :padding => 1, :height => 14, :borders =>[]},
						 {:content => " ", :size  => 7, :padding => 1, :height => 14, :borders =>[]},
						 {:content => " ", :size  => 7, :padding => 1, :height => 14, :borders =>[:right]}], 

						[{:content => !familiar_padre.nil? ? familiar_padre.name_familiar : '', :colspan => 3, :size  => 7, :padding => 1, :height => 14, :borders =>[:left,:right,:bottom]},
						 {:content => !familiar_padre.nil? ?  (familiar_padre.birthdate.strftime('%d-%m-%y')) : '', :size  => 7, :padding => 1, :align => :center, :height => 14, :borders =>[:left,:right,:bottom]},
						 {:content => !familiar_padre.nil? ? familiar_padre.education_familiar : '', :colspan => 2, :size  => 7, :padding => 1, :height => 14, :borders =>[:left,:right,:bottom]},
						 {:content => !familiar_padre.nil? ? familiar_padre.type_occupation.name : '', :size  => 7, :padding => 1, :height => 14, :borders =>[:left,:right,:bottom]},
						 {:content => !familiar_padre.nil? ? familiar_padre.phone_familiar : '', :size  => 7, :padding => 1, :height => 14, :borders =>[:left,:right,:bottom]}],

						[{:content => "Hijos", :colspan => 3, :size  => 7, :padding => 1, :font_style => :bold, :height => 14, :borders =>[:left]},
						 {:content => " ", :size  => 7, :padding => 1, :height => 14, :borders =>[]},
						 {:content => " ", :colspan => 2, :size  => 7, :padding => 1, :height => 14, :borders =>[]},
						 {:content => " ", :size  => 7, :padding => 1, :height => 14, :borders =>[]},
						 {:content => " ", :size  => 7, :padding => 1, :height => 14, :borders =>[:right]}], 

						[{:content => !familiar_hijo_1.nil? ? familiar_hijo_1.name_familiar : '', :colspan => 3, :size  => 7, :padding => 1, :height => 14, :borders =>[:left,:right,:bottom]},
						 {:content => !familiar_hijo_1.nil? ? (familiar_hijo_1.birthdate.strftime('%d-%m-%y')) : '', :size  => 7, :padding => 1, :align => :center, :height => 14, :borders =>[:left,:right,:bottom]},
						 {:content => !familiar_hijo_1.nil? ? familiar_hijo_1.education_familiar : '', :colspan => 2, :size  => 7, :padding => 1, :height => 14, :borders =>[:left,:right,:bottom]},
						 {:content => !familiar_hijo_1.nil? ? familiar_hijo_1.type_occupation.name : '', :size  => 7, :padding => 1, :height => 14, :borders =>[:left,:right,:bottom]},
						 {:content => !familiar_hijo_1.nil? ? familiar_hijo_1.phone_familiar : '', :size  => 7, :padding => 1, :height => 14, :borders =>[:left,:right,:bottom]}],

						[{:content => " ", :colspan => 3, :size  => 7, :font_style => :bold, :height => 14, :borders =>[:left]},
						 {:content => " ", :size  => 7, :padding => 1, :height => 14, :borders =>[]},
						 {:content => " ", :colspan => 2, :size  => 7, :padding => 1, :height => 14, :borders =>[]},
						 {:content => " ", :size  => 7, :padding => 1, :height => 14, :borders =>[]},
						 {:content => " ", :size  => 7, :padding => 1, :height => 14, :borders =>[:right]}], 

						[{:content => !familiar_hijo_2.nil? ? familiar_hijo_2.name_familiar : '',:colspan => 3, :size  => 7, :padding => 1, :height => 14, :borders =>[:left,:right,:bottom]},
						 {:content => !familiar_hijo_2.nil? ? (familiar_hijo_2.birthdate.strftime('%d-%m-%y')) : '', :size  => 7, :padding => 1, :height => 14, :align => :center, :borders =>[:left,:right,:bottom]},
						 {:content => !familiar_hijo_2.nil? ? familiar_hijo_2.education_familiar : '', :colspan => 2, :size  => 7, :padding => 1, :height => 14, :borders =>[:left,:right,:bottom]},
						 {:content => !familiar_hijo_2.nil? ? familiar_hijo_2.type_occupation.name : '', :size  => 7, :padding => 1, :height => 14, :borders =>[:left,:right,:bottom]},
						 {:content => !familiar_hijo_2.nil? ? familiar_hijo_2.phone_familiar : '', :size  => 7, :padding => 1, :height => 14, :borders =>[:left,:right,:bottom]}],
						
						[{:content => " ", :colspan => 3, :size  => 7, :font_style => :bold, :height => 14, :borders =>[:left]},
						 {:content => " ", :size  => 7, :height => 14, :borders =>[]},
						 {:content => " ", :colspan => 2, :size  => 7, :height => 14, :borders =>[]},
						 {:content => " ", :size  => 7, :height => 14, :borders =>[]},
						 {:content => " ", :size  => 7, :height => 14, :borders =>[:right]}], 

						[{:content => !familiar_hijo_3.nil? ? familiar_hijo_3.name_familiar : '', :colspan => 3, :size  => 7, :padding => 1, :height => 14, :borders =>[:left,:right,:bottom]},
						 {:content => !familiar_hijo_3.nil? ? (familiar_hijo_3.birthdate.strftime('%d-%m-%y')) : '', :size  => 7, :padding => 1, :height => 14, :align => :center, :borders =>[:left,:right,:bottom]},
						 {:content => !familiar_hijo_3.nil? ? familiar_hijo_3.education_familiar : '', :colspan => 2, :size  => 7, :padding => 1, :height => 14, :borders =>[:left,:right,:bottom]},
						 {:content => !familiar_hijo_3.nil? ? familiar_hijo_3.type_occupation.name : '', :size  => 7, :padding => 1, :height => 14, :borders =>[:left,:right,:bottom]},
						 {:content => !familiar_hijo_3.nil? ? familiar_hijo_3.phone_familiar : '', :size  => 7, :padding => 1, :height => 14, :borders =>[:left,:right,:bottom]}]					   
					]

						stroke do
							#HUELLA
							rectangle [395,761],80, 100
							rectangle [395,185],80, 105
						end	

					move_down 10
					table [
						[{:content => "AUTORIZACION DE DESCUENTO", :colspan => 8, :height => 16, :align => :center, :size => 9, :padding => 2, :font_style => :bold,
						  :width => 580}],

						[{:content => "Por medio de la presente me permito comunicarles que he decidico afiliarme al Fondo de Empleados del Banco de Bogotá, en consecuencia
						  me comprometo a aceptar los estatutos, reglamentos y disposiciones en general de la entidad. Autorizo expresamente a mi empleador para realizar las
						  deducciones sobre mis ingresos laborales, para las cuotas de ahorros y aportes, creditos y todos aquellos descuentos reglamentarios, de acuerdo a 
						  sus estatutos y decisiones de la junta directiva; sumas que solicito entregar al mencionado Fondo.",
						  :colspan => 8, :size => 8, :align => :justify, :height => 50}]  		  
					]

					move_down 10
					table [
						[{:content => "DECLARACION VOLUNTARIA DE ORIGEN DE FONDOS", :colspan => 8, :height => 16, :align => :center, :size => 9, :padding => 2, :font_style => :bold,
						  :width => 580}],

						[{:content => "Bajo la gravedad de juramento y actuando en nombre propio realizo la siguiente declaracion de origen y destinacion de recursos al fondo de Empleados del banco de Bogotá,
						 con el fin de cumplir con las disposiciones señaladas en su Sistema de Administración del Riesgo de Lavado de Activos y de la Financiacion del terrorismo: 1. Declaro que los activos, 
						 ingresos, bienes y demas rescursos provienen de actividades legales conforme a lo descrito en mi actividad y ocupacion. 2. No admitiré que terceros vinculen mi actividad con dineros, 
						 recursos o activos relacionados con el delito de lavado de activos o destinados a la financiacion del terrorismo. 3. Eximo al Fondo de Empleados Banco de Bogotá, de toda 
						 responsabilidad que se derive del comportamiento o el que se ocacione por la informacion falsa o errónea suministrada en la presente declaracion y en los documentos que respaldan o
						 soporten mis afirmaciones. 4. Autorizo al Fondo de Empleados Banco de Bogotá para que verifique y realuice las consultas que estime necesarias con el proposito de confirmar la 
						 informacion registrada en el formulario. 5. Los recursos que utilizo para realizar los pagos e inversiones en el Fondo de Empleados Banco de Bogotá tienen procedencia lícita y están 
						 soportados con el desarrollo de actividades legítimas. 6. No he sido, ni me encuentro incluido en investigaciones relacionadas con Lavado de Activos o Financiacion de Terrorismo. 
						 7. Estoy informado de mi obligacion de actualizar con una periodicidad minimo anual, la informacion que solicite la entidad por cada producto o servicio que utilice, suministrando 
						 la información documental exigida por el Fondo de Empleados Banco de Bogotá para dar cumplimiento a la normatividad vigente.",
						  :colspan => 8, :size => 7, :align => :justify, :height => 100}]  
					]
					
					table [
						[{:content => "COMO CONSTANCIA DE HABER LEIDO, ENTENDIDO Y ACEPTADO LO ANTERIOR, DECLARO QUE LA INFORMACION QUE HE SUMINISTRADO \n ES EXACTA EN TODAS SUS PARTES Y FIRMO EL PRESENTE DOCUMENTO", 
						  :colspan => 8, :size => 6, :font_style => :bold, :align => :center, :width => 580, :height => 25, :borders =>[]}],
						[{:content => " ", :colspan => 8, :height => 25, :borders =>[]}],  

						[{:content => " ", :colspan => 8, :height => 25, :borders =>[]}], 
						[{:content => " ", :colspan => 8, :height => 25, :borders =>[]}],
						[{:content => " ", :colspan => 8, :height => 25, :borders =>[]}],

						[{:content => " ", :height => 25, :padding => 5, :borders =>[]},
						 {:content => "_____________________________________________________________", :colspan => 4, :size => 6, :padding => 5, 
						  :align => :center, :height => 25, :borders =>[]},
					     {:content => " ", :colspan => 3, :height => 25, :borders =>[]}],

						[{:content => " ", :height => 25, :padding => -10, :borders =>[:bottom]},
						 {:content => "FIRMA Y CEDULA DEL ASOCIADO", :colspan => 4, :size => 7, :padding => -10, :font_style => :bold, 
						  :align => :center, :height => 25, :borders =>[:bottom]},
					     {:content => "Huella", :size => 7, :padding => -10, :align => :right, :font_style => :bold, :height => 25, 
					      :borders =>[:bottom]},
					     {:content => " ", :colspan => 3, :padding => -10, :height => 25, :borders =>[:bottom]}] 
					]					

					start_new_page
					table [
						[{:content => "NOTA", :colspan => 8, :height => 16, :align => :center, :size => 9, :padding => 2, :font_style => :bold,
						  :width => 580}],

						[{:content => "Por favor diligenciar, imprimir y firmar esta solicitud, adems anexar fotocopia de la cédula de ciuddania ampliada 150% y remitir estos documentos a las oficinas",
						  :colspan => 8, :size => 8, :align => :center, :height => 30}]  
					]

					move_down 10
					table [
						[{:content => "PARA USO EXCLUSIVO DEL FONDO DE EMPLEADOS DEL BANCO DE BOGOTÁ VERIFICACION DE LA INFORMACION", 
						  :colspan => 8, :height => 16, :align => :center, :size => 9, :padding => 2, :font_style => :bold, :width => 580}],
  						
  						[{:content => "", :colspan => 8, :height => 17, :borders => [:left, :right]}],
  						[{:content => "Fecha de verificacion", :colspan => 3, :size => 7, :padding => 5, :font_style => :bold, :height => 16, :align => :center, :borders => [:left]},
  						 {:content => " ", :size => 7, :height => 16, :borders => []}, 
  						 {:content => "Funcionario encargado de verificar informacion", :colspan => 4, :size => 7, :font_style => :bold, 
  						  :height => 15, :borders => [:right]}],

  						[{:content => "Dia", :size => 7, :font_style => :bold, :height => 16, :align => :right, :borders => [:left]},
  						 {:content => "Mes", :size => 7, :font_style => :bold, :height => 16, :align => :center, :borders => []},
  						 {:content => "Año", :size => 7, :font_style => :bold, :height => 16, :align => :left, :borders => []},
  						 {:content => "Hora", :colspan => 1, :size => 7, :font_style => :bold, :height => 16, :borders => []}, 
  						 {:content => " ", :colspan => 4, :height => 16, :borders => [:right]}],

  						[{:content => " ", :colspan => 8, :height => 17, :borders => [:left,:right]}], 
  						[{:content => " ", :colspan => 4, :height => 17, :borders => [:left]},
  						 {:content => "Cargo", :colspan => 4, :size => 7, :font_style => :bold, :height => 16, :borders => [:right]}],
  						[{:content => " ", :colspan => 8, :height => 17, :borders => [:left,:right]}], 
  						
  						[{:content => " ", :colspan => 4, :height => 17, :borders => [:left]},
  						 {:content => "Firma", :colspan => 4, :size => 7, :font_style => :bold, :height => 16, :borders => [:right]}],
  						[{:content => " ", :colspan => 8, :height => 17, :borders => [:left,:right]}], 

  						[{:content => " ", :colspan => 4, :height => 17, :borders => [:left]},
  						 {:content => "_______________________________", :colspan => 4, :size => 8, :font_style => :bold, :height => 17, :borders => [:right]}],
  						[{:content => " ", :colspan => 8, :height => 17, :borders => [:left,:right]}], 
  						[{:content => " ", :colspan => 8, :height => 17, :borders => [:left,:right]}], 

  						[{:content => "APROBACION DE LA AFILIACION", :colspan => 3, :size => 7, :font_style => :bold, :height => 16, :align => :center, :borders => [:left]},
  					     {:content => "AFILIADO A PARTIR DE", :colspan => 3, :size => 7, :font_style => :bold, :height => 16, :align => :center, :borders => []},
  					     {:content => "APROBADO POR", :colspan => 2, :size => 7, :font_style => :bold, :height => 16, :align => :center, :borders => [:right]}],

  					    [{:content => " ", :colspan => 3, :size => 7, :height => 16, :borders => [:left]},
  					     {:content => "dd/mm/aa", :colspan => 3, :size => 6, :text_color => "808B96", :padding => -1, :font_style => :bold, :height => 13, :align => :center, :borders => []},
  					     {:content => " ", :colspan => 2, :size => 7, :borders => [:right]}],

  					    [{:content => "SI                        NO", :colspan => 3, :size => 7, :font_style => :bold, :height => 16, :align => :center, :borders => [:left]},
  					     {:content => "________________________________", :colspan => 3, :size => 7, :font_style => :bold, :height => 16, :align => :center, :borders => []},
  					     {:content => "________________________________", :colspan => 2, :size => 7, :font_style => :bold, :height => 16, :align => :center, :borders => [:right]}],
  					     [{:content => " ", :colspan => 8, :height => 17, :borders => [:left,:right]}], 

  					    [{:content => "Observaciones", :size => 7, :font_style => :bold, :height => 16, :align => :center, :borders => [:left]},
  					     {:content => "   ____________________________________________________________________________________________________________________________________________", 
  					      :colspan => 7, :size => 7, :height => 16, :borders => [:right]}], 

  					    [{:content => " ", :size => 7, :font_style => :bold, :height => 16, :align => :center, :borders => [:left]},
  					     {:content => "   ____________________________________________________________________________________________________________________________________________", 
  					      :colspan => 7, :size => 7, :height => 16, :borders => [:right]}],   
  					    [{:content => " ", :colspan => 8, :height => 16, :borders => [:bottom,:left,:right]}],  
					]
						stroke do	 
							#DIA
	  						rectangle [45, 655], 45, 15
	  						#MES
	  						rectangle [90, 655], 45, 15
	  						#AÑO
	  						rectangle [135, 655], 45, 15
	  						#HORA
	  						rectangle [205, 655], 55, 15
	  						#FUNCION
	  						rectangle [292, 668], 250, 15
	  						#CARGO
	  						rectangle [292, 620], 250, 15
	  						#APROBACION
	  						rectangle [100, 483], 13, 13
	  						rectangle [150, 483], 13, 13
	  					end	

	  					bounding_box [bounds.left, bounds.bottom + 23], :width => bounds.width do
	  					number_pages "FE-001-01-2018", :align => :right, :size => 6, :color => "273746", :at => [1, 4]
	  				end 	
				end
					send_data pdf.render, 
					filename: 'formulario.pdf', 
					type: 'application/pdf'
				end	
		end 
	end	
end	
