class Content < ApplicationRecord
	belongs_to :type_content
	mount_uploader :image, ContentUploader
	
	validates :image, :presence  => { message: "Debe Agregar una Imagen"}, on: :create
	validates :description, :presence  => { message: "Debe agregar una descripcion"}   
end
 