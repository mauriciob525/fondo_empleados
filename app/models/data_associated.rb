class DataAssociated < ApplicationRecord
	has_many :info_works, dependent: :destroy
	has_many :families, dependent: :destroy
	has_many :financials, dependent: :destroy
	has_many :foreigns, dependent: :destroy

	#belongs_to :place_birth, :class_name => "City"
	
	belongs_to :type_format
	belongs_to :type_status 
	belongs_to :type_gender
	belongs_to :city
	belongs_to :departament
	belongs_to :type_home
	
	validates :date_filling, :presence => {message: "Fecha de diligenciamiento"}
	validates :type_format_id, :presence => {message:"Debe elegir un formato"}
	validates :name, :presence => {message: "Debe ingresar sus nombres"}
	validates :lastname, :presence => {message: "Debe ingresar sus apellidos" }
	validates :document, :presence => {message: "Debe ingresar su numero de documento" }
	validates :type_status_id, :presence => {message:"Debe elegir su estado civil"}
	validates :type_gender_id, :presence => {message:"Debe elegir su genero"}
	validates :place_birth, :presence => {message: "Debe seleccionar la ciudad de nacimiento"}
	validates :birthdate, :presence => {message: "Debe elegir la fecha de nacimiento"}
	validates :email, :presence => {message: "Debe ingresar su email personal" }
	validates :education, :presence => {message: "Debe ingresar su nivel de estudios" }
	validates :profession, :presence => {message: "Debe ingresar su profesion" }
	validates :address, :presence => {message: "Debe ingresar la direccion de residencia" }
	validates :city_id, :presence => {message: "Debe elegir la ciudad de residencia"}
	validates :departament_id, :presence => {message: "Debe elegir el departamento de residencia"}
	validates :phone, :presence => {message: "Debe ingresar un numero de telefono" }
	validates :mobile, :presence => {message: "Debe ingresar un numero movil" }
	validates :type_home_id, :presence => {message: "Debe elegir un tipo de residencia"}
end
