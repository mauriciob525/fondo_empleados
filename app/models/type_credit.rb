class TypeCredit < ApplicationRecord
	belongs_to :type_credit_employed
  	mount_uploader :image, NewUploader

  	def self.getTipos
  		TypeCredit.find_by_sql("(SELECT id, name, image, IF(type_credit_employed_id = 1 , 2, 3) type_id, active FROM type_credits) 
  								ORDER BY type_id, name ASC")
  	end
end
