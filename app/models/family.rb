class Family < ApplicationRecord
	belongs_to :type_family
	belongs_to :type_occupation
	belongs_to :data_associated

	validates :name_familiar, :presence  => { message: "Debe Agregar nombres y apellidos de un familiar"}
	validates :birthdate, :presence  => { message: "Debe Agregar fecha de nacimiento del familiar"}
 	validates :education_familiar, :presence  => { message: "Debe Agregar un nivel de estudios del familiar"}
 	validates :type_occupation_id, :presence => {message: "Debe elegir la ocupacion del familiar"}
	validates :phone_familiar, :presence  => { message: "Debe Agregar un numero de telefono del familiar"}
	validates :type_family_id, :presence => {message: "Debe elegir el tipo de familiar "}
	#validate  :validar_padres

	#def validar_padres
		#if type_family_id != 3
			#familiar = Family.where(type_family_id: type_family_id)
			#if familiar.count>0
				#errors.add(:type_family_id, 'Solo puede agregar uno de '+familiar.take.type_family.name)
			#end
		#end
	#end
  
end	