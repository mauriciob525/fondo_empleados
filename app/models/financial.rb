class Financial < ApplicationRecord
	belongs_to :type_financial
	#belongs_to :data_associated

	validates :ingress, :presence  => { message: "Debe agregar los ingresos mensuales"}
	validates :egress, :presence  => { message: "Debe agregar los egresos mensuales"}
 	validates :assets, :presence  => { message: "Debe agregar el total de activos "}
	validates :liabilities, :presence  => { message: "Debe agregar el total de pasivos"}
	validates :other_ingress, :presence  => { message: "Debe agregar otros ingresos"}
	validates :description, :presence  => { message: "Debe agregar una descripcion de otros ingresos"}
end
