class New < ApplicationRecord
	mount_uploader :image, NewUploader

	validates :title, :presence => { message: 'Debe agregar un titulo'}
	validates :short_description, :presence => { message: "Debe agregar una descripcion corta"} 
	validates :image, :presence  => { message: "Debe Agregar una Imagen"}, on: :create
	validates :description, :presence  => { message: "Debe agregar una descripcion"}
	#validates :link, :presence  => { message: "Debe agregar un link"}	
end
