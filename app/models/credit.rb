class Credit < ApplicationRecord
	belongs_to :type_credit
	mount_uploader :image, CreditUploader
	
	validates :image, :presence  => { message: "Debe Agregar una Imagen"}, on: :create 
	validates :description, :presence  => { message: "Debe agregar una descripcion"} 	

	def self.creditType(type_credit_employed_id)
		Credit.select("credits.*").
		joins(:type_credit).where('type_credits.type_credit_employed_id=?',type_credit_employed_id)
	end

	def self.getCreditosByType(type_credit_employed_id)
		Credit.select("credits.*,type_credits.name").
		joins(:type_credit).where('type_credits.type_credit_employed_id=?',type_credit_employed_id)
	end 

	  
end 
