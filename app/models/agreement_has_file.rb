class AgreementHasFile < ApplicationRecord
  belongs_to :type_file
  mount_uploader :url, AgreementHasFileUploader

end
