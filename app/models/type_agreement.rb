class TypeAgreement < ApplicationRecord
  	mount_uploader :image, NewUploader

  	def self.getTipos
  		TypeAgreement.find_by_sql("(SELECT id, name, image, 1 type_id, active FROM type_agreements)
									
									ORDER BY type_id, name ASC")
  	end
end