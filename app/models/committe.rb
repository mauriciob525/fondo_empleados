class Committe < ApplicationRecord
	validates :title, :presence => { message: "Debe agregar un titulo"} 
	validates :sub_title, :presence => { message: "Debe agregar un subtitulo"}
	validates :description, :presence  => { message: "Debe agregar una descripcion"}	
end
