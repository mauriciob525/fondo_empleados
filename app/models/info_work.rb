class InfoWork < ApplicationRecord
	belongs_to :type_occupation
	belongs_to :type_salary
	belongs_to :type_deal
	belongs_to :type_payment
	#belongs_to :data_associated

	validates :type_occupation_id, :presence => {message: " Debe elegir una ocupacion"} 
	validates :date_start, :presence  => { message: "Debe Agregar fecha de inicio"}
	validates :economic_act, :presence  => { message: "Debe Agregar actividad economica"}
	validates :cod_ciiu, :presence  => { message: "Debe Agregar codigo CIIU"}
	validates :company, :presence => {message: "Debe Agregar el nombre de la empresa"}
	validates :cod_area, :presence  => { message: "Debe Agregar codigo de area"}
	validates :area_of, :presence => {message: "Debe agregrar seccion oficina"}
	validates :region, :presence  => { message: "Debe Agregar region"}
	validates :phone_laboral, :presence  => { message: "Debe Agregar telefono"}
	validates :ext, :presence  => { message: "Debe Agregar extension"}
	validates :position, :presence  => { message: "Debe Agregar cargo"}
	validates :email_laboral, :presence  => { message: "Debe Agregar email corporativo"}
	validates :salary, :presence  => { message: "Debe Agregar sueldo basico"}
	validates :type_salary_id, :presence => {message: "Debe elegir un tipo de salario "}
	validates :type_deal_id, :presence => {message: "Debe elegir un tipo de contrato"}
	validates :type_payment_id, :presence => {message: "Debe elegir una forma de abono"}
	validates :num, :presence  => { message: "Debe Agregar numero"}
	validates :bank, :presence  => { message: "Debe Agregar nombre del banco"}
end
