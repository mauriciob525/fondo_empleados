class Agreement < ApplicationRecord
  belongs_to :type_agreement
  mount_uploader :image, AgreementUploader

  	validates :title, :presence => { message: "Debe agregar un titulo"}
	#validates :description, :presence  => { message: "Debe agregar una descripcion"}
	#validates :discount, :presence  => { message: "Debe agregar un descuento"}
	validates :image, :presence  => { message: "Debe Agregar una Imagen"}, on: :create
	
end
