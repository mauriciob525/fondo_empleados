// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//

//= require rails-ujs
//= require activestorage
//= require jquery
//= require jquery-ui


//= require ckeditor/init
//= require_tree ./vendor/

var slickConter = 0;
var droppedFiles = [];

$(document).ready(function(){

	$('.list-drag-images').slick({
		slidesToShow: 5,
		slidesToScroll: 1,
		infinite: false
	});

	$('form').validate({
		ignore: [],
		errorPlacement: function(error, element) {

			if(element.prop('required')){
				if (element.attr("name") == "description") {
					$('.error-description').append(error)

				} else {
					error.insertAfter(element);
				}
			}
			
		},
		rules: { 
			// description:{
			// 	//required: true,
			// }
		},
		submitHandler: function(form){
			$(form).find('[type="submit"]').prop('disabled', true)
			if($(form).attr('id') == "create_agreement"){

				var ajaxData = new FormData(form);
				var $input = $('#images');
				var $form = $(form);

				$.each( droppedFiles, function(i, file) {
					ajaxData.append( $input.attr('name'), file );
				});

				$.ajax({
					url: $form.attr('action'),
					type: $form.attr('method'),
					data: ajaxData,
					dataType: 'json',
					cache: false,
					contentType: false,
					processData: false,
					complete: function(data) {
						window.location.href = '/administrador'
					}
				});


			}else{
				form.submit();
			}
		}
		
	});	



	for (var i in CKEDITOR.instances) {
		CKEDITOR.instances[i].on('change', function() { CKEDITOR.instances[i].updateElement() });
	}

	
	var isAdvancedUpload = function() {
		var div = document.createElement('div');
		return (('draggable' in div) || ('ondragstart' in div && 'ondrop' in div)) && 'FormData' in window && 'FileReader' in window;
	}();

	if(isAdvancedUpload){
		var form = $('.box');
		form.addClass('has-drag-upload');

		

		form.on('drag dragstart dragend dragover dragenter dragleave drop', function(e) {
			e.preventDefault();
			e.stopPropagation();
		})
		.on('dragover dragenter', function() {
			form.addClass('is-dragover');
		})
		.on('dragleave dragend drop', function() {
			form.removeClass('is-dragover');
		})
		.on('drop', function(e) {
			droppedFiles =droppedFiles.concat(toArray(e.originalEvent.dataTransfer.files) );
			appendImagesToList(e.originalEvent.dataTransfer.files);

			
		});
	}

	$('body').on('click', '.delete-item', function(event){
		var slideIndex = $(this).parent('li').attr('data-slick-index');
		$('.list-drag-images').slick('slickRemove',slideIndex);

		var j = 0;
		$("li.slick-slide").each(function(){
			$(this).attr("data-slick-index",j);
			j++;
		})

		var id = $(this).parent('li').attr('data-id');
		if (id) {
			$.ajax({
		      type: "POST",
		      url: '/delete_img_convenios',
		      data: {id:id},
		      dataType: "json"
		    });
		}
	});

	$('.input-image input').change(function(){
		var file = this.files[0]
		var img = readImages(file);

		$('.input-image img').remove()
		$('.input-image').append(img)
	});

	$('.box-input-images input').change(function(){
		droppedFiles = droppedFiles.concat(toArray(this.files));
		appendImagesToList(this.files);
		this.value = null;
	});

})




function readImages(file){
	var reader = new FileReader();
	var image = new Image();

	reader.onloadend = function(){
		image.src = reader.result;
	}

	if(file)
		reader.readAsDataURL(file)

	return image;
}

function appendImagesToList(images){

	var ul = $('.list-drag-images');
	for(var img of images){
		var li = $('<li><span class="delete-item">BORRAR</span></li>')
		li.append(readImages(img));
		ul.slick('slickAdd', li);
		slickConter++
	}
}


function toArray(fileList) {
	return Array.prototype.slice.call(fileList);
}





