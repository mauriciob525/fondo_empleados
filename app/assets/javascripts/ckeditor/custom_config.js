if (typeof CKEDITOR !== 'undefined') {
  CKEDITOR.editorConfig = function (config) {

    config.toolbar_mini = [
    ["Font", "FontSize" , "-", "Bold",  "Italic",  "Underline",  "Strike","-",
    "JustifyLeft", "JustifyCenter", "JustifyRight", "JustifyBlock" , "-",
    "TextColor", "BGColor","-", "NumberedList" , "BulletedList", "-", "Cut", "Copy", "Paste", 
    , "-", "Undo" , "Redo" , "-"],
    ];
    config.extraPlugins = 'wordcount,notification';
    config.wordcount = {
        showCharCount: true,
        showWordCount: false,
        showParagraphs: false, 
        maxCharCount: 250,

    };
    config.format_tags = 'p;h1;h2;h3;h4;h5;h6;pre;address;div';
    config.toolbar = "mini";
    config.height = 300;
  }

 } 

