Rails.application.routes.draw do
  devise_for :admins, controllers: { sessions: 'admin/sessions' }
  get 'admin/index'
  get 'index/index'
  get 'pdf/getPdfFormulario'
  namespace :api , defaults: {format: 'json'} do
  	get 'getNovedades', to: 'web_services#getNovedades'
  	get 'getCreditos', to: 'web_services#getCreditos'
  	get 'getConvenios', to: 'web_services#getConvenios'
  	get 'getConvenio/:id', to: 'web_services#getConvenio'
    get 'getContenidos', to: 'web_services#getContenidos'
    get 'getComites', to: 'web_services#getComites'
    get 'getServicios', to: 'web_services#getServicios'
    get 'getBeneficios', to: 'web_services#getBeneficios'
    get 'getVinculate', to: 'web_services#getVinculate'
    get 'getContactos', to: 'web_services#getContactos'
    get 'getOpiniones', to: 'web_services#getOpiniones'
    get 'getFormulario', to: 'web_services#getFormulario'
    get 'getPdfFormulario', to: 'web_services#getPdfFormulario'
    get 'getDocumentos', to: 'web_services#getDocumentos'
    get 'getInfo', to: 'web_services#getInfo'
    post 'form', to: 'web_services#form'
  end
  
  get '/administrador', to: "admin#index"  
  get '/form', to: "index#index" 
  get '/getPdfFormulario', to: "pdf#getPdfFormulario"

  

  #NOVEDADES
  get '/crear_novedad', to: "admin#crear_novedad"
  get '/editar_novedad/:id', to: "admin#crear_novedad"
  post '/new_novedad', to: "admin#new_novedad"
  get '/delete_novedad/:id', to: "admin#delete_novedad"
  get '/active_novedad/:id', to: "admin#active_novedad"

  #CONVENIOS
  get '/crear_convenio', to: "admin#crear_convenio"
  get '/editar_convenio/:id', to: "admin#crear_convenio"
  post '/new_convenio', to: "admin#new_convenio"
  get '/delete_convenio/:id', to: "admin#delete_convenio"
  get '/active_convenio/:id', to: "admin#active_convenio"

  #CREDITOS
  get '/crear_credito/:tipo', to: "admin#crear_credito"
  get '/editar_credito/:tipo/:id', to: "admin#crear_credito"
  post '/new_credito', to: "admin#new_credito"
  get '/delete_credito/:id', to: "admin#delete_credito"
  get '/active_credito/:id', to: "admin#active_credito"

  #CONTENIDO
  get '/crear_contenido/:tipo', to: "admin#crear_contenido"
  get '/editar_contenido/:tipo/:id', to: "admin#crear_contenido"
  post '/new_contenido', to: "admin#new_contenido"
  get '/delete_contenido/:id', to: "admin#delete_contenido"   
  get '/active_contenido/:id', to: "admin#active_contenido"   
  get '/editar_documento/:id', to: "admin#editar_documento"   
  post '/update_file', to: "admin#update_file"   

  #JUNTA Y COMITE
  get '/crear_comite', to: "admin#crear_comite"
  get '/editar_comite/:id', to: "admin#crear_comite"
  post '/new_comite', to: "admin#new_comite"
  get '/delete_comite/:id', to: "admin#delete_comite" 
  get '/active_comite/:id', to: "admin#active_comite" 

  #TIPOS CREDITOS Y CONVENIOS
  get '/crear_tipos', to: "admin#crear_tipos"
  get '/editar_tipo/:id/:tipo', to: "admin#crear_tipos"
  post '/new_tipos', to: "admin#new_tipos"
  get '/delete_tipos/:id', to: "admin#delete_tipos"
  post '/delete_img_convenios', to: "admin#delete_img_convenios"
  get '/active_tipo/:id/:tipo', to: "admin#active_tipo"

  #CONTACTO
  get '/crear_contacto', to: "admin#crear_contacto"
  get '/editar_contacto/:id', to: "admin#crear_contacto"
  post '/new_contacto', to: "admin#new_contacto"
  get '/delete_contacto/:id', to: "admin#delete_contacto" 
  #OPINION
  get '/crear_opinion', to: "admin#crear_opinion"
  get '/editar_opinion/:id', to: "admin#crear_opinion"
  post '/new_opinion', to: "admin#new_opinion"
  get '/delete_opinion/:id', to: "admin#delete_opinion" 
  
end