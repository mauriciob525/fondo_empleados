(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule, routableComponents */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routableComponents", function() { return routableComponents; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _servicios_servicios_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./servicios/servicios.component */ "./src/app/servicios/servicios.component.ts");
/* harmony import */ var _beneficios_beneficios_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./beneficios/beneficios.component */ "./src/app/beneficios/beneficios.component.ts");
/* harmony import */ var _seguridad_seguridad_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./seguridad/seguridad.component */ "./src/app/seguridad/seguridad.component.ts");
/* harmony import */ var _contactanos_contactanos_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./contactanos/contactanos.component */ "./src/app/contactanos/contactanos.component.ts");
/* harmony import */ var _convenios_convenios_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./convenios/convenios.component */ "./src/app/convenios/convenios.component.ts");
/* harmony import */ var _vinculate_vinculate_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./vinculate/vinculate.component */ "./src/app/vinculate/vinculate.component.ts");
/* harmony import */ var _conocenos_conocenos_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./conocenos/conocenos.component */ "./src/app/conocenos/conocenos.component.ts");
/* harmony import */ var _pagina_convenio_pagina_convenio_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./pagina-convenio/pagina-convenio.component */ "./src/app/pagina-convenio/pagina-convenio.component.ts");
/* harmony import */ var _formulario_formulario_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./formulario/formulario.component */ "./src/app/formulario/formulario.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












var routes = [
    {
        path: '',
        children: [
            {
                path: '', component: _home_home_component__WEBPACK_IMPORTED_MODULE_2__["HomeComponent"],
                data: {
                    module: 'home',
                    pages: 'menu'
                }
            },
            {
                path: 'servicios', component: _servicios_servicios_component__WEBPACK_IMPORTED_MODULE_3__["ServiciosComponent"],
                data: {
                    module: 'servicios',
                    pages: 'menu'
                }
            },
            {
                path: 'beneficios', component: _beneficios_beneficios_component__WEBPACK_IMPORTED_MODULE_4__["BeneficiosComponent"],
                data: {
                    module: 'beneficios',
                    pages: 'menu'
                }
            },
            {
                path: 'conocenos', component: _conocenos_conocenos_component__WEBPACK_IMPORTED_MODULE_9__["ConocenosComponent"],
                data: {
                    module: 'conocenos',
                    pages: 'menu'
                }
            },
            {
                path: 'convenios', component: _convenios_convenios_component__WEBPACK_IMPORTED_MODULE_7__["ConveniosComponent"],
                data: {
                    module: 'convenios',
                    pages: 'menu'
                }
            },
            {
                path: 'vinculate', component: _vinculate_vinculate_component__WEBPACK_IMPORTED_MODULE_8__["VinculateComponent"],
                data: {
                    module: 'vinculate',
                    pages: 'menu'
                }
            },
            {
                path: 'seguridad', component: _seguridad_seguridad_component__WEBPACK_IMPORTED_MODULE_5__["SeguridadComponent"],
                data: {
                    module: 'seguridad',
                    pages: 'menu'
                }
            },
            {
                path: 'contactanos', component: _contactanos_contactanos_component__WEBPACK_IMPORTED_MODULE_6__["ContactanosComponent"],
                data: {
                    module: 'contactanos',
                    pages: 'menu'
                }
            },
            {
                //funciona!
                path: 'convenios/:Title', component: _pagina_convenio_pagina_convenio_component__WEBPACK_IMPORTED_MODULE_10__["PaginaConvenioComponent"],
                data: {
                    module: 'paginaConvenios',
                    page: 'menu'
                }
            },
            {
                //funciona!
                path: 'formulario', component: _formulario_formulario_component__WEBPACK_IMPORTED_MODULE_11__["FormularioComponent"],
                data: {
                    module: 'vinculate',
                    page: 'menu'
                }
            },
        ]
    }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes, {})],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());

var routableComponents = [
    _home_home_component__WEBPACK_IMPORTED_MODULE_2__["HomeComponent"],
    _servicios_servicios_component__WEBPACK_IMPORTED_MODULE_3__["ServiciosComponent"],
    _beneficios_beneficios_component__WEBPACK_IMPORTED_MODULE_4__["BeneficiosComponent"],
    _seguridad_seguridad_component__WEBPACK_IMPORTED_MODULE_5__["SeguridadComponent"],
    _contactanos_contactanos_component__WEBPACK_IMPORTED_MODULE_6__["ContactanosComponent"],
    _convenios_convenios_component__WEBPACK_IMPORTED_MODULE_7__["ConveniosComponent"],
    _vinculate_vinculate_component__WEBPACK_IMPORTED_MODULE_8__["VinculateComponent"],
    _conocenos_conocenos_component__WEBPACK_IMPORTED_MODULE_9__["ConocenosComponent"],
    _pagina_convenio_pagina_convenio_component__WEBPACK_IMPORTED_MODULE_10__["PaginaConvenioComponent"],
];


/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\n<router-outlet></router-outlet>\n<app-pie></app-pie>"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'FondoEmpleadosBDB';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _home_banner_home_banner_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./home-banner/home-banner.component */ "./src/app/home-banner/home-banner.component.ts");
/* harmony import */ var _home_convenios_home_convenios_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./home-convenios/home-convenios.component */ "./src/app/home-convenios/home-convenios.component.ts");
/* harmony import */ var _home_opiniones_home_opiniones_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./home-opiniones/home-opiniones.component */ "./src/app/home-opiniones/home-opiniones.component.ts");
/* harmony import */ var _home_contactanos_home_contactanos_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./home-contactanos/home-contactanos.component */ "./src/app/home-contactanos/home-contactanos.component.ts");
/* harmony import */ var _home_popup_home_popup_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./home-popup/home-popup.component */ "./src/app/home-popup/home-popup.component.ts");
/* harmony import */ var _servicios_servicios_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./servicios/servicios.component */ "./src/app/servicios/servicios.component.ts");
/* harmony import */ var _beneficios_beneficios_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./beneficios/beneficios.component */ "./src/app/beneficios/beneficios.component.ts");
/* harmony import */ var _seguridad_seguridad_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./seguridad/seguridad.component */ "./src/app/seguridad/seguridad.component.ts");
/* harmony import */ var _contactanos_contactanos_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./contactanos/contactanos.component */ "./src/app/contactanos/contactanos.component.ts");
/* harmony import */ var _convenios_convenios_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./convenios/convenios.component */ "./src/app/convenios/convenios.component.ts");
/* harmony import */ var _vinculate_vinculate_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./vinculate/vinculate.component */ "./src/app/vinculate/vinculate.component.ts");
/* harmony import */ var _conocenos_conocenos_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./conocenos/conocenos.component */ "./src/app/conocenos/conocenos.component.ts");
/* harmony import */ var _shared_header_header_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./shared/header/header.component */ "./src/app/shared/header/header.component.ts");
/* harmony import */ var _shared_pie_pie_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./shared/pie/pie.component */ "./src/app/shared/pie/pie.component.ts");
/* harmony import */ var _shared_directives_images_sizes_square_directive__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./shared/directives/images-sizes-square.directive */ "./src/app/shared/directives/images-sizes-square.directive.ts");
/* harmony import */ var _shared_directives_images_sizes_banner_directive__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./shared/directives/images-sizes-banner.directive */ "./src/app/shared/directives/images-sizes-banner.directive.ts");
/* harmony import */ var _shared_directives_square_div_directive__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./shared/directives/square-div.directive */ "./src/app/shared/directives/square-div.directive.ts");
/* harmony import */ var _shared_creditos_slider_creditos_slider_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./shared/creditos-slider/creditos-slider.component */ "./src/app/shared/creditos-slider/creditos-slider.component.ts");
/* harmony import */ var _pagina_convenio_pagina_convenio_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./pagina-convenio/pagina-convenio.component */ "./src/app/pagina-convenio/pagina-convenio.component.ts");
/* harmony import */ var _shared_convenios_page_convenios_page_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./shared/convenios-page/convenios-page.component */ "./src/app/shared/convenios-page/convenios-page.component.ts");
/* harmony import */ var _formulario_formulario_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./formulario/formulario.component */ "./src/app/formulario/formulario.component.ts");
/* harmony import */ var _services_service_manager_service__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./services/service-manager.service */ "./src/app/services/service-manager.service.ts");
/* harmony import */ var _pipes_html_safe_pipe__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./pipes/html-safe.pipe */ "./src/app/pipes/html-safe.pipe.ts");
/* harmony import */ var _shared_radio_group_radio_group_component__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./shared/radio-group/radio-group.component */ "./src/app/shared/radio-group/radio-group.component.ts");
/* harmony import */ var ngx_slick__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ngx-slick */ "./node_modules/ngx-slick/ngx-slick.umd.js");
/* harmony import */ var ngx_slick__WEBPACK_IMPORTED_MODULE_31___default = /*#__PURE__*/__webpack_require__.n(ngx_slick__WEBPACK_IMPORTED_MODULE_31__);
/* harmony import */ var ngx_device_detector__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ngx-device-detector */ "./node_modules/ngx-device-detector/ngx-device-detector.umd.js");
/* harmony import */ var ngx_device_detector__WEBPACK_IMPORTED_MODULE_32___default = /*#__PURE__*/__webpack_require__.n(ngx_device_detector__WEBPACK_IMPORTED_MODULE_32__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

































var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
                _home_home_component__WEBPACK_IMPORTED_MODULE_6__["HomeComponent"],
                _home_banner_home_banner_component__WEBPACK_IMPORTED_MODULE_7__["HomeBannerComponent"],
                _home_convenios_home_convenios_component__WEBPACK_IMPORTED_MODULE_8__["HomeConveniosComponent"],
                _home_opiniones_home_opiniones_component__WEBPACK_IMPORTED_MODULE_9__["HomeOpinionesComponent"],
                _home_contactanos_home_contactanos_component__WEBPACK_IMPORTED_MODULE_10__["HomeContactanosComponent"],
                _home_popup_home_popup_component__WEBPACK_IMPORTED_MODULE_11__["HomePopupComponent"],
                _servicios_servicios_component__WEBPACK_IMPORTED_MODULE_12__["ServiciosComponent"],
                _beneficios_beneficios_component__WEBPACK_IMPORTED_MODULE_13__["BeneficiosComponent"],
                _seguridad_seguridad_component__WEBPACK_IMPORTED_MODULE_14__["SeguridadComponent"],
                _contactanos_contactanos_component__WEBPACK_IMPORTED_MODULE_15__["ContactanosComponent"],
                _convenios_convenios_component__WEBPACK_IMPORTED_MODULE_16__["ConveniosComponent"],
                _vinculate_vinculate_component__WEBPACK_IMPORTED_MODULE_17__["VinculateComponent"],
                _conocenos_conocenos_component__WEBPACK_IMPORTED_MODULE_18__["ConocenosComponent"],
                _shared_header_header_component__WEBPACK_IMPORTED_MODULE_19__["HeaderComponent"],
                _shared_pie_pie_component__WEBPACK_IMPORTED_MODULE_20__["PieComponent"],
                _shared_directives_images_sizes_square_directive__WEBPACK_IMPORTED_MODULE_21__["ImagesSizesSquareDirective"],
                _shared_directives_images_sizes_banner_directive__WEBPACK_IMPORTED_MODULE_22__["ImagesSizesBannerDirective"],
                _shared_directives_square_div_directive__WEBPACK_IMPORTED_MODULE_23__["SquareDivDirective"],
                _shared_creditos_slider_creditos_slider_component__WEBPACK_IMPORTED_MODULE_24__["CreditosSliderComponent"],
                _pagina_convenio_pagina_convenio_component__WEBPACK_IMPORTED_MODULE_25__["PaginaConvenioComponent"],
                _shared_convenios_page_convenios_page_component__WEBPACK_IMPORTED_MODULE_26__["ConveniosPageComponent"],
                _formulario_formulario_component__WEBPACK_IMPORTED_MODULE_27__["FormularioComponent"],
                _pipes_html_safe_pipe__WEBPACK_IMPORTED_MODULE_29__["HtmlSafePipe"],
                _shared_radio_group_radio_group_component__WEBPACK_IMPORTED_MODULE_30__["RadioGroupComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_http__WEBPACK_IMPORTED_MODULE_3__["HttpModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                ngx_slick__WEBPACK_IMPORTED_MODULE_31__["SlickModule"].forRoot(),
                ngx_device_detector__WEBPACK_IMPORTED_MODULE_32__["DeviceDetectorModule"].forRoot()
            ],
            providers: [_services_service_manager_service__WEBPACK_IMPORTED_MODULE_28__["ServiceManagerService"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/beneficios/beneficios.component.css":
/*!*****************************************************!*\
  !*** ./src/app/beneficios/beneficios.component.css ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/beneficios/beneficios.component.html":
/*!******************************************************!*\
  !*** ./src/app/beneficios/beneficios.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"pageContent\">\n\t<div class=\"L-10 M-10 S-10 row pageBanner\">\n\t\t<div class=\"L-10 M-8 S-12 columns imagenBanner Beneficios\" appImagesSizesBanner>\n\t\t\t<img src=\"./assets/img/BanBeneficios.jpg\">\n\t\t\t<div class=\"PageTitle\"><h2>Beneficios</h2><h3>para que logres lo que te propones</h3></div>\n\t\t</div>\t\n\t\t<div class=\"L-2 M-2 S-12 columns bannerBotones\">\n\t\t\t<a class=\"L-12 M-12 S-6 columns OfVirtual\" href=\"https://servicios2.inube.com.co/linix/v6/860006643/login.php?nit=860006643\" target=\"_blank\">\n\t\t\t\t<div class=\"icono\"><img src=\"./assets/img/OfVirtIcon.png\"></div>\n\t\t\t\t<div class=\"texto\"><b>Oficina<b class=\"Resaltar\"> virtual</b></b></div>\n\t\t\t</a>\n\t\t\t<div class=\"L-12 M-12 S-6 columns Vinculate\" [routerLink]=\"['/vinculate']\">\n\t\t\t\t<div class=\"icono\"><img src=\"./assets/img/vinculIcon.png\"></div>\n\t\t\t\t<div class=\"texto\"><b>Vincúlate<b class=\"Resaltar\"> ahora</b></b></div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t<div class=\"L-10 M-10 S-10 row pageContainer\">\n\t\t<div>\n\t\t\t<div class=\"Icon BeneficiosAfilia\"></div>\n\t\t\t<div class=\"Title BeneficiosAfilia\"><h1>Cuando te afilias</h1></div>\n\t\t\t<div class=\"TextTitle\"><a>{{afiliaciones.desc}}</a></div>\n\t\t</div>\n\n\t\t<ng-container *ngFor=\"let afilacion of afiliaciones\">\n\t\t<div class=\"L-12 M-12 S-12 columns insideContent\">\n\t\t\t<img [src]=\"afilacion.image.url\" class=\"L-3 M-4 S-12 columns\">\n\t\t\t<div class=\"L-9 M-8 S-12 texto\" [innerHTML]=\"afilacion.description | safe:'html'\"></div>\n\t\t</div>\n\t\t</ng-container>\n\t</div>\n\t<div class=\"L-10 M-10 S-10 row pageContainer\">\n\t\t<div>\n\t\t\t<div class=\"Icon BeneficiosServi\"></div>\n\t\t\t<div class=\"Title BeneficiosServi\"><h1>Servicios y convenios</h1></div>\n\t\t</div>\n\t\t<ng-container *ngFor=\"let servicio of servicios_convenios\">\n\t\t<div class=\"L-12 M-12 S-12 columns insideContent\">\n\t\t\t<img [src]=\"servicio.image.url\" class=\"L-3 M-4 S-12 columns\">\n\t\t\t<div class=\"L-9 M-8 S-12 texto\" [innerHTML]=\"servicio.description | safe:'html'\"></div>\n\t\t</div>\n\t\t</ng-container>\n\t\t\n\t\t<div class=\"TextTitle\"><a>{{ servicios_convenios.desc }}</a></div>\n\t</div>\n</div>\t"

/***/ }),

/***/ "./src/app/beneficios/beneficios.component.ts":
/*!****************************************************!*\
  !*** ./src/app/beneficios/beneficios.component.ts ***!
  \****************************************************/
/*! exports provided: BeneficiosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BeneficiosComponent", function() { return BeneficiosComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _services_service_manager_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/service-manager.service */ "./src/app/services/service-manager.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var BeneficiosComponent = /** @class */ (function () {
    function BeneficiosComponent(service) {
        this.service = service;
        this.afiliaciones = [];
        this.servicios_convenios = [];
    }
    BeneficiosComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.service.getBeneficios()
            .subscribe(function (beneficios) {
            console.log(beneficios);
            _this.afiliaciones = beneficios.afiliacion;
            _this.servicios_convenios = beneficios.serv_convenios;
            _this.afiliaciones = beneficios.afiliacion.map(function (item) {
                item.image.url = _environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].URL_SERVER + item.image.url;
                item.image.thumb.url = _environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].URL_SERVER + item.image.thumb.url;
                return item;
            });
            _this.servicios_convenios = beneficios.serv_convenios.map(function (item) {
                item.image.url = _environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].URL_SERVER + item.image.url;
                item.image.thumb.url = _environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].URL_SERVER + item.image.thumb.url;
                return item;
            });
        });
    };
    BeneficiosComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-beneficios',
            template: __webpack_require__(/*! ./beneficios.component.html */ "./src/app/beneficios/beneficios.component.html"),
            styles: [__webpack_require__(/*! ./beneficios.component.css */ "./src/app/beneficios/beneficios.component.css")]
        }),
        __metadata("design:paramtypes", [_services_service_manager_service__WEBPACK_IMPORTED_MODULE_2__["ServiceManagerService"]])
    ], BeneficiosComponent);
    return BeneficiosComponent;
}());



/***/ }),

/***/ "./src/app/conocenos/conocenos.component.css":
/*!***************************************************!*\
  !*** ./src/app/conocenos/conocenos.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".contenedor{\n\twidth: auto;\n    position: relative;\n}\n\n.iconos ul{\n\tmargin-left: auto;\n\tmargin-right: auto;\n    text-align: center;\n}\n\n.iconos li{\n    display: inline-block;\n    font-size: .8em;\n    text-align: center;\n    margin: 0 13px 0 13px;\n    margin-top: 20px;\n    margin-bottom: 50px;\n    cursor: pointer;\n}\n\n.Title{\n    text-align: center;\n}\n\nspan{\n    display: none;\n}\n\n.previous {\n    font-size: 0.9em !important;\n    padding: 80px 0;\n}\n\n.previous .Objeto{\n    text-align: center;\n}\n\n.MainContainer{\n    z-index: 1000;\n    padding: 10px 0;\n}\n\n.MainContainer .Objeto {\n     width: 100%;\n     text-align: center;\n}\n\n.MainContainer .Credito {\n    height: 400px;\n    margin-bottom: 30px;\n}\n\n.Objeto{\n    border-radius: 5px;\n    padding-bottom: 15px;\n}\n\n.Objeto h1{\n    text-align: center;\n    color: white;\n    margin-top: 5px;\n    margin-bottom: 0;\n    font-size: 1em;\n}\n\n.Objeto a{\n    text-align: center;\n    color: white;\n    font-size: 1em;\n    margin-bottom: 5px;\n}\n\n.Objeto img {\n    padding: 5px;\n    border-radius: 5px;\n    \n}\n\n.Credito{\n    background-color: transparent !important;\n    border: solid 2px;\n    padding: 10px;\n    margin-top: -15px;\n    border-radius: 5px;\n}\n\n.Credito h1{\n    font-size: 0.62em;\n    margin-top: 10px;\n    margin-bottom: 0;\n}\n\n.Credito a, .Credito p{\n    font-size: 0.9em;\n}\n\n.contenedor .BackBanner{\n    left: 15px;\n    background-color: rgb(100 100 100);\n    border-radius: 150px;\n}\n\n.contenedor .NextBanner{\n    right: 15px;\n    background-color: rgb(100 100 100);\n    border-radius: 150px;\n}\n\n@media screen and (max-width: 39.9375em) {\n    .IconosHist{\n        position: static;\n    }\n\n    .IconosHist ul{\n        position: relative; \n        -webkit-transform: none;\n        transform: none;\n        margin-top: 6rem;\n        z-index: 10;\n    }\n\n    .IconosHist li{\n        width: 100%;\n        margin-bottom: 1rem;\n    }\n\n    .Linea{\n        height: 70%;\n        width: 4px;\n        border-left: solid 2px #75800f;\n        bottom: 40%;\n    }\n\n}"

/***/ }),

/***/ "./src/app/conocenos/conocenos.component.html":
/*!****************************************************!*\
  !*** ./src/app/conocenos/conocenos.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"pageContent\">\n\t<div class=\"L-10 M-10 S-10 row pageBanner\">\n\t\t<div class=\"L-10 M-8 S-12 columns imagenBanner Conocenos\" appImagesSizesBanner>\n\t\t\t<img src=\"./assets/img/BanConocenos.jpg\">\n\t\t\t<div class=\"PageTitle\"><h2>Conócenos</h2><h3>El Fondo de Empleados del Banco de Bogotá</h3></div>\n\t\t</div>\t\n\t\t<div class=\"L-2 M-2 S-12 columns bannerBotones\">\n\t\t\t<a class=\"L-12 M-12 S-6 columns OfVirtual\" href=\"https://servicios2.inube.com.co/linix/v6/860006643/login.php?nit=860006643\" target=\"_blank\">\n\t\t\t\t<div class=\"icono\"><img src=\"./assets/img/OfVirtIcon.png\"></div>\n\t\t\t\t<div class=\"texto\"><b>Oficina<b class=\"Resaltar\"> virtual</b></b></div>\n\t\t\t</a>\n\t\t\t<div class=\"L-12 M-12 S-6 columns Vinculate\" [routerLink]=\"['/vinculate']\">\n\t\t\t\t<div class=\"icono\"><img src=\"./assets/img/vinculIcon.png\"></div>\n\t\t\t\t<div class=\"texto\"><b>Vincúlate<b class=\"Resaltar\"> ahora</b></b></div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t<div class=\"L-10 M-10 S-10 row pageContainer\">\n\t\t<div>\n\t\t\t<div class=\"Icon ConocenosQuienes\"></div>\n\t\t\t<div class=\"Title ConocenosQuienes\"><h1>Quienes somos</h1></div>\n\t\t</div>\n\t\t<div class=\"L-12 M-12 S-12 columns insideContent\">\n\t\t\t<img class=\"L-3 M-4 S-12 columns\" src=\"./assets/img/ImgCuadr_0002_stock-photo-two-hands-trying-to-connect-couple-puzzle-piece-with-sunset-background-jigsaw-alone-wooden-puz.jpg\" alt=\"\">\n\t\t\t<div class=\"L-9 M-8 S-12 texto\">\n\t\t\t\t<h3>Misión</h3>\n\t\t\t\t<p>Somos una entidad del Sector Solidario que capta ahorros de sus asociados, optimizando estos recursos para ofrecer a sus asociados y sus familias un portafolio de productos y servicios principalmente financieros, que satisfaga permanentemente las necesidades económicas y sociales, brindándoles un mejoramiento en su calidad de vida, con base en los principios solidarios.\n\t\t\t\t</p>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"L-12 M-12 S-12 columns insideContent\">\n\t\t\t<img class=\"L-3 M-4 S-12 columns\" src=\"./assets/img/ImgCuadr_0005_stock-photo-the-road-mission-aspiration-goals-ideas-inspiration-vision-concept-759578713.jpg\" alt=\"\">\n\t\t\t<div class=\"L-9 M-8 S-12 texto\">\n\t\t\t\t<h3>Visión</h3>\n\t\t\t\t<p>El Fondo de Empleados del Banco de Bogotá quiere ser siempre el Fondo de Empleados líder que crece en beneficio de sus asociados, sus familias y de sus colaboradores.\n\t\t\t\t</p>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t<div class=\"L-10 M-10 S-10 row pageContainer\">\n\t\t<div>\n\t\t\t<div class=\"Icon ConocenosHist\"></div>\n\t\t\t<div class=\"Title ConocenosHist\"><h1>Nuestra historia</h1></div>\n\t\t</div>\n\t\t<div class=\"L-12 M-12 S-12 columns insideContent\">\n\t\t\t<img class=\"L-12 M-12 S-12 columns historia\" src=\"./assets/img/NuestraHist.jpg\" alt=\"\">\n\t\t\t<div class=\"IconosHist\">\n\t\t\t\t<ul >\n\t\t\t\t\t<li *ngFor=\"let item of NuestraHistoria\" (click)=\"item.selected = true\" [ngClass]=\"{'selected': item.selected}\">\n\t\t\t\t\t\t<div class=\"Prueba\">\n\t\t\t\t\t\t\t<div class=\"Fecha\"><h1>{{item.anno}}</h1></div>\n\t\t\t\t\t\t\t<div class=\"descripcion\">{{item.Descripcion}}</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</li>\n\t\t\t\t</ul>\n\t\t\t\t<div class=\"Linea\"></div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t<div class=\"L-10 M-10 S-10 row pageContainer\">\n\t\t<div>\n\t\t\t<div class=\"Icon ConocenosEstat\"></div>\n\t\t\t<div class=\"Title ConocenosEstat\"><h1>Estatutos</h1></div>\n\t\t</div>\n\t\t<div class=\"L-12 M-12 S-12 columns insideContent\">\n\t\t\t<img class=\"L-3 M-4 S-12 columns\" src=\"./assets/img/ImgCuadr_0001_stock-photo-young-tree-seedling-are-growing-on-rule-of-third-position-over-green-nature-outdoor-background.jpg\" alt=\"\">\n\t\t\t<div class=\"L-9 M-8 S-12 texto\">\n\t\t\t\tLos estatutos establecidos por el Fondo de Empleados definen su manera de constitución, su modo de ser administrado y dirigido, así mismo los derechos y deberes entre el Fondo y sus asociados y con respecto a terceros, el destino de los bienes, la liquidación de la entidad, etcétera.<br><br>Si deseas conocer los estatutos del Fondo de Empleados, puedes consultarlos aquí: <a [href]=\"link_estatutos\" target=\"_blank\">Estatutos</a> <br><br>Si deseas conocer los reglamentos para la solicitud de créditos, puedes consultarlos aquí: <a [href]=\"link_reglamento\" target=\"_blank\">Archivo de Reglamentos</a>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t<div class=\"L-10 M-10 S-10 row pageContainer\">\n\t\t<div>\n\t\t\t<div class=\"Icon ConocenosQuienes\"></div>\n\t\t\t<div class=\"Title ConocenosQuienes\"><h1>Órganos de Dirección</h1></div>\n\t\t</div>\n\t\t<div class=\"L-12 M-12 S-12 row insideContent\">\n\t\t\t<div class=\"L-12 M-12 S-12 row contenedor\">\n\t\t\t\t<app-creditos-slider *ngIf=\"Junta\" [data]=\"Junta\"></app-creditos-slider>\n\t\t\t</div>\n\n\t\t</div>\n\t</div>\n</div>\t"

/***/ }),

/***/ "./src/app/conocenos/conocenos.component.ts":
/*!**************************************************!*\
  !*** ./src/app/conocenos/conocenos.component.ts ***!
  \**************************************************/
/*! exports provided: ConocenosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConocenosComponent", function() { return ConocenosComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_data_services__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/data.services */ "./src/app/services/data.services.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _services_service_manager_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/service-manager.service */ "./src/app/services/service-manager.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ConocenosComponent = /** @class */ (function () {
    function ConocenosComponent(service) {
        this.service = service;
        this.BackPosition = 3;
        this.Position = 0;
        this.FrontPosition = this.Position + 1;
        this.ArrClasses = ["JuntaDir", "ControlSoc", "Apelaciones", "RevFisc"];
        this.icons = ["junta1.png", "junta2.png", "junta3.png", "junta4.png"];
    }
    ConocenosComponent.prototype.ChangePosition = function (ev) {
    };
    ConocenosComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.link_estatutos = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].URL_SERVER + '/formatos/estatutos_fondo_de_empleados.pdf';
        this.link_reglamento = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].URL_SERVER + '/formatos/reglamento_de_credito.pdf';
        this.NuestraHistoria = _services_data_services__WEBPACK_IMPORTED_MODULE_1__["Historia"].map(function (item) {
            item['selected'] = false;
            return item;
        });
        this.service.getComites()
            .subscribe(function (comite) {
            _this.Junta = comite.map(function (item, i) {
                console.log(_this.ArrClasses[i]);
                return {
                    "id": (i + 1),
                    "Clase": _this.ArrClasses[i],
                    "Description": item.description,
                    "Titulo": item.title,
                    "Objeto": item.sub_title,
                    "TextIcon": item.title,
                    "Icon": "/assets/img/" + _this.icons[i]
                };
            });
        });
    };
    ConocenosComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-conocenos',
            template: __webpack_require__(/*! ./conocenos.component.html */ "./src/app/conocenos/conocenos.component.html"),
            styles: [__webpack_require__(/*! ./conocenos.component.css */ "./src/app/conocenos/conocenos.component.css")],
        }),
        __metadata("design:paramtypes", [_services_service_manager_service__WEBPACK_IMPORTED_MODULE_3__["ServiceManagerService"]])
    ], ConocenosComponent);
    return ConocenosComponent;
}());



/***/ }),

/***/ "./src/app/contactanos/contactanos.component.css":
/*!*******************************************************!*\
  !*** ./src/app/contactanos/contactanos.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".pageContainer{\n    /*background-color: rgb(240, 240, 240);*/\n}\n\n.contenedor{\n    width: auto;\n    position: relative;\n}\n\n.iconos ul{\n    margin-left: auto;\n    margin-right: auto;\n    text-align: center;\n}\n\n.iconos li{\n    display: inline-block;\n    font-size: .8em;\n    text-align: center;\n    margin: 0 13px 0 13px;\n    margin-top: 20px;\n    margin-bottom: 50px;\n    cursor: pointer;\n    vertical-align: top;\n}\n\n.Title{\n    text-align: center;\n}\n\nspan{\n    text-align: center;\n    width: 65px;    \n}\n\nli .icon_1{\n    margin: 0 auto;\n    background-image: url(/assets/img/assets.png);\n    background-position: -368px -627px;\n    padding-top: 60px;\n    width: 62px;\n    height: 60px;\n    border:solid 2px #F0C730;\n    border-radius: 31px;\n    color: #F0C730;\n}\n\nli .icon_2{\n    margin: 0 auto;\n    background-image: url(/assets/img/assets.png);\n    background-position: -368px -684px;\n    padding-top: 60px;\n    width: 62px;\n    height: 60px;\n    border:solid 2px #5698AA;\n    border-radius: 31px;\n    color: #5698AA;\n}\n\nli .icon_3{\n    margin: 0 auto;\n    background-image: url(/assets/img/assets.png);\n    background-position: -368px -740px;\n    padding-top: 60px;\n    width: 62px;\n    height: 60px;\n    border:solid 2px #CC4229;\n    border-radius: 31px;\n    color: #CC4229;\n}\n\nli .icon_4{\n    margin: 0 auto;\n    background-image: url(/assets/img/assets.png);\n    background-position: -368px -798px;\n    padding-top: 60px;\n    width: 62px;\n    height: 60px;\n    border:solid 2px #28487E;\n    border-radius: 31px;\n    color: #28487E;\n}\n\nli .icon_5{\n    margin: 0 auto;\n    background-image: url(/assets/img/assets.png);\n    background-position: -368px -854px;\n    padding-top: 60px;\n    width: 62px;\n    height: 60px;\n    border:solid 2px #BFCA43;\n    border-radius: 31px;\n    color: #BFCA43;\n}\n\nli .icon_6{\n    margin: 0 auto;\n    background-image: url(/assets/img/assets.png);\n    background-position: -368px -911px;\n    padding-top: 60px;\n    width: 62px;\n    height: 60px;\n    border:solid 2px #E0992F;\n    border-radius: 31px;\n    color: #E0992F;\n}\n\nli .icon_1:hover, li .icon_1.selected {\n    background-color: #F0C730;\n    background-position-x: -483px;\n}\n\nli .icon_2:hover, li .icon_2.selected{\n    background-color: #5698AA;\n    background-position-x: -483px;\n\n}\n\nli .icon_3:hover, li .icon_3.selected{\n    background-color: #CC4229;\n    background-position-x: -483px;\n}\n\nli .icon_4:hover, li .icon_4.selected{\n    background-color: #28487E;\n    background-position-x: -483px;\n  \n}\n\nli .icon_5:hover, li .icon_5.selected{\n    background-color: #BFCA43;\n    background-position-x: -483px;\n   \n}\n\nli .icon_6:hover, li .icon_6.selected{\n    background-color: #E0992F;\n    background-position-x: -483px;\n}\n\n.datos{\n    text-align: center;\n    vertical-align: middle;\n}\n\n.mailIcon {\n    width: 55px;\n    height: 55px;\n    background-image: url(/assets/img/assets.png); \n    background-position: -57px -375px;\n    display: inline-block;\n    margin:0 10px 0 10px;\n}\n\n.telIcon {\n    display: inline-block;\n    width: 55px;\n    height: 55px;\n    background-image: url(/assets/img/assets.png); \n    background-position: 0 -375px;\n    margin:0 10px 0 10px; \n}\n\n.mail{\n    height: 60px;\n    display: inline-block;\n    vertical-align: middle;\n    margin:0 10px 0 10px;\n}\n\n.tel {\n    height: 60px;\n    vertical-align: middle;\n    display: inline-block;\n    margin:0 10px 0 10px;\n}\n\n@media screen and (max-width: 39.9375em) {\n    .Title{\n        width: 100%;\n        text-align: left !important;\n        margin-bottom: 0.5rem !important;\n\n    }\n\n    .Title .icon-title{\n        display: inline-block;\n        vertical-align: middle;\n        margin-right: 1rem;\n    }\n\n    .Title .text-title{\n        display: inline-block;\n        vertical-align: middle;\n        font-size: 1rem;\n    }\n\n    .telIcon{\n        display: block;\n        margin: 0 auto 0.2rem;\n    }\n}"

/***/ }),

/***/ "./src/app/contactanos/contactanos.component.html":
/*!********************************************************!*\
  !*** ./src/app/contactanos/contactanos.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"pageContent\" *ngIf=\"data\">\n\t<div class=\"L-10 M-10 S-10 row pageBanner\">\n\t\t<div class=\"L-10 M-8 S-12 columns imagenBanner ContactanosPage\" appImagesSizesBanner>\n\t\t\t<img src=\"./assets/img/BanContact.jpg\">\n\t\t\t<div class=\"PageTitle\"><h2>Contáctanos</h2><h3>conoce los servicios que tenemos para tí</h3></div>\n\t\t</div>\t\n\t\t<div class=\"L-2 M-2 S-12 columns bannerBotones\">\n\t\t\t<a class=\"L-12 M-12 S-6 columns OfVirtual\" href=\"https://servicios2.inube.com.co/linix/v6/860006643/login.php?nit=860006643\" target=\"_blank\">\n\t\t\t\t<div class=\"icono\"><img src=\"./assets/img/OfVirtIcon.png\"></div>\n\t\t\t\t<div class=\"texto\"><b>Oficina<b class=\"Resaltar\"> virtual</b></b></div>\n\t\t\t</a>\n\t\t\t<div class=\"L-12 M-12 S-6 columns Vinculate\" [routerLink]=\"['/vinculate']\">\n\t\t\t\t<div class=\"icono\"><img src=\"./assets/img/vinculIcon.png\"></div>\n\t\t\t\t<div class=\"texto\"><b>Vincúlate<b class=\"Resaltar\"> ahora</b></b></div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\t\n\t<div class=\"L-10 M-10 S-10 row pageContainer\">\n\t\t<div class=\"L-12 M-12 S-12 row contenedor\">\n\t\t\t<div class=\"iconos\">\n\t\t\t\t<ul class=\"L-10 M-10 S-10 columns centered\">\n\t\t\t\t\t<li class=\"Title\" *ngFor=\"let item of data\">\n\t\t\t\t\t\t<div [ngClass]=\"{selected: curr_data.id == item.id}\" (click)=\"ChangePosition(item.id-1)\" class=\"{{item.Clase}} icon-title\"></div>\n\t\t\t\t\t\t<div class=\"text-title\">{{item.Area}}</div>\n\t\t\t\t\t</li>\n\t\t\t\t</ul>\n\t\t\t</div>\n\t\t\t<div class=\"datos\">\n\t\t\t\t<div class=\"mailIcon\"></div>\n\t\t\t\t<div class=\"mail\">{{curr_data.Correo}}</div>\n\t\t\t\t<div class=\"telIcon\"></div>\n\t\t\t\t<div class=\"tel\">{{curr_data.Ext}}</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\t"

/***/ }),

/***/ "./src/app/contactanos/contactanos.component.ts":
/*!******************************************************!*\
  !*** ./src/app/contactanos/contactanos.component.ts ***!
  \******************************************************/
/*! exports provided: ContactanosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactanosComponent", function() { return ContactanosComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_service_manager_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/service-manager.service */ "./src/app/services/service-manager.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ContactanosComponent = /** @class */ (function () {
    function ContactanosComponent(api) {
        this.api = api;
        this.index_serie = 6;
    }
    ContactanosComponent.prototype.ChangePosition = function (ev) {
        var curr = this.data.filter(function (x) { return x.id == (ev + 1); });
        this.curr_data = curr[0];
    };
    ContactanosComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.api.getContactos().subscribe(function (data) {
            var max_items = data.contactos.length;
            var multiplier = Math.round(max_items / _this.index_serie);
            var index = 0;
            _this.data = data.contactos.map(function (item, i) {
                var data = {
                    "id": (i + 1),
                    "Clase": "icon_" + (index + 1),
                    "Area": item.title,
                    "Correo": item.email,
                    "Ext": item.telephone
                };
                index = (index < (_this.index_serie - 1)) ? index + 1 : 0;
                return data;
            });
            _this.curr_data = _this.data[0];
        });
    };
    ContactanosComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-contactanos',
            template: __webpack_require__(/*! ./contactanos.component.html */ "./src/app/contactanos/contactanos.component.html"),
            styles: [__webpack_require__(/*! ./contactanos.component.css */ "./src/app/contactanos/contactanos.component.css")]
        }),
        __metadata("design:paramtypes", [_services_service_manager_service__WEBPACK_IMPORTED_MODULE_1__["ServiceManagerService"]])
    ], ContactanosComponent);
    return ContactanosComponent;
}());



/***/ }),

/***/ "./src/app/convenios/convenios.component.css":
/*!***************************************************!*\
  !*** ./src/app/convenios/convenios.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".pageContainer{\n    background-color: rgb(240, 240, 240);\n}\n\n.contenedor{\n    width: auto;\n    position: relative;\n    background-color: rgb(240, 240, 240);\n}\n\n.iconos ul{\n    margin-left: auto;\n    margin-right: auto;\n    text-align: center;\n}\n\n.iconos li{\n    display: inline-block;\n    font-size: .8em;\n    text-align: center;\n    margin: 0 13px 0 13px;\n    margin-top: 20px;\n    margin-bottom: 50px;\n    cursor: pointer;\n    vertical-align: top;\n}\n\n.Title{\n    text-align: center;\n}\n\nspan{\n    text-align: center;\n    width: 65px;    \n}\n\nli .Todos{\n    margin: 0 auto;\n    background-image: url(/assets/img/assets.png);\n    background-position: -598px -624px;\n    padding-top: 60px;\n    width: 62px;\n    height: 60px;\n    border:solid 2px #F0C730;\n    border-radius: 31px;\n    color: #F0C730;\n}\n\nli .Turismo{\n    margin: 0 auto;\n    background-image: url(/assets/img/assets.png);\n    background-position: -598px -681px;\n    padding-top: 60px;\n    width: 62px;\n    height: 60px;\n    border:solid 2px #5698AA;\n    border-radius: 31px;\n    color: #5698AA;\n}\n\nli .Salud{\n    margin: 0 auto;\n    background-image: url(/assets/img/assets.png);\n    background-position: -598px -737px;\n    padding-top: 60px;\n    width: 62px;\n    height: 60px;\n    border:solid 2px #CC4229;\n    border-radius: 31px;\n    color: #CC4229;\n}\n\nli .Seguros{\n    margin: 0 auto;\n    background-image: url(/assets/img/assets.png);\n    background-position: -598px -795px;\n    padding-top: 60px;\n    width: 62px;\n    height: 60px;\n    border:solid 2px #28487E;\n    border-radius: 31px;\n    color: #28487E;\n}\n\nli .Tecnologia{\n    margin: 0 auto;\n    background-image: url(/assets/img/assets.png);\n    background-position: -598px -851px;\n    padding-top: 60px;\n    width: 62px;\n    height: 60px;\n    border:solid 2px #BFCA43;\n    border-radius: 31px;\n    color: #BFCA43;\n}\n\nli .Concesionarios{\n    margin: 0 auto;\n    background-image: url(/assets/img/assets.png);\n    background-position: -598px -908px;\n    padding-top: 60px;\n    width: 62px;\n    height: 60px;\n    border:solid 2px #E0992F;\n    border-radius: 31px;\n    color: #E0992F;\n}\n\nli .Todos:hover, li .Todos.selected {\n    background-color: #F0C730;\n    background-position-x: -712px;\n}\n\nli .Turismo:hover, li .Turismo.selected{\n    background-color: #5698AA;\n    background-position-x: -712px;\n\n}\n\nli .Salud:hover, li .Salud.selected{\n    background-color: #CC4229;\n    background-position-x: -712px;\n}\n\nli .Seguros:hover, li .Seguros.selected{\n    background-color: #28487E;\n    background-position-x: -712px;\n  \n}\n\nli .Tecnologia:hover, li .Tecnologia.selected{\n    background-color: #BFCA43;\n    background-position-x: -712px;\n   \n}\n\nli .Concesionarios:hover, li .Concesionarios.selected{\n    background-color: #E0992F;\n    background-position-x: -712px;\n}\n\nli.FilteredConv {\n    cursor: pointer;\n    position: relative;\n    padding: 10px;\n}\n\nli.FilteredConv:hover .More {\n    display: block;\n}\n\n.FilteredConv img{\n    border: solid 1px grey;\n    border-radius: 15px;\n}\n\n.More {\n    display: none;\n    position: absolute;\n    top: 0;\n    left: 0;\n    padding: 10px;\n    width: 100%;\n    height: 100%;\n}\n\n.MoreContainer {\n    background-color: rgba(40,72,126,.9);\n    color: white !important;\n    border: solid 1px grey;\n    border-radius: 15px;\n    height: 99%;\n    padding: 10px;\n    }\n\n.Titulo h1{\n    color: white;\n}\n\n.descript{\n    border-top: solid 2px grey;\n    margin-top: -20px;\n    padding-top: 5px;\n}\n\n.descuento{\n    position: relative;\n    font-size: 2em;\n    text-align: center;\n    -webkit-transform: translateY(50%);\n            transform: translateY(50%)\n}\n\n.descuento h1{\n    color:white;\n    margin-top: 0;\n}"

/***/ }),

/***/ "./src/app/convenios/convenios.component.html":
/*!****************************************************!*\
  !*** ./src/app/convenios/convenios.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"pageContent\">\n\t<div class=\"L-10 M-10 S-10 row pageBanner\">\n\t\t<div class=\"L-10 M-8 S-12 columns imagenBanner ConveniosPage\" appImagesSizesBanner>\n\t\t\t<img src=\"./assets/img/BanConven.jpg\">\n\t\t\t<div class=\"PageTitle\"><h2>Convenios</h2><h3>para que disfrutes con los que más amas</h3></div>\n\t\t</div>\t\n\t\t<div class=\"L-2 M-2 S-12 columns bannerBotones\">\n\t\t\t<a class=\"L-12 M-12 S-6 columns OfVirtual\" href=\"https://servicios2.inube.com.co/linix/v6/860006643/login.php?nit=860006643\" target=\"_blank\">\n\t\t\t\t<div class=\"icono\"><img src=\"./assets/img/OfVirtIcon.png\"></div>\n\t\t\t\t<div class=\"texto\"><b>Oficina<b class=\"Resaltar\"> virtual</b></b></div>\n\t\t\t</a>\n\t\t\t<div class=\"L-12 M-12 S-6 columns Vinculate\" [routerLink]=\"['/vinculate']\">\n\t\t\t\t<div class=\"icono\"><img src=\"./assets/img/vinculIcon.png\"></div>\n\t\t\t\t<div class=\"texto\"><b>Vincúlate<b class=\"Resaltar\"> ahora</b></b></div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t<div class=\"L-10 M-10 S-10 row pageContainer\">\n\t\t<app-convenios-page></app-convenios-page>\n\t</div>\n</div>\t"

/***/ }),

/***/ "./src/app/convenios/convenios.component.ts":
/*!**************************************************!*\
  !*** ./src/app/convenios/convenios.component.ts ***!
  \**************************************************/
/*! exports provided: ConveniosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConveniosComponent", function() { return ConveniosComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ConveniosComponent = /** @class */ (function () {
    function ConveniosComponent() {
    }
    ConveniosComponent.prototype.ngOnInit = function () {
    };
    ConveniosComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-convenios',
            template: __webpack_require__(/*! ./convenios.component.html */ "./src/app/convenios/convenios.component.html"),
            styles: [__webpack_require__(/*! ./convenios.component.css */ "./src/app/convenios/convenios.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ConveniosComponent);
    return ConveniosComponent;
}());



/***/ }),

/***/ "./src/app/formulario/formulario.component.css":
/*!*****************************************************!*\
  !*** ./src/app/formulario/formulario.component.css ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".section-form{\n\n}\n\n.header-section-form{\n\tbackground: #27487e;\n\tmargin-bottom: 0;\n\ttext-shadow: none;\n\tfont-size: 1.8rem;\n\tpadding: 0.5rem 1rem;\n}\n\n.content-section-form{\n\tpadding: 0.8rem;\n}\n\n.section-form .columns {\n    /*padding: 0 0.5rem 0 0.5rem;*/\n}\n\n.content-checks{\n\tborder: 1px solid #cacaca;\n    padding: 0.5rem;\n    font-size: 0.8rem;\n    text-align: center;\n    min-height: 110px;\n}\n\n.content-checks .checkbox{\n    width: 40%;\n    margin-top: 0.8rem;\n}\n\n.content-checks .other-text{\n\tmargin-top: 0.5rem;\n}\n\n.content-checks .other-text input{\n\tmargin-bottom: 0;\n\theight: 35px;\n}\n\n.label-check{\n\tborder: 1px solid #cacaca;\n    width: 100%;\n    height: 49px;\n    padding: 0.5rem;\n    padding-top: 0.9rem;\n    text-align: center;\n    font-size: 0.8rem;\n}\n\nbutton{\n\tbackground: #ED4023;\n    display: block;\n    padding: 0.6rem;\n    font-size: 1.2rem;\n    color: white;\n    border: 0;\n    border-radius: 60px;\n    margin: 1rem auto;\n    width: 320px;\n    outline: none;\n}\n\nbutton img{\n\tdisplay: inline-block;\n\twidth: 50px;\n\tvertical-align: middle;\n}\n\n.input-invalid{\n\tborder-color: red;\n}\n\n.add-button{\n\tcursor: pointer;\n}\n\n.add-button img{\n\twidth: 40px;\n    display: inline-block;\n    vertical-align: middle;\n    margin-right: 5px;\n}\n\n.Formularios ul li{\n\tfont-size: 1.2rem;\n    width: auto;\n}\n\n.Formularios ul li .texto{\n\twidth: 60%;\n    margin: 0.5rem auto;\n    color: #ed3e12;\n    font-weight: 400;\n}\n\n.callout{\n    border: 1px solid #f99999;\n    padding: 1rem;\n    background: #f7e5e5;\n    margin-bottom: 1rem;\n    color: #fb6f6f;\n    font-weight: 400;\n    padding-left: 2rem;\n}\n\n.callout ul{\n    list-style: none;\n}\n\n.callout ul li{\n    margin-bottom: 0.5rem;\n}"

/***/ }),

/***/ "./src/app/formulario/formulario.component.html":
/*!******************************************************!*\
  !*** ./src/app/formulario/formulario.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"pageContent\">\n\t<div class=\"L-10 M-10 S-10 row pageBanner\">\n\t\t<div class=\"L-10 M-8 S-12 columns imagenBanner Vinculate\" appImagesSizesBanner>\n\t\t\t<img src=\"./assets/img/BanVincul.jpg\">\n\t\t\t<div class=\"PageTitle\"><h2>Vincúlate</h2><h3>y disfruta de todos nuestros beneficios</h3></div>\n\t\t</div>\t\n\t\t<div class=\"L-2 M-2 S-12 columns bannerBotones\">\n\t\t\t<a class=\"L-12 M-12 S-6 columns OfVirtual\" href=\"https://servicios2.inube.com.co/linix/v6/860006643/login.php?nit=860006643\" target=\"_blank\">\n\t\t\t\t<div class=\"icono\"><img src=\"./assets/img/OfVirtIcon.png\"></div>\n\t\t\t\t<div class=\"texto\"><b>Oficina<b class=\"Resaltar\"> virtual</b></b></div>\n\t\t\t</a>\n\t\t\t<div class=\"L-12 M-12 S-6 columns Vinculate\" [routerLink]=\"['/vinculate']\">\n\t\t\t\t<div class=\"icono\"><img src=\"./assets/img/vinculIcon.png\"></div>\n\t\t\t\t<div class=\"texto\"><b>Vincúlate<b class=\"Resaltar\"> ahora</b></b></div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t\n\t<div class=\"L-10 M-10 S-10 row pageContainer\">\n\t\t<div class=\"callout\" *ngIf=\"errors\">\n\t\t\t<ul>\n\t\t\t\t<li *ngFor=\"let error of errors\">\n\t\t\t\t\t<div *ngFor=\"let e of error\" >\n\t\t\t\t\t\t{{ e }}\n\t\t\t\t\t</div>\n\t\t\t\t</li>\n\t\t\t</ul>\n\t\t</div>\n\t\t<a target=\"_blank\" #pdf_url download></a>\n\t\t<form  #f=\"ngForm\" novalidate (ngSubmit)=\"f.valid && onSubmit()\" #f=\"ngForm\" (keydown.enter)=\"$event.preventDefault()\">\n\t\t\t<div class=\"section-form\">\n\t\t\t\t<h3 class=\"header-section-form\">Datos del asociado</h3>\n\t\t\t\t<div class=\"content-section-form\">\n\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t<label class=\"columns L-3 M-3 S-12\">\n\t\t\t\t\t\t\t<input \n\t\t\t\t\t\t\t\ttype=\"text\"\n\t\t\t\t\t\t\t\tonfocus=\"(this.type='date')\"\n\t\t\t\t\t\t\t\tmax=\"9999-12-31\"\n\t\t\t\t\t\t\t\tonblur=\"(this.type='text')\"\n\t\t\t\t\t\t\t\tplaceholder=\"Fecha de diligenciamiento\"\n\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\tname=\"date_filling\"\n\t\t\t\t\t\t\t\t[(ngModel)]=\"form.date_filling\"\n\t\t\t\t\t\t\t\t#date_filling=\"ngModel\"\n\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !date_filling.valid}\">\n\t\t\t\t\t\t</label>\n\t\t\t\t\t\t<label class=\"columns L-3 M-3 S-12\">\n\t\t\t\t\t\t\t<select \n\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\tname=\"type_format_id\"\n\t\t\t\t\t\t\t\t[(ngModel)]=\"form.type_format_id\"\n\t\t\t\t\t\t\t\t#type_format_id=\"ngModel\"\n\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !type_format_id.valid}\">\n\t\t\t\t\t\t\t\t<option value=\"\">Tipo de formato</option>\n\t\t\t\t\t\t\t\t<option *ngFor=\"let item of tipo_formato\" [value]=\"item.id\">{{ item.desc }}</option>\n\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t</label>\t\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t<label class=\"columns L-3 M-3 S-12\">\n\t\t\t\t\t\t\t<input \n\t\t\t\t\t\t\t\ttype=\"text\"\n\t\t\t\t\t\t\t\tplaceholder=\"Nombres\"\n\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\tname=\"name\"\n\t\t\t\t\t\t\t\t[(ngModel)]=\"form.name\"\n\t\t\t\t\t\t\t\t#name=\"ngModel\"\n\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !name.valid}\"\n\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t</label>\n\t\t\t\t\t\t<label class=\"columns L-3 M-3 S-12\">\n\t\t\t\t\t\t\t<input \n\t\t\t\t\t\t\t\ttype=\"text\"\n\t\t\t\t\t\t\t\tplaceholder=\"Apellidos\"\n\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\tname=\"lastname\"\n\t\t\t\t\t\t\t\t[(ngModel)]=\"form.lastname\"\n\t\t\t\t\t\t\t\t#lastname=\"ngModel\"\n\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !lastname.valid}\"\n\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t</label>\n\t\t\t\t\t\t<label class=\"columns L-2 M-2 S-12\">\n\t\t\t\t\t\t\t<input \n\t\t\t\t\t\t\t\ttype=\"text\"\n\t\t\t\t\t\t\t\tplaceholder=\"Cédula\"\n\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\tname=\"document\"\n\t\t\t\t\t\t\t\t[(ngModel)]=\"form.document\"\n\t\t\t\t\t\t\t\t#document=\"ngModel\"\n\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !document.valid}\"\n\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t</label>\n\t\t\t\t\t\t<label class=\"columns L-2 M-2 S-12\">\n\t\t\t\t\t\t\t<input \n\t\t\t\t\t\t\t\ttype=\"text\"\n\t\t\t\t\t\t\t\tplaceholder=\"Lugar de expedición\"\n\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\tname=\"lugar_ex\"\n\t\t\t\t\t\t\t\t[(ngModel)]=\"form.lugar_expedicion\"\n\t\t\t\t\t\t\t\t#lugar_expedicion=\"ngModel\"\n\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !lugar_expedicion.valid}\"\n\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t</label>\n\t\t\t\t\t\t<label class=\"columns L-2 M-2 S-12\">\n\t\t\t\t\t\t\t<input \n\t\t\t\t\t\t\t\ttype=\"text\"\n\t\t\t\t\t\t\t\tonfocus=\"(this.type='date')\"\n\t\t\t\t\t\t\t\tmax=\"9999-12-31\"\n\t\t\t\t\t\t\t\tonblur=\"(this.type='text')\"\n\t\t\t\t\t\t\t\tplaceholder=\"Fecha expedición\"\n\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\tname=\"date_expedition\"\n\t\t\t\t\t\t\t\t[(ngModel)]=\"form.date_expedition\"\n\t\t\t\t\t\t\t\t#date_expedition=\"ngModel\"\n\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !date_expedition.valid}\"\n\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t</label>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t<div class=\"columns L-3 M-3 S-12\">\n\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t<label class=\"columns L-6 M-6 S-12\">\n\t\t\t\t\t\t\t\t<select \n\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\tname=\"type_status_id\"\n\t\t\t\t\t\t\t\t[(ngModel)]=\"form.type_status_id\"\n\t\t\t\t\t\t\t\t#type_status_id=\"ngModel\"\n\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !type_status_id.valid}\">\n\t\t\t\t\t\t\t\t<option value=\"\">Estado civil</option>\n\t\t\t\t\t\t\t\t<option *ngFor=\"let item of estado_civil\" [value]=\"item.id\">{{ item.desc }}</option>\n\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t<label class=\"columns L-6 M-6 S-12\">\n\t\t\t\t\t\t\t\t<select \n\t\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\t\tname=\"type_gender_id\"\n\t\t\t\t\t\t\t\t\t[(ngModel)]=\"form.type_gender_id\"\n\t\t\t\t\t\t\t\t\t#type_gender_id=\"ngModel\"\n\t\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !type_gender_id.valid}\">\n\t\t\t\t\t\t\t\t\t\t<option value=\"\">Género</option>\n\t\t\t\t\t\t\t\t\t\t<option *ngFor=\"let item of genero\" [value]=\"item.id\">{{ item.desc }}</option>\n\t\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"columns L-3 M-3 S-12\">\n\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t<label class=\"columns L-6 M-6 S-12\">\n\t\t\t\t\t\t\t\t\t<select \n\t\t\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\t\t\tname=\"place_birth\"\n\t\t\t\t\t\t\t\t\t\t[(ngModel)]=\"form.place_birth\"\n\t\t\t\t\t\t\t\t\t\t#place_birth=\"ngModel\"\n\t\t\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !place_birth.valid}\">\n\t\t\t\t\t\t\t\t\t\t\t<option value=\"\">Lugar de nacimiento</option>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<option *ngFor=\"let item of ciudades\" [value]=\"item.id\">{{ item.desc }}</option>\n\t\t\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t<label class=\"columns L-6 M-6 S-12\">\n\t\t\t\t\t\t\t\t<input \n\t\t\t\t\t\t\t\t\ttype=\"text\"\n\t\t\t\t\t\t\t\t\tonfocus=\"(this.type='date')\"\n\t\t\t\t\t\t\t\t\tmax=\"9999-12-31\"\n\t\t\t\t\t\t\t\t\tonblur=\"(this.type='text')\"\n\t\t\t\t\t\t\t\t\ttype=\"text\"\n\t\t\t\t\t\t\t\t\tplaceholder=\"Fecha\"\n\t\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\t\tname=\"birthdate\"\n\t\t\t\t\t\t\t\t\t[(ngModel)]=\"form.birthdate\"\n\t\t\t\t\t\t\t\t\t#birthdate=\"ngModel\"\n\t\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !birthdate.valid}\"\n\t\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<label class=\"columns L-3 M-3 S-12\">\n\t\t\t\t\t\t\t<input \n\t\t\t\t\t\t\t\ttype=\"text\"\n\t\t\t\t\t\t\t\tplaceholder=\"Email Personal\"\n\t\t\t\t\t\t\t\t[pattern]=\"validators.email\" \n\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\tname=\"email\"\n\t\t\t\t\t\t\t\t[(ngModel)]=\"form.email\"\n\t\t\t\t\t\t\t\t#email=\"ngModel\"\n\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !email.valid}\"\n\n\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t</label>\n\t\t\t\t\t\t<div class=\"columns L-3 M-3 S-12\">\n\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t<label class=\"columns L-6 M-6 S-12\">\n\t\t\t\t\t\t\t\t<input \n\t\t\t\t\t\t\t\t\ttype=\"text\"\n\t\t\t\t\t\t\t\t\tplaceholder=\"Nivel de estudios\"\n\t\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\t\tname=\"education\"\n\t\t\t\t\t\t\t\t\t[(ngModel)]=\"form.education\"\n\t\t\t\t\t\t\t\t\t#education=\"ngModel\"\n\t\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !education.valid}\"\n\t\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t<label class=\"columns L-6 M-6 S-12\">\n\t\t\t\t\t\t\t\t<input \n\t\t\t\t\t\t\t\t\ttype=\"text\"\n\t\t\t\t\t\t\t\t\tplaceholder=\"Profesion\"\n\t\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\t\tname=\"profession\"\n\t\t\t\t\t\t\t\t\t[(ngModel)]=\"form.profession\"\n\t\t\t\t\t\t\t\t\t#profession=\"ngModel\"\n\t\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !profession.valid}\"\n\t\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t<label class=\"columns L-3 M-3 S-12\">\n\t\t\t\t\t\t\t<input \n\t\t\t\t\t\t\t\ttype=\"text\"\n\t\t\t\t\t\t\t\tplaceholder=\"Dirección de residencia\"\n\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\tname=\"address\"\n\t\t\t\t\t\t\t\t[(ngModel)]=\"form.address\"\n\t\t\t\t\t\t\t\t#address=\"ngModel\"\n\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !address.valid}\"\n\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t</label>\n\t\t\t\t\t\t<div class=\"columns L-3 M-3 S-12\">\n\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t<label class=\"columns L-6 M-6 S-12\">\n\t\t\t\t\t\t\t\t\t<select\n\t\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\t\tname=\"departament_id\"\n\t\t\t\t\t\t\t\t\t[(ngModel)]=\"form.departament_id\"\n\t\t\t\t\t\t\t\t\t#departament_id=\"ngModel\"\n\t\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !departament_id.valid}\"\n\t\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t\t\t<option value=\"\">Departamento</option>\n\t\t\t\t\t\t\t\t\t\t<option *ngFor=\"let item of departamento\" [value]=\"item.id\">{{ item.desc }}</option>\n\t\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t<label class=\"columns L-6 M-6 S-12\">\n\t\t\t\t\t\t\t\t<select\n\t\t\t\t\t\t\t\t\tplaceholder=\"Ciudad\"\n\t\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\t\tname=\"city_id\"\n\t\t\t\t\t\t\t\t\t[(ngModel)]=\"form.city_id\"\n\t\t\t\t\t\t\t\t\t#city_id=\"ngModel\"\n\t\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !city_id.valid}\">\n\t\t\t\t\t\t\t\t\t<option value=\"\">Ciudad</option>\n\t\t\t\t\t\t\t\t\t\t<option *ngFor=\"let item of ciudades\" [value]=\"item.id\">{{ item.desc }}</option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"columns L-3 M-3 S-12\">\n\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t<label class=\"columns L-6 M-6 S-12\">\n\t\t\t\t\t\t\t\t<input \n\t\t\t\t\t\t\t\t\ttype=\"text\"\n\t\t\t\t\t\t\t\t\tplaceholder=\"Teléfono\"\n\t\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\t\t[pattern]=\"validators.phone\" \n\t\t\t\t\t\t\t\t\tname=\"phone\"\n\t\t\t\t\t\t\t\t\t[(ngModel)]=\"form.phone\"\n\t\t\t\t\t\t\t\t\t#phone=\"ngModel\"\n\t\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !phone.valid}\"\n\t\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t<label class=\"columns L-6 M-6 S-12\">\n\t\t\t\t\t\t\t\t<input \n\t\t\t\t\t\t\t\t\ttype=\"text\"\n\t\t\t\t\t\t\t\t\tplaceholder=\"Tel. Celular\"\n\t\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\t\t[pattern]=\"validators.mobil\" \n\t\t\t\t\t\t\t\t\tname=\"mobile\"\n\t\t\t\t\t\t\t\t\t[(ngModel)]=\"form.mobile\"\n\t\t\t\t\t\t\t\t\t#mobile=\"ngModel\"\n\t\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !mobile.valid}\"\n\t\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<label class=\"columns L-3 M-3 S-12\">\n\t\t\t\t\t\t\t<select \n\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\tname=\"type_home_id\"\n\t\t\t\t\t\t\t\t[(ngModel)]=\"form.type_home_id\"\n\t\t\t\t\t\t\t\t#type_home_id=\"ngModel\"\n\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !type_home_id.valid}\">\n\t\t\t\t\t\t\t\t<option value=\"\">Tipo de residencia</option>\n\t\t\t\t\t\t\t\t<option *ngFor=\"let item of tipo_residencia\" [value]=\"item.id\">{{ item.desc }}</option>\n\n\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t</label>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t<label class=\"columns L-3 M-3 S-12\">\n\t\t\t\t\t\t\t<input \n\t\t\t\t\t\t\t\ttype=\"text\"\n\t\t\t\t\t\t\t\tplaceholder=\"Barrio\"\n\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\tname=\"barrio\"\n\t\t\t\t\t\t\t\t[(ngModel)]=\"form.barrio\"\n\t\t\t\t\t\t\t\t#barrio=\"ngModel\"\n\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !barrio.valid}\"\n\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t</label>\n\n\t\t\t\t\t\t<label class=\"columns L-2 M-2 S-12\">\n\t\t\t\t\t\t\t<input \n\t\t\t\t\t\t\t\ttype=\"text\"\n\t\t\t\t\t\t\t\tplaceholder=\"Estrato\"\n\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\t[pattern]=\"validators.number\" \n\t\t\t\t\t\t\t\tmaxlength=\"1\" \n\t\t\t\t\t\t\t\tname=\"estrato\"\n\t\t\t\t\t\t\t\t[(ngModel)]=\"form.estrato\"\n\t\t\t\t\t\t\t\t#estrato=\"ngModel\"\n\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !estrato.valid}\"\n\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t</label>\n\t\t\t\t\t</div>\n\t\t\t\t\t\n\t\t\t\t</div>\n\t\t\t</div>\n\n\t\t\t<div class=\"section-form\">\n\t\t\t\t<h3 class=\"header-section-form\">Información laboral</h3>\n\t\t\t\t<div class=\"content-section-form\">\n\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t<div class=\"columns L-3 M-3 S-12\">\n\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t<label class=\"columns L-6 M-6 S-12\">\n\t\t\t\t\t\t\t\t\t<select \n\t\t\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\t\t\tname=\"type_occupation_id\"\n\t\t\t\t\t\t\t\t\t\t[(ngModel)]=\"form.type_occupation_id\"\n\t\t\t\t\t\t\t\t\t\t#type_occupation_id=\"ngModel\"\n\t\t\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !type_occupation_id.valid}\"\n\t\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t\t\t<option value=\"\">Ocupación</option>\n\t\t\t\t\t\t\t\t\t\t<option *ngFor=\"let item of tipo_ocupacion\" [value]=\"item.id\">{{ item.desc }}</option>\n\t\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t<label class=\"columns L-6 M-6 S-12\">\n\t\t\t\t\t\t\t\t\t<input \n\t\t\t\t\t\t\t\t\t\ttype=\"text\"\n\t\t\t\t\t\t\t\t\t\tplaceholder=\"Fecha de ingreso\"\n\t\t\t\t\t\t\t\t\t\tonfocus=\"(this.type='date')\"\n\t\t\t\t\t\t\t\t\t\tmax=\"9999-12-31\"\n\t\t\t\t\t\t\t\t\t\tonblur=\"(this.type='text')\"\n\t\t\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\t\t\tname=\"date_start\"\n\t\t\t\t\t\t\t\t\t\t[(ngModel)]=\"form.date_start\"\n\t\t\t\t\t\t\t\t\t\t#date_start=\"ngModel\"\n\t\t\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !date_start.valid}\"\n\t\t\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"columns L-3 M-3 S-12\">\n\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t<label class=\"columns L-6 M-6 S-12\">\n\t\t\t\t\t\t\t\t\t<input \n\t\t\t\t\t\t\t\t\t\ttype=\"text\"\n\t\t\t\t\t\t\t\t\t\tplaceholder=\"Actividad económica\"\n\t\t\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\t\t\tname=\"economic_act\"\n\t\t\t\t\t\t\t\t\t\t[(ngModel)]=\"form.economic_act\"\n\t\t\t\t\t\t\t\t\t\t#economic_act=\"ngModel\"\n\t\t\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !economic_act.valid}\"\n\t\t\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t<label class=\"columns L-6 M-6 S-12\">\n\t\t\t\t\t\t\t\t\t<input \n\t\t\t\t\t\t\t\t\t\ttype=\"text\"\n\t\t\t\t\t\t\t\t\t\tplaceholder=\"Códido CIIU\"\n\t\t\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\t\t\tname=\"cod_ciiu\"\n\t\t\t\t\t\t\t\t\t\t[(ngModel)]=\"form.cod_ciiu\"\n\t\t\t\t\t\t\t\t\t\t#cod_ciiu=\"ngModel\"\n\t\t\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !cod_ciiu.valid}\"\n\t\t\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<label class=\"columns L-3 M-3 S-12\">\n\t\t\t\t\t\t\t<input \n\t\t\t\t\t\t\t\ttype=\"text\"\n\t\t\t\t\t\t\t\tplaceholder=\"Nombre de la empresa\"\n\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\tname=\"company\"\n\t\t\t\t\t\t\t\t[(ngModel)]=\"form.company\"\n\t\t\t\t\t\t\t\t#company=\"ngModel\"\n\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !company.valid}\">\n\t\t\t\t\t\t</label>\n\t\t\t\t\t\t<div class=\"columns L-3 M-3 S-12\">\n\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t<label class=\"columns L-6 M-6 S-12\">\n\t\t\t\t\t\t\t\t\t<input \n\t\t\t\t\t\t\t\t\t\ttype=\"text\"\n\t\t\t\t\t\t\t\t\t\tplaceholder=\"Área / Sección / Of.\"\n\t\t\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\t\t\tname=\"area_of\"\n\t\t\t\t\t\t\t\t\t\t[(ngModel)]=\"form.area_of\"\n\t\t\t\t\t\t\t\t\t\t#area_of=\"ngModel\"\n\t\t\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !area_of.valid}\"\n\t\t\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t<label class=\"columns L-6 M-6 S-12\">\n\t\t\t\t\t\t\t\t\t<input \n\t\t\t\t\t\t\t\t\t\ttype=\"text\"\n\t\t\t\t\t\t\t\t\t\tplaceholder=\"Códido área / Of.\"\n\t\t\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\t\t\tname=\"cod_area\"\n\t\t\t\t\t\t\t\t\t\t[(ngModel)]=\"form.cod_area\"\n\t\t\t\t\t\t\t\t\t\t#cod_area=\"ngModel\"\n\t\t\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !cod_area.valid}\"\n\t\t\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t<div class=\"columns L-3 M-3 S-12\">\n\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t<label class=\"columns L-6 M-6 S-12\">\n\t\t\t\t\t\t\t\t\t<input \n\t\t\t\t\t\t\t\t\t\ttype=\"text\"\n\t\t\t\t\t\t\t\t\t\tplaceholder=\"Región\"\n\t\t\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\t\t\tname=\"region\"\n\t\t\t\t\t\t\t\t\t\t[(ngModel)]=\"form.region\"\n\t\t\t\t\t\t\t\t\t\t#region=\"ngModel\"\n\t\t\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !region.valid}\"\n\t\t\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t<label class=\"columns L-4 M-4 S-12\">\n\t\t\t\t\t\t\t\t\t<input \n\t\t\t\t\t\t\t\t\t\ttype=\"text\"\n\t\t\t\t\t\t\t\t\t\tplaceholder=\"Telefono\"\n\t\t\t\t\t\t\t\t\t\t[pattern]=\"validators.phone\" \n\t\t\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\t\t\tname=\"phone_laboral\"\n\t\t\t\t\t\t\t\t\t\t[(ngModel)]=\"form.phone_laboral\"\n\t\t\t\t\t\t\t\t\t\t#phone_laboral=\"ngModel\"\n\t\t\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !phone_laboral.valid}\"\n\t\t\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t<label class=\"columns L-2 M-2 S-12\">\n\t\t\t\t\t\t\t\t\t<input \n\t\t\t\t\t\t\t\t\t\ttype=\"text\"\n\t\t\t\t\t\t\t\t\t\tplaceholder=\"Ext\"\n\t\t\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\t\t\tname=\"ext\"\n\t\t\t\t\t\t\t\t\t\t[(ngModel)]=\"form.ext\"\n\t\t\t\t\t\t\t\t\t\t#ext=\"ngModel\"\n\t\t\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !ext.valid}\"\n\t\t\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<label class=\"columns L-3 M-3 S-12\">\n\t\t\t\t\t\t\t<input \n\t\t\t\t\t\t\t\ttype=\"text\"\n\t\t\t\t\t\t\t\tplaceholder=\"Cargo\"\n\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\tname=\"position\"\n\t\t\t\t\t\t\t\t[(ngModel)]=\"form.position\"\n\t\t\t\t\t\t\t\t#position=\"ngModel\"\n\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !position.valid}\">\n\t\t\t\t\t\t</label>\n\t\t\t\t\t\t<label class=\"columns L-3 M-3 S-12\">\n\t\t\t\t\t\t\t<input \n\t\t\t\t\t\t\t\ttype=\"text\"\n\t\t\t\t\t\t\t\tplaceholder=\"Email corporativo\"\n\t\t\t\t\t\t\t\t[pattern]=\"validators.email\" \n\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\tname=\"email_laboral\"\n\t\t\t\t\t\t\t\t[(ngModel)]=\"form.email_laboral\"\n\t\t\t\t\t\t\t\t#email_laboral=\"ngModel\"\n\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !email_laboral.valid}\">\n\t\t\t\t\t\t</label>\n\t\t\t\t\t\t<div class=\"columns L-3 M-3 S-12\">\n\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t<label class=\"columns L-6 M-6 S-12\">\n\t\t\t\t\t\t\t\t\t<input \n\t\t\t\t\t\t\t\t\t\ttype=\"text\"\n\t\t\t\t\t\t\t\t\t\tplaceholder=\"Sueldo básico\"\n\t\t\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\t\t\tname=\"salary\"\n\t\t\t\t\t\t\t\t\t\t[(ngModel)]=\"form.salary\"\n\t\t\t\t\t\t\t\t\t\t#salary=\"ngModel\"\n\t\t\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !salary.valid}\"\n\t\t\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t<div class=\"columns L-3 M-3 S-12\">\n\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t<label class=\"columns L-6 M-6 S-12\">\n\t\t\t\t\t\t\t\t\t<select \n\t\t\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\t\t\tname=\"type_salary_id\"\n\t\t\t\t\t\t\t\t\t\t[(ngModel)]=\"form.type_salary_id\"\n\t\t\t\t\t\t\t\t\t\t#type_salary_id=\"ngModel\"\n\t\t\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !type_salary_id.valid}\">\n\t\t\t\t\t\t\t\t\t\t\t<option value=\"\">Tipo de salario</option>\n\t\t\t\t\t\t\t\t<option *ngFor=\"let item of tipo_salario\" [value]=\"item.id\">{{ item.desc }}</option>\n\t\t\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t<label class=\"columns L-6 M-6 S-12\">\n\t\t\t\t\t\t\t\t\t<select \n\t\t\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\t\t\tname=\"type_deal_id\"\n\t\t\t\t\t\t\t\t\t\t[(ngModel)]=\"form.type_deal_id\"\n\t\t\t\t\t\t\t\t\t\t#type_deal_id=\"ngModel\"\n\t\t\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !type_deal_id.valid}\">\n\t\t\t\t\t\t\t\t\t\t\t<option value=\"\">Tipo de contrato</option>\n\t\t\t\t\t\t\t\t\t\t\t<option *ngFor=\"let item of tipo_contrato\" [value]=\"item.id\">{{ item.desc }}</option>\n\t\t\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"columns L-3 M-3 S-12\">\n\t\t\t\t\t\t\t<div class=\"row\" *ngIf=\"form.type_deal_id == 3\">\n\t\t\t\t\t\t\t\t<label class=\"columns L-12 M-12 S-12\">\n\t\t\t\t\t\t\t\t\t<input \n\t\t\t\t\t\t\t\t\t\ttype=\"text\"\n\t\t\t\t\t\t\t\t\t\tplaceholder=\"Otro contrato\"\n\t\t\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\t\t\tname=\"other_contract\"\n\t\t\t\t\t\t\t\t\t\t[(ngModel)]=\"form.other_contract\"\n\t\t\t\t\t\t\t\t\t\t#other_contract=\"ngModel\"\n\t\t\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !other_contract.valid}\">\n\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t<div class=\"columns L-3 M-3 S-12\">\n\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t<label class=\"columns L-6 M-6 S-12\">\n\t\t\t\t\t\t\t\t\t<select \n\t\t\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\t\t\tname=\"type_payment_id\"\n\t\t\t\t\t\t\t\t\t\t[(ngModel)]=\"form.type_payment_id\"\n\t\t\t\t\t\t\t\t\t\t#type_payment_id=\"ngModel\"\n\t\t\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !type_payment_id.valid}\">\n\t\t\t\t\t\t\t\t\t\t\t<option value=\"\">Forma de abono</option>\n\t\t\t\t\t\t\t\t\t\t\t<option *ngFor=\"let item of forma_abono\" [value]=\"item.id\">{{ item.desc }}</option>\n\t\t\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t<label class=\"columns L-6 M-6 S-12\">\n\t\t\t\t\t\t\t\t\t<input \n\t\t\t\t\t\t\t\t\t\ttype=\"text\"\n\t\t\t\t\t\t\t\t\t\tplaceholder=\"Número\"\n\t\t\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\t\t\tname=\"num\"\n\t\t\t\t\t\t\t\t\t\t[(ngModel)]=\"form.num\"\n\t\t\t\t\t\t\t\t\t\t#num=\"ngModel\"\n\t\t\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !num.valid}\"\n\t\t\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"columns L-3 M-3 S-12\">\n\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t<label class=\"columns L-6 M-6 S-12\">\n\t\t\t\t\t\t\t\t\t<input \n\t\t\t\t\t\t\t\t\t\ttype=\"text\"\n\t\t\t\t\t\t\t\t\t\tplaceholder=\"Banco\"\n\t\t\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\t\t\tname=\"bank\"\n\t\t\t\t\t\t\t\t\t\t[(ngModel)]=\"form.bank\"\n\t\t\t\t\t\t\t\t\t\t#bank=\"ngModel\"\n\t\t\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !bank.valid}\"\n\t\t\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\n\t\t\t\t</div>\n\t\t\t</div>\n\n\t\t\t<div class=\"section-form\">\n\t\t\t\t<h3 class=\"header-section-form\">Información familiar</h3>\n\t\t\t\t<div class=\"content-section-form\">\n\t\t\t\t\t<ng-container *ngFor=\"let familiar of familiares; let i = index\">\n\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t<label class=\"columns L-3 M-3 S-12\">\n\t\t\t\t\t\t\t<input \n\t\t\t\t\t\t\t\ttype=\"text\"\n\t\t\t\t\t\t\t\tplaceholder=\"Nombres y apellidos\"\n\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\tname=\"name_familiar[{{i}}]\"\n\t\t\t\t\t\t\t\t[(ngModel)]=\"familiares[i].name_familiar\"\n\t\t\t\t\t\t\t\t#name_familiar=\"ngModel\"\n\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !name_familiar.valid}\"\n\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t</label>\n\t\t\t\t\t\t<div class=\"columns L-3 M-3 S-12\">\n\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t<label class=\"columns L-6 M-6 S-12\">\n\t\t\t\t\t\t\t\t\t<input \n\t\t\t\t\t\t\t\t\t\tonfocus=\"(this.type='date')\"\n\t\t\t\t\t\t\t\t\t\tmax=\"9999-12-31\"\n\t\t\t\t\t\t\t\t\t\tonblur=\"(this.type='text')\"\n\t\t\t\t\t\t\t\t\t\ttype=\"text\"\n\t\t\t\t\t\t\t\t\t\tplaceholder=\"Fecha de nacimiento\"\n\t\t\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\t\t\tname=\"birthdate_familiar[{{i}}]\"\n\t\t\t\t\t\t\t\t\t\t[(ngModel)]=\"familiares[i].birthdate_familiar\"\n\t\t\t\t\t\t\t\t\t\t#birthdate_familiar=\"ngModel\"\n\t\t\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !birthdate_familiar.valid}\"\n\t\t\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t<label class=\"columns L-6 M-6 S-12\">\n\t\t\t\t\t\t\t\t\t<input \n\t\t\t\t\t\t\t\t\t\ttype=\"text\"\n\t\t\t\t\t\t\t\t\t\tplaceholder=\"Nivel de educación\"\n\t\t\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\t\t\tname=\"education_familiar[{{i}}]\"\n\t\t\t\t\t\t\t\t\t\t[(ngModel)]=\"familiares[i].education_familiar\"\n\t\t\t\t\t\t\t\t\t\t#education_familiar=\"ngModel\"\n\t\t\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !education_familiar.valid}\"\n\t\t\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"columns L-3 M-3 S-12\">\n\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t<label class=\"columns L-6 M-6 S-12\">\n\t\t\t\t\t\t\t\t\t<select \n\t\t\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\t\t\tname=\"type_occupation_id_familiar[{{i}}]\"\n\t\t\t\t\t\t\t\t\t\t[(ngModel)]=\"familiares[i].type_occupation_id_familiar\"\n\t\t\t\t\t\t\t\t\t\t#type_occupation_id_familiar=\"ngModel\"\n\t\t\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !type_occupation_id_familiar.valid}\">\n\t\t\t\t\t\t\t\t\t\t<option value=\"\">Ocupación</option>\n\t\t\t\t\t\t\t\t\t\t<option *ngFor=\"let item of tipo_ocupacion\" [value]=\"item.id\">{{ item.desc }}</option>\n\t\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t<label class=\"columns L-6 M-6 S-12\">\n\t\t\t\t\t\t\t\t\t<input \n\t\t\t\t\t\t\t\t\t\ttype=\"text\"\n\t\t\t\t\t\t\t\t\t\tplaceholder=\"Teléfono\"\n\t\t\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\t\t\tname=\"phone_familiar[{{i}}]\"\n\t\t\t\t\t\t\t\t\t\t[(ngModel)]=\"familiares[i].phone_familiar\"\n\t\t\t\t\t\t\t\t\t\t#phone_familiar=\"ngModel\"\n\t\t\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !phone_familiar.valid}\"\n\t\t\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"columns L-3 M-3 S-12\">\n\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t<label class=\"columns L-6 M-6 S-12\">\n\t\t\t\t\t\t\t\t\t<select \n\t\t\t\t\t\t\t\t\t\tplaceholder=\"Tipo de familiar\"\n\t\t\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\t\t\tname=\"type_family_id[{{i}}]\"\n\t\t\t\t\t\t\t\t\t\t[(ngModel)]=\"familiares[i].type_family_id\"\n\t\t\t\t\t\t\t\t\t\t#type_family_id=\"ngModel\"\n\t\t\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !type_family_id.valid}\"\n\t\t\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t\t\t\t<option value=\"\">Tipo de familia</option>\n\t\t\t\t\t\t\t\t\t\t\t<option *ngFor=\"let item of tipo_familiar\" [value]=\"item.id\">{{ item.desc }}</option>\n\t\t\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t<label class=\"columns L-6 M-6 S-12\">\n\t\t\t\t\t\t\t\t\t<a *ngIf=\"i == 0\" (click)=\"duplicateRow()\" class=\"add-button\" ><img src=\"assets/img/button_add.svg\" alt=\"\">Agregar</a>\n\t\t\t\t\t\t\t\t\t<a *ngIf=\"i > 0\" (click)=\"deleteRow(i)\" class=\"add-button\" ><img src=\"assets/img/button_delete.svg\" alt=\"\">Eliminar</a>\n\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t</div>\n\t\t\t\t\t</ng-container>\n\t\t\t\t</div>\n\t\t\t</div>\n\n\t\t\t<div class=\"section-form\">\n\t\t\t\t<h3 class=\"header-section-form\">Información financiera</h3>\n\t\t\t\t<div class=\"content-section-form\">\n\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t<div class=\"columns L-3 M-3 S-12\">\n\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t<label class=\"columns L-6 M-6 S-12\">\n\t\t\t\t\t\t\t\t\t<input \n\t\t\t\t\t\t\t\t\t\ttype=\"text\"\n\t\t\t\t\t\t\t\t\t\tplaceholder=\"Total ingresos mensuales\"\n\t\t\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\t\t\t[pattern]=\"validators.number\" \n\t\t\t\t\t\t\t\t\t\tname=\"ingress\"\n\t\t\t\t\t\t\t\t\t\t[(ngModel)]=\"form.ingress\"\n\t\t\t\t\t\t\t\t\t\t#ingress=\"ngModel\"\n\t\t\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !ingress.valid}\"\n\t\t\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t<label class=\"columns L-6 M-6 S-12\">\n\t\t\t\t\t\t\t\t\t<input \n\t\t\t\t\t\t\t\t\t\ttype=\"text\"\n\t\t\t\t\t\t\t\t\t\tplaceholder=\"Total egresos mensuales\"\n\t\t\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\t\t\t[pattern]=\"validators.number\" \n\t\t\t\t\t\t\t\t\t\tname=\"egress\"\n\t\t\t\t\t\t\t\t\t\t[(ngModel)]=\"form.egress\"\n\t\t\t\t\t\t\t\t\t\t#egress=\"ngModel\"\n\t\t\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !egress.valid}\"\n\t\t\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"columns L-3 M-3 S-12\">\n\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t<label class=\"columns L-6 M-6 S-12\">\n\t\t\t\t\t\t\t\t\t<input \n\t\t\t\t\t\t\t\t\t\ttype=\"text\"\n\t\t\t\t\t\t\t\t\t\tplaceholder=\"Total activos\"\n\t\t\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\t\t\t[pattern]=\"validators.number\" \n\t\t\t\t\t\t\t\t\t\tname=\"assets\"\n\t\t\t\t\t\t\t\t\t\t[(ngModel)]=\"form.assets\"\n\t\t\t\t\t\t\t\t\t\t#assets=\"ngModel\"\n\t\t\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !assets.valid}\"\n\t\t\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t<label class=\"columns L-6 M-6 S-12\">\n\t\t\t\t\t\t\t\t\t<input \n\t\t\t\t\t\t\t\t\t\ttype=\"text\"\n\t\t\t\t\t\t\t\t\t\tplaceholder=\"Total pasivos\"\n\t\t\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\t\t\t[pattern]=\"validators.number\" \n\t\t\t\t\t\t\t\t\t\tname=\"liabilities\"\n\t\t\t\t\t\t\t\t\t\t[(ngModel)]=\"form.liabilities\"\n\t\t\t\t\t\t\t\t\t\t#liabilities=\"ngModel\"\n\t\t\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !liabilities.valid}\"\n\t\t\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<label class=\"columns L-3 M-3 S-12\">\n\t\t\t\t\t\t\t<input \n\t\t\t\t\t\t\t\ttype=\"text\"\n\t\t\t\t\t\t\t\tplaceholder=\"Otros ingresos o ingresos no operacionales\"\n\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\t[pattern]=\"validators.number\" \n\t\t\t\t\t\t\t\tname=\"other_ingress\"\n\t\t\t\t\t\t\t\t[(ngModel)]=\"form.other_ingress\"\n\t\t\t\t\t\t\t\t#other_ingress=\"ngModel\"\n\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !other_ingress.valid}\">\n\t\t\t\t\t\t</label>\n\t\t\t\t\t\t<label class=\"columns L-3 M-3 S-12\">\n\t\t\t\t\t\t\t<input \n\t\t\t\t\t\t\t\ttype=\"text\"\n\t\t\t\t\t\t\t\tplaceholder=\"Descripción de otros ingresos o ingresos no operacionales\"\n\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\tname=\"description\"\n\t\t\t\t\t\t\t\t[(ngModel)]=\"form.description\"\n\t\t\t\t\t\t\t\t#description=\"ngModel\"\n\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !description.valid}\">\n\t\t\t\t\t\t</label>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\n\t\t\t<div class=\"section-form\">\n\t\t\t\t<h3 class=\"header-section-form\">Operaciones en moneda extranjera</h3>\n\t\t\t\t<div class=\"content-section-form\">\n\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t<div class=\"columns L-3 M-3 S-12\">\n\t\t\t\t\t\t\t<radio-group \n\t\t\t\t\t\t\t\t[listData]=\"radio_options\"\n\t\t\t\t\t\t\t\ttitle=\"Realiza operaciones de moneda extanjera\"\n\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\tname=\"ext_operation\"\n\t\t\t\t\t\t\t\t[(ngModel)]=\"form.ext_operation\"\n\t\t\t\t\t\t\t\t#ext_operation=\"ngModel\"\n\t\t\t\t\t\t\t\t[isValid]=\"f.submitted && !ext_operation.valid\"\n\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t</radio-group>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"columns L-3 M-3 S-12\">\n\t\t\t\t\t\t\t<radio-group \n\t\t\t\t\t\t\t\t[listData]=\"radio_options\"\n\t\t\t\t\t\t\t\ttitle=\"Posee cuentas en el extranjero\"\n\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\tname=\"have_ext_accouts\"\n\t\t\t\t\t\t\t\t[(ngModel)]=\"form.have_ext_accouts\"\n\t\t\t\t\t\t\t\t#have_ext_accouts=\"ngModel\"\n\t\t\t\t\t\t\t\t[isValid]=\"f.submitted && !have_ext_accouts.valid\"\n\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t</radio-group>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"columns L-6 M-6 S-12\">\n\t\t\t\t\t\t\t<div class=\"row\" *ngIf=\"have_ext_accouts.value == 1\">\n\t\t\t\t\t\t\t\t<label class=\"columns L-half-5 M-half-5 S-12\">\n\t\t\t\t\t\t\t\t\t<input \n\t\t\t\t\t\t\t\t\t\ttype=\"text\"\n\t\t\t\t\t\t\t\t\t\tplaceholder=\"No. Cuenta\"\n\t\t\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\t\t\tname=\"num_ext\"\n\t\t\t\t\t\t\t\t\t\t[(ngModel)]=\"form.num_ext\"\n\t\t\t\t\t\t\t\t\t\t#num_ext=\"ngModel\"\n\t\t\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !num_ext.valid}\"\n\t\t\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t<label class=\"columns L-half-5 M-half-5 S-12\">\n\t\t\t\t\t\t\t\t\t<input \n\t\t\t\t\t\t\t\t\t\ttype=\"text\"\n\t\t\t\t\t\t\t\t\t\tplaceholder=\"Banco\"\n\t\t\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\t\t\tname=\"bank_ext\"\n\t\t\t\t\t\t\t\t\t\t[(ngModel)]=\"form.bank_ext\"\n\t\t\t\t\t\t\t\t\t\t#bank_ext=\"ngModel\"\n\t\t\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !bank_ext.valid}\"\n\t\t\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t<label class=\"columns L-half-5 M-half-5 S-12\">\n\t\t\t\t\t\t\t\t\t<input \n\t\t\t\t\t\t\t\t\t\ttype=\"text\"\n\t\t\t\t\t\t\t\t\t\tplaceholder=\"Moneda\"\n\t\t\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\t\t\tname=\"coin_ext\"\n\t\t\t\t\t\t\t\t\t\t[(ngModel)]=\"form.coin_ext\"\n\t\t\t\t\t\t\t\t\t\t#coin_ext=\"ngModel\"\n\t\t\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !coin_ext.valid}\"\n\t\t\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t<label class=\"columns L-half-5 M-half-5 S-12\">\n\t\t\t\t\t\t\t\t\t<select \n\t\t\t\t\t\t\t\t\t    required\n\t\t\t\t\t\t\t\t\t\tname=\"city_id_ext\"\n\t\t\t\t\t\t\t\t\t\t[(ngModel)]=\"form.city_id_ext\"\n\t\t\t\t\t\t\t\t\t\t#city_id_ext=\"ngModel\"\n\t\t\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !city_id_ext.valid}\">\n\t\t\t\t\t\t\t\t\t\t\t<option value=\"\">Ciudad</option>\n\t\t\t\t\t\t\t\t\t\t\t\t<option *ngFor=\"let item of ciudades\" [value]=\"item.id\">{{ item.desc }}</option>\n\t\t\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t<label class=\"columns  L-half-5 M-half-5 S-12\">\n\t\t\t\t\t\t\t\t\t<select\n\t\t\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\t\t\tname=\"country_id_ext\"\n\t\t\t\t\t\t\t\t\t\t[(ngModel)]=\"form.country_id_ext\"\n\t\t\t\t\t\t\t\t\t\t#country_id_ext=\"ngModel\"\n\t\t\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !country_id_ext.valid}\">\n\t\t\t\t\t\t\t\t\t\t<option value=\"\">País</option>\n\t\t\t\t\t\t\t\t\t\t<option *ngFor=\"let item of paises\" [value]=\"item.id\">{{ item.desc }}</option>\n\t\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t<label class=\"columns  L-half-5 M-half-5 S-12 label-check\" [ngClass]=\"{'input-invalid': f.submitted && !affirmation.valid}\">\n\t\t\t\t\t\t\t\t\tDeclaro que no realizo transacciones en moneda extranjera\n\t\t\t\t\t\t\t\t\t<input \n\t\t\t\t\t\t\t\t\t\ttype=\"checkbox\"\n\t\t\t\t\t\t\t\t\t\tname=\"affirmation\"\n\t\t\t\t\t\t\t\t\t\t[(ngModel)]=\"form.affirmation\"\n\t\t\t\t\t\t\t\t\t\t#affirmation=\"ngModel\"\n\t\t\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"columns L-12 M-12 S-12\" *ngIf=\"ext_operation.value == 1\">\n\t\t\t\t\t\t\t<div class=\"row other-text\">\n\t\t\t\t\t\t\t\t\t<label class=\"columns L-12 M-12 S-12\">\n\t\t\t\t\t\t\t\t\t\t<input \n\t\t\t\t\t\t\t\t\t\t\ttype=\"text\"\n\t\t\t\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\t\t\t\tname=\"description_ext_operation\"\n\t\t\t\t\t\t\t\t\t\t\t[(ngModel)]=\"form.description_ext_operation\"\n\t\t\t\t\t\t\t\t\t\t\t#description_ext_operation=\"ngModel\"\n\t\t\t\t\t\t\t\t\t\t\t[ngClass]=\"{'input-invalid': f.submitted && !description_ext_operation.valid}\"\n\t\t\t\t\t\t\t\t\t\t\tplaceholder=\"¿Cuáles operaciones de moneda extanjera realiza?\">\n\t\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\n\t\t\t<div class=\"section-form\">\n\t\t\t\t<h3 class=\"header-section-form\">Declaracion de persona expuesta publicamente (pep)</h3>\n\t\t\t\t<div class=\"content-section-form\">\n\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t<div class=\"columns L-6 M-6 S-12\">\n\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t<label class=\"columns L-6 M-6 S-12\">\n\t\t\t\t\t\t\t\t\t<radio-group \n\t\t\t\t\t\t\t\t\t\t[listData]=\"radio_options\"\n\t\t\t\t\t\t\t\t\t\ttitle=\"¿Maneje recursos públicos o tenga poder de disposición sobre estos?\"\n\t\t\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\t\t\tname=\"recursos_publicos\"\n\t\t\t\t\t\t\t\t\t\t[(ngModel)]=\"form.recursos_publicos\"\n\t\t\t\t\t\t\t\t\t\t#recursos_publicos=\"ngModel\"\n\t\t\t\t\t\t\t\t\t\t[isValid]=\"f.submitted && !recursos_publicos.valid\"\n\t\t\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t\t</radio-group>\n\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t<label class=\"columns L-6 M-6 S-12\">\n\t\t\t\t\t\t\t\t\t<radio-group \n\t\t\t\t\t\t\t\t\t\t[listData]=\"radio_options\"\n\t\t\t\t\t\t\t\t\t\ttitle=\"¿Tiene o goza de reconocimiento público?\"\n\t\t\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\t\t\tname=\"reconocimiento_publico\"\n\t\t\t\t\t\t\t\t\t\t[(ngModel)]=\"form.reconocimiento_publico\"\n\t\t\t\t\t\t\t\t\t\t#reconocimiento_publico=\"ngModel\"\n\t\t\t\t\t\t\t\t\t\t[isValid]=\"f.submitted && !reconocimiento_publico.valid\"\n\t\t\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t\t</radio-group>\n\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"columns L-6 M-6 S-12\">\n\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t<label class=\"columns L-12 M-12 S-12\">\n\t\t\t\t\t\t\t\t\t<radio-group \n\t\t\t\t\t\t\t\t\t\t[listData]=\"radio_options\"\n\t\t\t\t\t\t\t\t\t\ttitle=\"¿Tiene grado de poder público o desempeña una funcion pública prominente o destaca en el estado, relacionada con alguno de los cargos descritos en el decreto 1674 de 2016?\"\n\t\t\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\t\t\tname=\"grado_publico\"\n\t\t\t\t\t\t\t\t\t\t[(ngModel)]=\"form.grado_publico\"\n\t\t\t\t\t\t\t\t\t\t#grado_publico=\"ngModel\"\n\t\t\t\t\t\t\t\t\t\t[isValid]=\"f.submitted && !grado_publico.valid\"\n\t\t\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t\t</radio-group>\n\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"columns L-12 M-12 S-12\">\n\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t<label class=\"columns L-12 M-12 S-12\">\n\t\t\t\t\t\t\t\t\t<radio-group \n\t\t\t\t\t\t\t\t\t\t[inlineRadio]=\"true\"\n\t\t\t\t\t\t\t\t\t\t[listData]=\"radio_options\"\n\t\t\t\t\t\t\t\t\t\ttitle=\"¿Tiene familiares, hasta el segundo grado de consanguinidad, que encajen en los escenarios descritos previamente?\"\n\t\t\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\t\t\tname=\"grado_consanguinidad\"\n\t\t\t\t\t\t\t\t\t\t[(ngModel)]=\"form.grado_consanguinidad\"\n\t\t\t\t\t\t\t\t\t\t#grado_consanguinidad=\"ngModel\"\n\t\t\t\t\t\t\t\t\t\t[isValid]=\"f.submitted && !grado_consanguinidad.valid\"\n\t\t\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t\t</radio-group>\n\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\n\t\t\t<div class=\"section-form\">\n\t\t\t\t<h3 class=\"header-section-form\">Autorización de descuento</h3>\n\t\t\t\t<div class=\"content-section-form\">\n\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t<div class=\"columns L-12 M-12 S-12\">\n\t\t\t\t\t\t\t<div class=\"content-checks\" [ngClass]=\"{'input-invalid': f.submitted && !discount.valid}\">\n\t\t\t\t\t\t\t\tPor medio de la presente me permito comunicarles que he decidido afiliarme al Fondo de Empleados del Banco de Bogotá; en consecuencia me comprometo a aceptar los Estatutos, reglamentos y disposiciones en general de la Entidad. Autorizo expresamente a mi empleador para realizar las deducciones sobre mis ingresos laborales, para las cuotas de ahorros y aportes, créditos y todos aquellos descuentos reglamentarios, de acuerdo a sus Estatutos y decisiones de la Junta Directiva; sumas que solicito entregar al mencionado Fondo.\n\t\t\t\t\t\t\t\t<div class=\"row checkbox\">\n\t\t\t\t\t\t\t\t\t<label class=\"columns L-12 M-12 S-12\">\n\t\t\t\t\t\t\t\t\t\tAcepto <input \n\t\t\t\t\t\t\t\t\t\t\t\t\ttype=\"checkbox\"\n\t\t\t\t\t\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\t\t\t\t\t\tname=\"discount\"\n\t\t\t\t\t\t\t\t\t\t\t\t\t[(ngModel)]=\"form.discount\"\n\t\t\t\t\t\t\t\t\t\t\t\t\t#discount=\"ngModel\"\n\t\t\t\t\t\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\n\t\t\t<div class=\"section-form\">\n\t\t\t\t<h3 class=\"header-section-form\">Declaración voluntaria de origen de fondos</h3>\n\t\t\t\t<div class=\"content-section-form\">\n\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t<div class=\"columns L-12 M-12 S-12\">\n\t\t\t\t\t\t\t<div class=\"content-checks\" [ngClass]=\"{'input-invalid': f.submitted && !source.valid}\">\n\t\t\t\t\t\t\t\tBajo la gravedad de juramento y actuando en nombre propio realizo la siguiente declaración de origen y destinación de recursos al Fondo de Empleados Banco de Bogotá, con el fin de cumplir con las disposiciones señaladas en su Sistema de Administración del Riesgo de Lavado de Activos y de la Financiación del Terrorismo: 1. Declaro que los activos, ingresos, bienes y demás recursos provienen de actividades legales conforme a lo descrito en mi actividad y ocupación. 2. No admitiré que terceros vinculen mi actividad con dineros, recursos o activos relacionados con el delito de lavado de activos o destinados a la financiación del terrorismo. 3. Eximo al Fondo de Empleados Banco de Bogotá, de toda responsabilidad que se derive del comportamiento o el que se ocasione por la información falsa ó errónea suministrada en la presente declaración y en los documentos que respaldan o soporten mis afirmaciones. 4. Autorizo al Fondo de Empleados Banco de Bogotá, para que verifique y realice las consultas que estime necesarias con el propósito de confirmar la información registrada en este formulario. 5. Los recursos que utilizo para realizar los pagos e inversiones en el Fondo de Empleados Banco de Bogotá tienen procedencia lícita y están soportados con el desarrollo de actividades legítimas. 6. No he sido, ni me encuentro incluido en investigaciones relacionadas con Lavado de Activos o Financiación del Terrorismo. 7. Estoy informado de mi obligación de actualizar con una periodicidad como mínimo anual, la información que solicite la entidad por cada producto o servicio que utilice, suministrando la información documental exigida por el Fondo de Empleados Banco de Bogotá para dar cumplimiento a la normatividad vigente.\n\t\t\t\t\t\t\t\t<div class=\"row checkbox\">\n\t\t\t\t\t\t\t\t\t<label class=\"columns L-12 M-12 S-12\">\n\t\t\t\t\t\t\t\t\t\tAcepto <input type=\"checkbox\"\n\t\t\t\t\t\t\t\t\t\t\t\t\trequired\n\t\t\t\t\t\t\t\t\t\t\t\t\tname=\"source\"\n\t\t\t\t\t\t\t\t\t\t\t\t\t[(ngModel)]=\"form.source\"\n\t\t\t\t\t\t\t\t\t\t\t\t\t#source=\"ngModel\"\n\t\t\t\t\t\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\n\t\t\t<div class=\"section-form\">\n\t\t\t\t<button> <img src=\"assets/img/icon_button.svg\" alt=\"\"> imprimir formulario</button>\n\t\t\t</div>\n\n\t\t</form>\n\t\t\t\n\t</div>\n\t\n</div>\t"

/***/ }),

/***/ "./src/app/formulario/formulario.component.ts":
/*!****************************************************!*\
  !*** ./src/app/formulario/formulario.component.ts ***!
  \****************************************************/
/*! exports provided: FormularioComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormularioComponent", function() { return FormularioComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_service_manager_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../services/service-manager.service */ "./src/app/services/service-manager.service.ts");
/* harmony import */ var _shared_familiar__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../shared/familiar */ "./src/app/shared/familiar.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var FormularioComponent = /** @class */ (function () {
    function FormularioComponent(services) {
        this.services = services;
        this.form = {};
        this.familiares = [];
        this.validators = {
            email: /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)+$/,
            phone: /^[0-9]{7}$/,
            mobil: /^[3]{1}[0-9]{9}$/,
            number: /^[0-9]+$/
        };
        this.tipo_formato = [];
        this.genero = [];
        this.estado_civil = [];
        this.departamento = [];
        this.ciudades = [];
        this.paises = [];
        this.tipo_residencia = [];
        this.tipo_salario = [];
        this.tipo_contrato = [];
        this.forma_abono = [];
        this.tipo_ocupacion = [];
        this.tipo_familiar = [];
        this.radio_options = [
            { value: 1, name: 'Si' },
            { value: 2, name: 'No' }
        ];
        this.familiares.push(new _shared_familiar__WEBPACK_IMPORTED_MODULE_2__["Familiar"]());
    }
    FormularioComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.form.type_format_id = "";
        this.form.place_birth = "";
        this.form.type_status_id = "";
        this.form.type_payment_id = "";
        this.form.city_id = "";
        this.form.departament_id = "";
        this.form.type_home_id = "";
        this.form.type_salary_id = "";
        this.form.type_occupation_id = "";
        this.form.type_gender_id = "";
        this.form.type_deal_id = "";
        this.form.country_id_ext = "";
        this.form.city_id_ext = "";
        this.services.getInfo()
            .subscribe(function (data) {
            var lists = {};
            data = Object.keys(data).map(function (key) {
                lists[key] = data[key].map(function (item) {
                    return { id: item.id, desc: item.name };
                });
            });
            _this.tipo_formato = lists.tipo_formato;
            _this.genero = lists.tipo_genero;
            _this.estado_civil = lists.tipo_estado;
            _this.departamento = lists.departamento;
            _this.ciudades = lists.ciudad;
            _this.paises = lists.pais;
            _this.tipo_residencia = lists.tipo_vivienda;
            _this.tipo_salario = lists.tipo_de_salario;
            _this.tipo_contrato = lists.tipo_contrato;
            _this.forma_abono = lists.tipo_abono;
            _this.tipo_ocupacion = lists.tipo_ocupacion;
            _this.tipo_familiar = lists.tipo_familiar;
        });
    };
    FormularioComponent.prototype.duplicateRow = function () {
        this.familiares.push(new _shared_familiar__WEBPACK_IMPORTED_MODULE_2__["Familiar"]());
        console.log(this.familiares);
    };
    FormularioComponent.prototype.deleteRow = function (index) {
        this.familiares.splice(index, 1);
    };
    FormularioComponent.prototype.onSubmit = function () {
        var _this = this;
        this.form['familiares'] = this.familiares;
        console.log(this.form);
        this.services.sendVinculacion(this.form)
            .subscribe(function (data) {
            if (data.errors.length > 0) {
                _this.errors = data.errors;
                return;
            }
            _this.pdf_url.nativeElement.href = _this.services.baseUrl(data.link_pdf);
            console.log(_this.pdf_url.nativeElement);
            _this.pdf_url.nativeElement.click();
            console.log(data);
        });
    };
    FormularioComponent.prototype.lazyBoolean = function (condition) {
        return new Promise(function (resolve) {
            setTimeout(function (_) {
                console.log(condition);
                resolve(condition);
            }, 100);
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('pdf_url'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], FormularioComponent.prototype, "pdf_url", void 0);
    FormularioComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-formulario',
            template: __webpack_require__(/*! ./formulario.component.html */ "./src/app/formulario/formulario.component.html"),
            styles: [__webpack_require__(/*! ./formulario.component.css */ "./src/app/formulario/formulario.component.css")]
        }),
        __metadata("design:paramtypes", [_services_service_manager_service__WEBPACK_IMPORTED_MODULE_1__["ServiceManagerService"]])
    ], FormularioComponent);
    return FormularioComponent;
}());



/***/ }),

/***/ "./src/app/home-banner/home-banner.component.css":
/*!*******************************************************!*\
  !*** ./src/app/home-banner/home-banner.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".imagenes{\n\tposition: relative;\n}\n\n.masInfo{\n\tposition: absolute;\n\tdisplay: none;\n\tbackground-color: rgba(255, 255, 255, 0.9);\t\n\theight: 100%;\n\twidth: 100%;\n\ttop: 0;\n\tpadding: 10px;\n}\n\n.imagenes:hover .masInfo{\n\tdisplay: block;\n}\n\n.titulo{\n\tborder-bottom: solid 1px #4E4E4E;\n}\n\n.titulo h1{\n\tmargin-bottom: 0px;\n}\n\n.descripcion {\n\tmargin-top: 5px;\n}\n\n.descript{\n\tword-break: break-word;\n}\n\n.verMas{\n\tdisplay: block;\n\theight: 23px;\n\twidth: 23px;\n\tbackground-image: url(\"/assets/img/assets.png\");\n\tbackground-position: -120px 0;\n\ttext-align: center;\n\tposition: absolute;\n\tbottom: 10px;\n\tleft: 50% - 23px ;\n\tright: 50%;\n}\n\n.OfVirtual{\n\tbackground-color: #6E2562;\t\n\tborder: solid 2px white;\n}\n\n.Vinculate{\n\tbackground-color: #CC4229;\n\tborder: solid 2px white;\n}\n\n.Ahorra{\n\tbackground-color: #28487E;\n\tborder: solid 2px white;\n}\n\n.Creditos{\n\tbackground-color: #BFCA43;\n\tborder: solid 2px white;\n}\n\n.icono {\n\tmargin: auto;\n\tmargin-top: 10%; \n\twidth: 50%;\n\theight: 50%;\n\tborder-radius: 50%;\n\tborder:solid 3px white;\n\tposition: relative;\n/*\tmin-width: 130px;\n    min-height: 130px;*/\n}\n\n.texto {\n\tmargin: 10px auto;\n\ttext-align: center;\n}\n\n.no-center{\n\tfloat: left;\n\tclear: none;\n}\n\n@media screen and (max-width: 550px) {\n\tb {font-size: 1em}\n}\n\n@media screen and (min-width: 551px) and (max-width: 1024px) {\n\tb {font-size: 1em}\n}\n\n@media screen and (max-width: 39.9375em) {\n\t.accesosDirectos{\n\t\theight: 150px !important;\n\t}\n}"

/***/ }),

/***/ "./src/app/home-banner/home-banner.component.html":
/*!********************************************************!*\
  !*** ./src/app/home-banner/home-banner.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"L-10 M-12 S-10  centered columns banner\">\n\n\t<ngx-slick class=\"carousel\" #slickModal=\"slick-modal\" [config]=\"slideConfig\" >\n\t\t<div ngxSlickItem class=\"slide\"  *ngFor=\"let pic of banners\"  (click)=\"openLink(pic.link)\" [ngClass]=\"{linkable: pic.link}\" (afterChange)=\"afterChange($event)\">\n\t\t\t<div class=\"L-12 M-12 S-12 columns imagenes\">\n\t\t\t\t<img [src]=\"pic.picture\">\n\t\t\t\t<div class=\"masInfo\">\n\t\t\t\t\t<div class=\"titulo\"><h1>{{ pic.title }}</h1></div>\n\t\t\t\t\t\n\t\t\t\t\t<div class=\"descript\"><a [innerHTML]=\"pic.Descripcion\"></a></div>\n\t\t\t\t\t\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</ngx-slick>\n\t<span #unlockslick></span>\n\n\t<!-- <div class=\"L-12 M-10 S-12 centered columns  bannerContainer\" *ngFor=\"let pic of banners\" > -->\n\t<!-- <div class=\"L-10 M-10 S-10 columns centered bannerContainer\" > -->\n\t\t<!-- div class=\"L-12 M-12 S-12 columns imagenes\">\n\t\t\t\t<img [src]=\"pic.picture\">\n\t\t\t\t<div class=\"masInfo\">\n\t\t\t\t\t<div class=\"titulo\"><h1>{{ pic.title }}</h1></div>\n\t\t\t\t\t\n\t\t\t\t\t<div class=\"descript\"><a [innerHTML]=\"pic.Descripcion\"></a></div>\n\t\t\t\t\t<a class=\"verMas\" [href]=\"pic.link\" target=\"_blank\"></a>\n\t\t\t\t</div>\n\t\t</div>\n\t</div> -->\n\t<div class=\"L-12 M-10 S-12 columns centered\">\n\t\t<div class=\"row\">\n\t\t\t<div class=\"L-3 M-3 S-6 columns accesosDirectos OfVirtual\"  appSquareDiv>\n\t\t\t\t<a class=\"accesosContainer\"  href=\"https://servicios2.inube.com.co/linix/v6/860006643/login.php?nit=860006643\" target=\"_blank\">\t\n\t\t\t\t\t<div class=\"icono\">\n\t\t\t\t\t\t<span class=\"icon_svg OfVirtIcon_svg\"></span>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"texto\"><b>Oficina<b class=\"Resaltar\"> virtual</b></b></div>\n\t\t\t\t</a>\n\t\t\t</div>\n\t\t\t<div class=\"L-3 M-3 S-6 columns accesosDirectos Vinculate\" [routerLink]=\"['vinculate']\" appSquareDiv>\n\t\t\t\t<a class=\"accesosContainer\">\t\n\t\t\t\t\t<div class=\"icono\">\n\t\t\t\t\t\t<span class=\"icon_svg vinculIcon_svg\"></span>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"texto\"><b>Vincúlate<b class=\"Resaltar\"> ahora</b></b></div>\n\t\t\t\t</a>\n\t\t\t</div>\n\t\t\t<div class=\"L-3 M-3 S-6 columns accesosDirectos Ahorra\" [routerLink]=\"['servicios']\" appSquareDiv>\n\t\t\t\t<a class=\"accesosContainer\">\t\n\t\t\t\t\t<div class=\"icono\">\n\t\t\t\t\t\t<span class=\"icon_svg AhorrIcon_svg\"></span>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"texto\"><b>Ahorra y cumple tus<b class=\"Resaltar\"> metas</b></b></div>\n\t\t\t\t</a>\n\t\t\t</div>\n\t\t\t<div class=\"L-3 M-3 S-6 columns accesosDirectos Creditos\"  [routerLink]=\"['/servicios']\" fragment=\"creditos\" appSquareDiv>\n\t\t\t\t<a class=\"accesosContainer\">\t\n\t\t\t\t\t<div class=\"icono\">\n\t\t\t\t\t\t<span class=\"icon_svg CredIcon_svg\"></span>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"texto\"><b>Créditos para lo que<b class=\"Resaltar\"> necesites</b></b></div>\n\t\t\t\t</a>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>"

/***/ }),

/***/ "./src/app/home-banner/home-banner.component.ts":
/*!******************************************************!*\
  !*** ./src/app/home-banner/home-banner.component.ts ***!
  \******************************************************/
/*! exports provided: HomeBannerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeBannerComponent", function() { return HomeBannerComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _services_service_manager_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/service-manager.service */ "./src/app/services/service-manager.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HomeBannerComponent = /** @class */ (function () {
    function HomeBannerComponent(service) {
        this.service = service;
        this.convenios = [];
    }
    HomeBannerComponent.prototype.openLink = function (link) {
        if (link)
            window.open(link, '_blank');
    };
    HomeBannerComponent.prototype.afterChange = function (e) {
        console.log(this.newFocus);
    };
    HomeBannerComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.slideConfig = {
            autoplay: true,
            autoplaySpeed: 2000,
        };
        this.service.getNovedades()
            .subscribe(function (data) {
            console.log(data);
            _this.banners = data.filter(function (item) { return item.is_principal; });
            if (_this.banners.length <= 0) {
                _this.banners = data.map(function (item) {
                    var description = (item.description.length <= 10) ? item.description : item.description;
                    return {
                        picture: _environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].URL_SERVER + item.image.url,
                        Descripcion: description,
                        link: item.link,
                        title: item.title
                    };
                });
            }
            else {
                _this.banners = [_this.banners.map(function (item) {
                        var description = (item.description.length <= 10) ? item.description : item.description;
                        return {
                            picture: _environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].URL_SERVER + item.image.url,
                            Descripcion: description,
                            link: item.link,
                            principal: item.is_principal,
                            title: item.title
                        };
                    })[0]];
                console.log(_this.banners);
            }
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('unlockslick'),
        __metadata("design:type", Object)
    ], HomeBannerComponent.prototype, "newFocus", void 0);
    HomeBannerComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-home-banner',
            template: __webpack_require__(/*! ./home-banner.component.html */ "./src/app/home-banner/home-banner.component.html"),
            styles: [__webpack_require__(/*! ./home-banner.component.css */ "./src/app/home-banner/home-banner.component.css")]
        }),
        __metadata("design:paramtypes", [_services_service_manager_service__WEBPACK_IMPORTED_MODULE_2__["ServiceManagerService"]])
    ], HomeBannerComponent);
    return HomeBannerComponent;
}());



/***/ }),

/***/ "./src/app/home-contactanos/home-contactanos.component.css":
/*!*****************************************************************!*\
  !*** ./src/app/home-contactanos/home-contactanos.component.css ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@media screen and (max-width: 39.9375em) {\n\n\t.contactanosLeft, .contactanosRight{\n\t\tdisplay: block;\n\t\tmargin: 0 auto 2rem;\n\t\tfloat: none;\n\t\tclear: both;\n\t}\t\n\n\t.contactIcon{\n\t\tpadding: 0;\n\t}\n\n}\n"

/***/ }),

/***/ "./src/app/home-contactanos/home-contactanos.component.html":
/*!******************************************************************!*\
  !*** ./src/app/home-contactanos/home-contactanos.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"L-12 M-12 S-12 columns\">\n\t<div class=\"L-10 M-10 S-10 columns centered contactanosContainer\">\n\t\t\t<div class=\"L-12 M-12 S-12 ContactUs\">\n\t\t\t\t<div class=\"L-3 M-3 S-10  columns contactanosLeft\">\n\t\t\t\t\t<div class=\"Icon Contactanos\"></div>\n\t\t\t\t\t<div class=\"Title Contactanos\"><h1>Contáctanos</h1></div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"L-12 M-12 S-12 ContactUs\">\n\t\t\t\t<div class=\"L-3 M-3 S-10  columns contactanosLeft\">\n\t\t\t\t\t<div class=\"contactIcon\">\n\t\t\t\t\t\t<div class=\"iconTel\"></div>\n\t\t\t\t\t\t<div class=\"text\">\n\t\t\t\t\t\t\t<a routerLink=\"/contactanos\">Consulta aquí nuestras extensiones.</a>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"contactIcon\">\n\t\t\t\t\t\t<div class=\"iconMail\"></div>\n\t\t\t\t\t\t<div class=\"text\">\n\t\t\t\t\t\t\t<a href=\"mailto:ebautis@bancodebogota.com.co\" target=\"_blank\">ebautis@bancodebogota.com.co</a>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"contactIcon\">\n\t\t\t\t\t\t<div class=\"iconDir\"></div>\n\t\t\t\t\t\t<div class=\"text\">\n\t\t\t\t\t\t\t<a>Calle 36 N° 7 – 41 Oficinas 203 y 302 Bogotá.<br>Bogotá, Colombia</a>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"L-6 M-6 S-12 columns contactanosMap\">\n\t\t\t\t\t<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d497.1043941979988!2d-74.0670239861775!3d4.623482594437752!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e3f998648077529%3A0x59b0da9208f05573!2zQ2wuIDM2ICM3LTQxLCBCb2dvdMOh!5e0!3m2!1ses-419!2sco!4v1551970515445\" width=\"100%\" height=\"300\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>\n\t\t\t\t\t\n\t\t\t\t</div>\n\t\t\t\t<div class=\"L-3 M-3 S-10 contactanosRight\">\n\t\t\t\t\t<div class=\"contactIcon\">\n\t\t\t\t\t\t<div class=\"iconPolProt\"></div>\n\t\t\t\t\t\t<div class=\"text\">\n\t\t\t\t\t\t\t<a [href]=\"politica_datos\" target=\"_blank\">Política de protección de datos personales</a>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"contactIcon\">\n\t\t\t\t\t\t<div class=\"supersolidaria\">\n\t\t\t\t\t\t\t<img src=\"./assets/img/supersolidaria.png\" alt=\"\">\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t</div>\t\n</div>\t"

/***/ }),

/***/ "./src/app/home-contactanos/home-contactanos.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/home-contactanos/home-contactanos.component.ts ***!
  \****************************************************************/
/*! exports provided: HomeContactanosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeContactanosComponent", function() { return HomeContactanosComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HomeContactanosComponent = /** @class */ (function () {
    function HomeContactanosComponent() {
    }
    HomeContactanosComponent.prototype.ngOnInit = function () {
        this.politica_datos = _environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].URL_SERVER + '/formatos/politica_de_proteccion_de_datos_personales.pdf';
    };
    HomeContactanosComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-home-contactanos',
            template: __webpack_require__(/*! ./home-contactanos.component.html */ "./src/app/home-contactanos/home-contactanos.component.html"),
            styles: [__webpack_require__(/*! ./home-contactanos.component.css */ "./src/app/home-contactanos/home-contactanos.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], HomeContactanosComponent);
    return HomeContactanosComponent;
}());



/***/ }),

/***/ "./src/app/home-convenios/home-convenios.component.css":
/*!*************************************************************!*\
  !*** ./src/app/home-convenios/home-convenios.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".slider-convenios{\n\tposition: relative;\n    height: 210px;\n    margin: 0 auto;\n}\n\n\n\n\n@media screen and (max-width: 39.9375em) {\n\t.scrollConvenios{\n\t\toverflow: hidden;\n\t\twidth: 210px;\n    \theight: 210px;\n\t}\n\t.conveniosSlider{\n\t\twidth: 1260px;\n\t\tmargin: 0;\n\n\t}\n\n\t.slider-convenios{\n    \twidth: 210px;\n\t}\n}"

/***/ }),

/***/ "./src/app/home-convenios/home-convenios.component.html":
/*!**************************************************************!*\
  !*** ./src/app/home-convenios/home-convenios.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"L-12 M-12 S-12 columns\">\n\t<div class=\"L-10 M-10 S-12 columns centered conveniosContainer\">\n\t\t<div class=\"Icon Convenios\"></div>\n\t\t<div class=\"Title Convenios\"><h1>Convenios y Alianzas</h1></div>\n\n\t\t<ngx-slick class=\"carousel\" #slickModal=\"slick-modal\" [config]=\"slideConfig\" >\n\t\t\t<div ngxSlickItem class=\" convenio slide\"  *ngFor=\"let item of Conv\" [routerLink]=\"['/convenios', item.id]\" >\n\t\t\t\t<figure><img src=\"{{item.picture}}\" alt=\"\" ></figure>\n\t\t\t</div>\n\n\t\t</ngx-slick>\n\t\t\n\t</div>\n</div>"

/***/ }),

/***/ "./src/app/home-convenios/home-convenios.component.ts":
/*!************************************************************!*\
  !*** ./src/app/home-convenios/home-convenios.component.ts ***!
  \************************************************************/
/*! exports provided: HomeConveniosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeConveniosComponent", function() { return HomeConveniosComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _services_service_manager_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/service-manager.service */ "./src/app/services/service-manager.service.ts");
/* harmony import */ var ngx_device_detector__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-device-detector */ "./node_modules/ngx-device-detector/ngx-device-detector.umd.js");
/* harmony import */ var ngx_device_detector__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(ngx_device_detector__WEBPACK_IMPORTED_MODULE_3__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var HomeConveniosComponent = /** @class */ (function () {
    function HomeConveniosComponent(service, deviceService) {
        this.service = service;
        this.deviceService = deviceService;
        this.Conv = [];
        this.DisplayConvenios = [];
        this.Position = 0;
        this.slideConfig = {};
    }
    HomeConveniosComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.slideConfig = this.deviceService.isMobile() ? {
            "slidesToShow": 1, "slidesToScroll": 1
        } : { "slidesToShow": 6, "slidesToScroll": 1 };
        this.slideConfig['autoplay'] = true;
        this.slideConfig['autoplaySpeed'] = 2000;
        this.service.getConvenios()
            .subscribe(function (data) {
            console.log(data);
            _this.Conv = data.convenios;
            _this.Conv = _this.Conv.map(function (item, i) {
                return {
                    "id": item.id,
                    "Title": item.title,
                    "picture": _environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].URL_SERVER + item.image.url
                };
            });
        });
    };
    HomeConveniosComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-home-convenios',
            template: __webpack_require__(/*! ./home-convenios.component.html */ "./src/app/home-convenios/home-convenios.component.html"),
            styles: [__webpack_require__(/*! ./home-convenios.component.css */ "./src/app/home-convenios/home-convenios.component.css")]
        }),
        __metadata("design:paramtypes", [_services_service_manager_service__WEBPACK_IMPORTED_MODULE_2__["ServiceManagerService"], ngx_device_detector__WEBPACK_IMPORTED_MODULE_3__["DeviceDetectorService"]])
    ], HomeConveniosComponent);
    return HomeConveniosComponent;
}());



/***/ }),

/***/ "./src/app/home-opiniones/home-opiniones.component.css":
/*!*************************************************************!*\
  !*** ./src/app/home-opiniones/home-opiniones.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@media screen and (max-width: 39.9375em) {\n\t.opinionesContent{\n\t\tborder-right: 0;\n\t}\n}\n\n"

/***/ }),

/***/ "./src/app/home-opiniones/home-opiniones.component.html":
/*!**************************************************************!*\
  !*** ./src/app/home-opiniones/home-opiniones.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"L-12 M-12 S-12 columns\">\n\t<div class=\"L-10 M-10 S-10 columns centered opinionesContainer\">\n\t\t\t<div class=\"Icon Opiniones\"></div>\n\t\t\t<div class=\"Title Opiniones\"><h1>Opiniones de nuestros afiliados</h1></div>\n\n\n\t\t\t<ngx-slick class=\"carousel\" #slickModal=\"slick-modal\" [config]=\"slideConfig\" >\n\t\t\t\t<div class=\"L-3 M-6 S-hide opinionesContent\" ngxSlickItem *ngFor=\"let item of Opiniones\">\n\t\t\t\t\t<div class=\"comillaUno\"><h1>\"</h1></div>\n\t\t\t\t\t<div class=\"opinion\"><a [innerHTML]=\"item.Opinion\"></a></div>\n\t\t\t\t\t<div class=\"nombre\"><a class=\"Resaltar\">{{item.Nombre}}</a><br>{{item.Cargo}}</div>\n\t\t\t\t\t<div class=\"comillaDos\"><h1>\"</h1></div>\n\t\t\t\t</div>\n\t\t\t</ngx-slick>\n\n\n\t\t\t\n\t</div>\n</div>\t"

/***/ }),

/***/ "./src/app/home-opiniones/home-opiniones.component.ts":
/*!************************************************************!*\
  !*** ./src/app/home-opiniones/home-opiniones.component.ts ***!
  \************************************************************/
/*! exports provided: HomeOpinionesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeOpinionesComponent", function() { return HomeOpinionesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_service_manager_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/service-manager.service */ "./src/app/services/service-manager.service.ts");
/* harmony import */ var ngx_device_detector__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-device-detector */ "./node_modules/ngx-device-detector/ngx-device-detector.umd.js");
/* harmony import */ var ngx_device_detector__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(ngx_device_detector__WEBPACK_IMPORTED_MODULE_2__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HomeOpinionesComponent = /** @class */ (function () {
    function HomeOpinionesComponent(api, deviceService) {
        this.api = api;
        this.deviceService = deviceService;
        this.slideConfig = {};
    }
    HomeOpinionesComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.slideConfig = this.deviceService.isMobile() ? {
            "slidesToShow": 1, "slidesToScroll": 1
        } : { "slidesToShow": 4, "slidesToScroll": 1 };
        this.api.getOpiniones().subscribe(function (data) {
            _this.Opiniones = data.opiniones.map(function (item, i) {
                return {
                    "id": (i + 1),
                    "Opinion": item.opinion,
                    "Nombre": item.name,
                    "Cargo": item.position,
                };
            });
        });
    };
    HomeOpinionesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-home-opiniones',
            template: __webpack_require__(/*! ./home-opiniones.component.html */ "./src/app/home-opiniones/home-opiniones.component.html"),
            styles: [__webpack_require__(/*! ./home-opiniones.component.css */ "./src/app/home-opiniones/home-opiniones.component.css")]
        }),
        __metadata("design:paramtypes", [_services_service_manager_service__WEBPACK_IMPORTED_MODULE_1__["ServiceManagerService"], ngx_device_detector__WEBPACK_IMPORTED_MODULE_2__["DeviceDetectorService"]])
    ], HomeOpinionesComponent);
    return HomeOpinionesComponent;
}());



/***/ }),

/***/ "./src/app/home-popup/home-popup.component.css":
/*!*****************************************************!*\
  !*** ./src/app/home-popup/home-popup.component.css ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/home-popup/home-popup.component.html":
/*!******************************************************!*\
  !*** ./src/app/home-popup/home-popup.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [ngClass]=\"{selected: opened == true}\" class=\"PopUpContainer\">\n\t<div class=\"L-6 M-8 S-10 row PopUpParent\">\n\t\t<div class=\"L-12 M-12 S-12 row pageBanner PopUp\">\n\t\t\t<div class=\"L-12 M-12  columns imagenBanner PopUp\" appImagesSizesBanner>\n\t\t\t\t<img src=\"./assets/img/BanWhite.jpg\">\n\t\t\t\t<div class=\"PageTitle\"><h2>{{infoConvenios[0].Title}}</h2><h3>{{infoConvenios[0].Short_Desc}}</h3></div>\n\t\t\t</div>\t\n\t\t</div>\n\t\t<div class=\"L-12 M-12 S-12 row pageContainer PopUp\">\n\t\t\t<div class=\"L-12 M-12 S-12 columns insideContent\">\n\t\t\t\t<img class=\"L-3 M-4 S-5 columns\" src=\"{{infoConvenios[0].picture}}\" alt=\"\">\n\t\t\t\t<div class=\"L-9 M-8 S-7 texto\">\n\t\t\t\t\t<ul>\n\t\t\t\t\t\t<li>{{infoConvenios[0].Long_Desc}}</li>\n\t\t\t\t\t</ul>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\t\n\t\t<div class=\"cerrar\" (click)=\"opened = false\">X</div>\n\t</div>\n</div>\t"

/***/ }),

/***/ "./src/app/home-popup/home-popup.component.ts":
/*!****************************************************!*\
  !*** ./src/app/home-popup/home-popup.component.ts ***!
  \****************************************************/
/*! exports provided: HomePopupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePopupComponent", function() { return HomePopupComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_data_services__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/data.services */ "./src/app/services/data.services.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HomePopupComponent = /** @class */ (function () {
    function HomePopupComponent() {
        this.opened = true;
    }
    HomePopupComponent.prototype.ngOnInit = function () {
        this.infoConvenios = _services_data_services__WEBPACK_IMPORTED_MODULE_1__["PopUp"];
    };
    HomePopupComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-home-popup',
            template: __webpack_require__(/*! ./home-popup.component.html */ "./src/app/home-popup/home-popup.component.html"),
            styles: [__webpack_require__(/*! ./home-popup.component.css */ "./src/app/home-popup/home-popup.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], HomePopupComponent);
    return HomePopupComponent;
}());



/***/ }),

/***/ "./src/app/home/home.component.css":
/*!*****************************************!*\
  !*** ./src/app/home/home.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/home/home.component.html":
/*!******************************************!*\
  !*** ./src/app/home/home.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-home-banner></app-home-banner>\n<app-home-convenios></app-home-convenios>\n<app-home-opiniones></app-home-opiniones>\n<app-home-contactanos></app-home-contactanos>\n<!-- <app-home-popup></app-home-popup> -->\n"

/***/ }),

/***/ "./src/app/home/home.component.ts":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HomeComponent = /** @class */ (function () {
    function HomeComponent() {
    }
    HomeComponent.prototype.ngOnInit = function () {
    };
    HomeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.css */ "./src/app/home/home.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/pagina-convenio/pagina-convenio.component.css":
/*!***************************************************************!*\
  !*** ./src/app/pagina-convenio/pagina-convenio.component.css ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".PageTitle h2, .PageTitle h3{\n    color: #28487e;\n}\n\n/*.pageContainer{\n    background-color: rgb(240, 240, 240);\n}*/\n\n.insideContent .columns{\n\tpadding-right: 2px;\n\tfont-size: 0rem;\n}\n\n.slick-track{\n\tmin-height: 300px;\n}\n\n.slick-slide{\n\theight: auto;\n}\n\n.slick-convenios .slick-prev{\n\tleft: 7px;\n    -webkit-filter: brightness(0.5);\n            filter: brightness(0.5);\n    z-index: 9;\n}\n\n.slick-convenios .slick-next{\n\tright: 7px;\n    -webkit-filter: brightness(0.5);\n            filter: brightness(0.5);\n}\n\n.slideSlick{\n\twidth: 100%;\n\theight: 400px;\n\tposition: relative;\n\tbackground: #333;\n}\n\n.slideSlick img{\n\t\n    position: absolute;\n    right: 50%;\n    bottom: 50%;\n    -webkit-transform: translate(50%, 50%);\n            transform: translate(50%, 50%);\n}\n\n.slideSlick img.horizontal-image{\n\twidth: 100%;\n    height: auto;\n}\n\n.slideSlick img.vertical-image{\n\theight: 100%;\n    width: auto;\n}\n"

/***/ }),

/***/ "./src/app/pagina-convenio/pagina-convenio.component.html":
/*!****************************************************************!*\
  !*** ./src/app/pagina-convenio/pagina-convenio.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"pageContent\" *ngIf=\"infoConvenios\">\n\t<div class=\"L-10 M-10 S-10 row pageBanner\">\n\t\t<div class=\"L-10 M-8 S-12 columns imagenBanner ConveniosPage\" appImagesSizesBanner>\n\t\t\t<img src=\"./assets/img/BanWhite.jpg\">\n\t\t\t<div class=\"PageTitle\"><h2>{{infoConvenios.Title}}</h2><h3>{{infoConvenios.Short_Desc}}</h3></div>\n\t\t</div>\t\n\t\t<div class=\"L-2 M-2 S-12 columns bannerBotones\">\n\t\t\t<a class=\"L-12 M-12 S-6 columns OfVirtual\" href=\"https://servicios2.inube.com.co/linix/v6/860006643/login.php?nit=860006643\" target=\"_blank\">\n\t\t\t\t<div class=\"icono\"><img src=\"./assets/img/OfVirtIcon.png\"></div>\n\t\t\t\t<div class=\"texto\"><b>Oficina<b class=\"Resaltar\"> virtual</b></b></div>\n\t\t\t</a>\n\t\t\t<div class=\"L-12 M-12 S-6 columns Vinculate\" [routerLink]=\"['/vinculate']\">\n\t\t\t\t<div class=\"icono\"><img src=\"./assets/img/vinculIcon.png\"></div>\n\t\t\t\t<div class=\"texto\"><b>Vincúlate<b class=\"Resaltar\"> ahora</b></b></div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t<div class=\"L-10 M-10 S-10 row pageContainer\">\n\t\t<div class=\"L-12 M-12 S-12 columns insideContent\">\n\t\t\t<div class=\"L-3 M-4 S-12 columns\">\n\t\t\t\t\t<img   src=\"{{infoConvenios.picture}}\">\t\n\t\t\t</div>\n\t\t\t\n\t\t\t<div class=\"L-9 M-8 S-12 columns\">\n\t\t\t\t<ngx-slick class=\"carousel slick-convenios\" #slickModal=\"slick-modal\" [config]=\"slideConfig\" (afterChange)=\"afterChange($event)\">\n\t\t\t\t\t<div class=\"slideSlick\" ngxSlickItem  *ngFor=\"let image of infoConvenios.slides\">\n\t\t\t\t\t\t<img (load)=\"loadImge(img)\" [src]=\"image\" #img>\t\n\t\t\t\t\t</div>\n\t\t\t\t</ngx-slick>\n\t\t\t\t<div class=\"texto\" [innerHTML]=\"infoConvenios.Long_Desc | safe:'html'\"></div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\t\n\t<div class=\"L-10 M-10 S-10 row pageContainer\">\n\t\t<app-convenios-page></app-convenios-page>\n\t</div>\n</div>\t"

/***/ }),

/***/ "./src/app/pagina-convenio/pagina-convenio.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/pagina-convenio/pagina-convenio.component.ts ***!
  \**************************************************************/
/*! exports provided: PaginaConvenioComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaginaConvenioComponent", function() { return PaginaConvenioComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_service_manager_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/service-manager.service */ "./src/app/services/service-manager.service.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PaginaConvenioComponent = /** @class */ (function () {
    function PaginaConvenioComponent(route, service) {
        this.route = route;
        this.service = service;
        this.convenios = [];
    }
    PaginaConvenioComponent.prototype.escogerConvenio = function (list, id) {
        var returnData = [];
        for (var _i = 0, list_1 = list; _i < list_1.length; _i++) {
            var item = list_1[_i];
            if (item.id == id) {
                returnData.push(item);
                this.infoConvenios = {
                    "Title": item.title,
                    "Long_Desc": item.description,
                    "picture": _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].URL_SERVER + item.image.url,
                    "slides": item.carrousel.map(function (img) {
                        return _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].URL_SERVER + img.url.url;
                    })
                };
            }
        }
        return returnData;
    };
    PaginaConvenioComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.slideConfig = {
            "slidesToShow": 1,
            "slidesToScroll": 1,
            "infinite": true,
        };
        this.route.params.subscribe(function (params) {
            _this.service.getConvenios()
                .subscribe(function (data) {
                _this.convenios = data.convenios;
                _this.escogerConvenio(_this.convenios, parseInt(params.Title));
            });
        });
    };
    PaginaConvenioComponent.prototype.loadImge = function (image) {
        if (event['path'][0].width > event['path'][0].height) {
            image.className = "horizontal-image";
        }
        else {
            image.className = "vertical-image";
        }
    };
    PaginaConvenioComponent.prototype.afterChange = function (event) {
    };
    PaginaConvenioComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-pagina-convenio',
            template: __webpack_require__(/*! ./pagina-convenio.component.html */ "./src/app/pagina-convenio/pagina-convenio.component.html"),
            styles: [__webpack_require__(/*! ./pagina-convenio.component.css */ "./src/app/pagina-convenio/pagina-convenio.component.css")],
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], _services_service_manager_service__WEBPACK_IMPORTED_MODULE_2__["ServiceManagerService"]])
    ], PaginaConvenioComponent);
    return PaginaConvenioComponent;
}());



/***/ }),

/***/ "./src/app/pipes/html-safe.pipe.ts":
/*!*****************************************!*\
  !*** ./src/app/pipes/html-safe.pipe.ts ***!
  \*****************************************/
/*! exports provided: HtmlSafePipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HtmlSafePipe", function() { return HtmlSafePipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HtmlSafePipe = /** @class */ (function () {
    function HtmlSafePipe(sanitizer) {
        this.sanitizer = sanitizer;
    }
    HtmlSafePipe.prototype.transform = function (value, type) {
        switch (type) {
            case "html":
                return this.sanitizer.bypassSecurityTrustHtml(value);
            case "script":
                return this.sanitizer.bypassSecurityTrustScript(value);
            case "style":
                return this.sanitizer.bypassSecurityTrustStyle(value);
            case "url":
                return this.sanitizer.bypassSecurityTrustUrl(value);
            case "resourceUrl":
                return this.sanitizer.bypassSecurityTrustResourceUrl(value);
            default: throw new Error("Invalid safe type specified: " + type);
        }
    };
    HtmlSafePipe = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({
            name: 'safe'
        }),
        __metadata("design:paramtypes", [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["DomSanitizer"]])
    ], HtmlSafePipe);
    return HtmlSafePipe;
}());



/***/ }),

/***/ "./src/app/seguridad/seguridad.component.css":
/*!***************************************************!*\
  !*** ./src/app/seguridad/seguridad.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/seguridad/seguridad.component.html":
/*!****************************************************!*\
  !*** ./src/app/seguridad/seguridad.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"pageContent\">\n\t<div class=\"L-10 M-10 S-10 row pageBanner\">\n\t\t<div class=\"L-10 M-8 S-12 columns imagenBanner Seguridad\" appImagesSizesBanner>\n\t\t\t<img src=\"./assets/img/BanSeguridad.jpg\">\n\t\t\t<div class=\"PageTitle\"><h2>Seguridad</h2><h3>recomendaciones para mantener tu cuenta segura</h3></div>\n\t\t</div>\t\n\t\t<div class=\"L-2 M-2 S-12 columns bannerBotones\">\n\t\t\t<a class=\"L-12 M-12 S-6 columns OfVirtual\" href=\"https://servicios2.inube.com.co/linix/v6/860006643/login.php?nit=860006643\" target=\"_blank\">\n\t\t\t\t<div class=\"icono\"><img src=\"./assets/img/OfVirtIcon.png\"></div>\n\t\t\t\t<div class=\"texto\"><b>Oficina<b class=\"Resaltar\"> virtual</b></b></div>\n\t\t\t</a>\n\t\t\t<div class=\"L-12 M-12 S-6 columns Vinculate\" [routerLink]=\"['/vinculate']\">\n\t\t\t\t<div class=\"icono\"><img src=\"./assets/img/vinculIcon.png\"></div>\n\t\t\t\t<div class=\"texto\"><b>Vincúlate<b class=\"Resaltar\"> ahora</b></b></div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t<div class=\"L-10 M-10 S-10 row pageContainer\">\n\t\t<div>\n\t\t\t<div class=\"Icon SeguridadInfo\"></div>\n\t\t\t<div class=\"Title SeguridadInfo\"><h1>Recomendaciones</h1></div>\n\t\t</div>\n\t\t<div class=\"L-12 M-12 S-12 columns insideContent\">\n\t\t\t<img class=\"L-3 M-4 S-12 columns\" src=\"./assets/img/seguridad.jpg\" alt=\"\">\n\t\t\t<div class=\"L-9 M-8 S-12 texto\">\n\t\t\t\t<ul class=\"VinculateInfoList\">\n\t\t\t\t\t<li>Utilice siempre equipos de uso personal para realizar sus transacciones (oficina o casa), no use computadores de sitios públicos, su usuario y contraseña se podrían ver comprometidos. </li>\n\t\t\t\t\t<br/>\n\t\t\t\t\t<li>Digite siempre la dirección del Fondo de Empleados, <a href=\"https://febdb.com.co/\" target=\"_blank\">febdb.com.co</a> directamente en la barra de su navegador y asegúrese que aparece precedida por https. </li>\n\t\t\t\t\t<br/>\n\t\t\t\t\t<li>Cambie periódicamente su contraseña, recuerde que es personal e intransferible. </li>\n\t\t\t\t\t<br/>\n\t\t\t\t\t<li>Cada vez que finalice su actividad en el portal del fondo de empleados, cierre la sesión y asegúrese de no tener su usuario activo. </li>\n\t\t\t\t\t<br/>\n\t\t\t\t\t<li>El Fondo de empleados Banco de Bogotá nunca solicitará información confidencial para actualización de datos, relacionada con números de sus productos financieros y claves correspondientes a través de correo electrónico. </li>\n\t\t\t\t\t<br/>\n\t\t\t\t\t<li>Periódicamente revise que los equipos desde los cuales se conecta al Fondo de Empleados Banco de Bogotá estén libres de virus, software espía, troyanos; ya que pueden comprometer la seguridad de la información que maneja. </li>\n\t\t\t\t\t<br/>\n\t\t\t\t\t<li>Verifique que se encuentra en la página del Fondo de Empleados Banco de Bogotá, si aparece en la parte inferior o superior izquierda de su navegador la imagen de un candado. </li>\n\t\t\t\t\t<br/>\n\t\t\t\t\t<li>Nunca almacene sus contraseñas en los navegadores que use. Memorícelas en lo posible. </li>\n\t\t\t\t\t<br/>\n\t\t\t\t\t<li>Nunca responda a ninguna solicitud de información personal ni confidencial a través de un correo electrónico. </li>\n\t\t\t\t\t<br/>\n\t\t\t\t\t<li>Instale en sus equipos programas licenciados de protección contra virus, troyanos, pharming,phishing y keylogger. </li>\n\t\t\t\t</ul>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\t"

/***/ }),

/***/ "./src/app/seguridad/seguridad.component.ts":
/*!**************************************************!*\
  !*** ./src/app/seguridad/seguridad.component.ts ***!
  \**************************************************/
/*! exports provided: SeguridadComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SeguridadComponent", function() { return SeguridadComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_service_manager_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/service-manager.service */ "./src/app/services/service-manager.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SeguridadComponent = /** @class */ (function () {
    function SeguridadComponent(service) {
        this.service = service;
    }
    SeguridadComponent.prototype.ngOnInit = function () {
    };
    SeguridadComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-seguridad',
            template: __webpack_require__(/*! ./seguridad.component.html */ "./src/app/seguridad/seguridad.component.html"),
            styles: [__webpack_require__(/*! ./seguridad.component.css */ "./src/app/seguridad/seguridad.component.css")]
        }),
        __metadata("design:paramtypes", [_services_service_manager_service__WEBPACK_IMPORTED_MODULE_1__["ServiceManagerService"]])
    ], SeguridadComponent);
    return SeguridadComponent;
}());



/***/ }),

/***/ "./src/app/services/data.services.ts":
/*!*******************************************!*\
  !*** ./src/app/services/data.services.ts ***!
  \*******************************************/
/*! exports provided: banner, ConvCategorias, PopUp, conveniosSlider, Opiniones, creditos, Historia, junta, formulario, servcred, contacto */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "banner", function() { return banner; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConvCategorias", function() { return ConvCategorias; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PopUp", function() { return PopUp; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "conveniosSlider", function() { return conveniosSlider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Opiniones", function() { return Opiniones; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "creditos", function() { return creditos; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Historia", function() { return Historia; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "junta", function() { return junta; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "formulario", function() { return formulario; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "servcred", function() { return servcred; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "contacto", function() { return contacto; });
var banner = [
    {
        "id": 1,
        "Titulo": "Titulo",
        "Descripcion": "Esta es una <a class='Resaltar'>descripción</a> corta del convenio",
        "picture": "./assets/img/ImgCuadr_0003_stock-photo-tower-from-sand-and-blue-flag-on-the-coast-600653054.jpg",
    },
    {
        "id": 2,
        "Titulo": "Titulo",
        "Descripcion": "Esta es una <a class='Resaltar'>descripción</a> corta del convenio",
        "picture": "./assets/img/ImgCuadr_0004_stock-photo-toward-adventure-girl-relaxing-and-enjoying-road-trip-happy-girl-rides-into-the-sunset-in-vint.jpg",
    },
    {
        "id": 3,
        "Titulo": "Titulo",
        "Descripcion": "Esta es una <a class='Resaltar'>descripción</a> corta del convenio",
        "picture": "./assets/img/ImgCuadr_0006_stock-photo-plant-growing-in-savings-coins-investment-and-interest-concept-365284043.jpg",
    },
    {
        "id": 4,
        "Titulo": "Titulo",
        "Descripcion": "Esta es una <a class='Resaltar'>descripción</a> corta del convenio",
        "picture": "./assets/img/ImgCuadr_0007_stock-photo-people-charity-family-real-estate-and-home-concept-close-up-of-man-and-girl-holding-house-keys.jpg",
    },
    {
        "id": 5,
        "Titulo": "Titulo",
        "Descripcion": "Esta es una <a class='Resaltar'>descripción</a> corta del convenio",
        "picture": "./assets/img/ImgCuadr_0009_stock-photo-mother-and-daughter-enjoying-sunset-on-a-lake-387450340.jpg",
    },
    {
        "id": 6,
        "Titulo": "Titulo",
        "Descripcion": "Esta es una <a class='Resaltar'>descripción</a> corta del convenio",
        "picture": "./assets/img/ImgCuadr_0011_stock-photo-healthy-woman-celebrating-during-a-beautiful-sunset-happy-and-free-556808413.jpg",
    },
    {
        "id": 7,
        "Titulo": "Titulo",
        "Descripcion": "Esta es una <a class='Resaltar'>descripción</a> corta del convenio",
        "picture": "./assets/img/ImgCuadr_0012_stock-photo-healthy-food-vegetables-for-heart-heath-on-wooden-turquoise-background-1116095816.jpg",
    },
    {
        "id": 8,
        "Titulo": "Titulo",
        "Descripcion": "Esta es una <a class='Resaltar'>descripción</a> corta del convenio",
        "picture": "./assets/img/ImgCuadr_0013_stock-photo-hand-of-business-woman-wearing-wood-watch-has-smart-phone-mouse-plant-and-keyboard-placed-on-7.jpg",
    },
];
var ConvCategorias = [
    {
        "id": "",
        "Nombre": "Todos",
        "Clase": "Todos",
    },
    {
        "id": 1,
        "Nombre": "Turismo",
        "Clase": "Turismo",
    },
    {
        "id": 2,
        "Nombre": "Salud",
        "Clase": "Salud",
    },
    {
        "id": 3,
        "Nombre": "Seguros",
        "Clase": "Seguros",
    },
    {
        "id": 4,
        "Nombre": "Tecnología",
        "Clase": "Tecnologia",
    },
    {
        "id": 5,
        "Nombre": "Concesionarios",
        "Clase": "Concesionarios",
    },
];
var PopUp = [
    {
        "id": 1,
        "Cat_Id": 1,
        "picture": "./assets/img/conv1.jpg",
        "Title": "Óptica Colombiana",
        "Short_Desc": "Esta es una descripción corta",
        "Descuento": "20%",
        "Long_Desc": "Esta es una descripción larga del convenio",
    },
];
var conveniosSlider = [
    {
        "id": 1,
        "Cat_Id": 1,
        "picture": "./assets/img/conv1.jpg",
        "Title": "Óptica Colombiana",
        "Short_Desc": "Esta es una descripción corta",
        "Descuento": "20%",
        "Long_Desc": "Esta es una descripción larga del convenio",
    },
    {
        "id": 2,
        "Cat_Id": 2,
        "picture": "./assets/img/conv2.jpg",
        "Title": "Merpes",
        "Short_Desc": "Esta es una descripción corta",
        "Descuento": "20%",
        "Long_Desc": "Esta es una descripción larga del convenio",
    },
    {
        "id": 3,
        "Cat_Id": 3,
        "picture": "./assets/img/conv3.jpg",
        "Title": "Bike Hike",
        "Short_Desc": "Esta es una descripción corta",
        "Descuento": "20%",
        "Long_Desc": "Esta es una descripción larga del convenio",
    },
    {
        "id": 4,
        "Cat_Id": 4,
        "picture": "./assets/img/conv4.jpg",
        "Title": "Clinica Barraquer",
        "Short_Desc": "Esta es una descripción corta",
        "Descuento": "20%",
        "Long_Desc": "Esta es una descripción larga del convenio",
    },
    {
        "id": 5,
        "Cat_Id": 5,
        "picture": "./assets/img/conv5.jpg",
        "Title": "Naturaleza & Descanso",
        "Short_Desc": "Esta es una descripción corta",
        "Descuento": "20%",
        "Long_Desc": "Esta es una descripción larga del convenio",
    },
    {
        "id": 6,
        "Cat_Id": 1,
        "picture": "./assets/img/conv6.jpg",
        "Title": "La Cabaña Alpina",
        "Short_Desc": "Esta es una descripción corta",
        "Descuento": "20%",
        "Long_Desc": "Esta es una descripción larga del convenio",
    },
    {
        "id": 7,
        "Cat_Id": 2,
        "picture": "./assets/img/conv7.jpg",
        "Title": "Popsy",
        "Short_Desc": "Esta es una descripción corta",
        "Descuento": "20%",
        "Long_Desc": "Esta es una descripción larga del convenio",
    },
    {
        "id": 8,
        "Cat_Id": 5,
        "picture": "./assets/img/conv4.jpg",
        "Title": "Título convenio",
        "Short_Desc": "Esta es una descripción corta",
        "Descuento": "20%",
        "Long_Desc": "Esta es una descripción larga del convenio",
    },
];
var Opiniones = [
    {
        "id": 1,
        "Opinion": "Lorem ipsum dolor sit amet, <a class='Resaltar'> consectetur </a> adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        "Nombre": "Juan Florez",
        "Cargo": "Gerente Operaciones",
    },
    {
        "id": 2,
        "Opinion": "Lorem ipsum dolor sit amet, <a class='Resaltar'>consectetur </a> adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        "Nombre": "Juan Florez",
        "Cargo": "Gerente Operaciones",
    },
    {
        "id": 3,
        "Opinion": "Lorem ipsum dolor sit amet, <a class='Resaltar'>consectetur </a> adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        "Nombre": "Juan Florez",
        "Cargo": "Gerente Operaciones",
    },
    {
        "id": 4,
        "Opinion": "Lorem ipsum dolor sit amet, <a class='Resaltar'>consectetur </a> adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        "Nombre": "Juan Florez",
        "Cargo": "Gerente Operaciones",
    },
];
var creditos = [
    {
        "id": 1,
        "Clase": "LibDes",
        "Titulo": "Crédito Ordinario",
        "Picture": "./assets/img/SliderCred_0006_Consumo.jpg",
        "Objeto": "Libre destino, consumo.",
        "Cupo": "Hasta el 100% el valor de los ahorros permanentes, los intereses capitalizados más los aportes  personales (sujeto a cumplimiento de requisitos).",
        "Antiguedad": "Mínima seis (6) meses consecutivos como asociado al Fondo.",
        "Plazo": "Hasta 36 meses.",
        "Amortizacion": "Mensual, mensual y semestral ó fijo.",
        "IntCorriente": "Aprobado por la Junta Directiva - Ver tasa vigente.",
        "IntMora": "Aprobada por la Junta Directiva - Ver tasa vigente.",
        "Garantia": "Garantizado con los ahorros y los aportes personales del solicitante y firma de la correspondiente libranza.",
    },
    {
        "id": 2,
        "Clase": "LibInv",
        "Titulo": "Crédito Extraordinario",
        "Picture": "./assets/img/SliderCred_0005_Libre.jpg",
        "Objeto": "Libre Inversión",
        "Cupo": "Se determinará tomando hasta 4 veces el valor de los ahorros permanentes más los aportes personales.(sujeto a cumplimiento de requisitos).",
        "Antiguedad": "Mínima doce (12) meses consecutivos como asociado al Fondo",
        "Plazo": "Hasta 60 meses.",
        "Amortizacion": "Mensual, mensual y semestral ó fijo",
        "IntCorriente": "Aprobado por la Junta Directiva",
        "IntMora": "Aprobado por la Junta Directiva",
        "Garantia": "Garantizado con pagaré suscrito por el deudor y la firma de uno o más codeudores solidarios, así: Cuando los créditos extraordinarios superen los depósitos totales (ahorros mas aportes personales) y supere también en veinte (20) SMLV su valor (hoy $14.754.340), se exigirá dos codeudores solidarios. En caso contrario se exigirá solo uno.",
    },
    {
        "id": 3,
        "Clase": "Viv",
        "Titulo": "Crédito Extraordinario",
        "Picture": "./assets/img/SliderCred_0004_Vivienda.jpg",
        "Objeto": "Complementario de Vivienda",
        "Cupo": "Se determinará tomando hasta 6 veces el valor de los ahorros permanentes más los aportes personales (sujeto a cumplimiento de requisitos).",
        "Antiguedad": "Mínima veinticuatro (24) meses consecutivos como asociado al Fondo",
        "Plazo": "Hasta 60 meses.",
        "Amortizacion": "Mensual, mensual y semestral",
        "IntCorriente": "Aprobada por la Junta Directiva",
        "IntMora": "Aprobada por la Junta Directiva",
        "Garantia": "Garantizado con pagaré suscrito por el deudor y la firma de uno o más codeudores solidarios, así: Cuando los créditos extraordinarios superen a los depósitos totales (ahorros mas aportes personales) y supere también en veinte (20) SMLV su valor (hoy $14.754.340), se exigirá dos codeudores solidarios. En caso contrario se exigirá solo uno.",
        "Requisito": "Ser beneficiario de crédito para vivienda otorgado por el Banco o Filial",
    },
    {
        "id": 4,
        "Clase": "Estu",
        "Titulo": "Crédito Extraordinario",
        "Picture": "./assets/img/SliderCred_0003_Estudio.jpg",
        "Objeto": "Estudio",
        "Cupo": "Hasta el 80% del valor de la orden de la matrícula universitaria del asociado, siempre y cuando este valor este dentro de las cuatro (4) veces el monto de los ahorros permanentes más los aportes personales (sujeto a cumplimiento de requisitos).",
        "Antiguedad": "Mínima doce (12) meses consecutivos como asociado al Fondo",
        "Plazo": "Hasta 6 meses",
        "Amortizacion": "Mensual, mensual y semestral ó fijo",
        "IntCorriente": "Aprobada por la Junta Directiva",
        "IntMora": "Aprobada por la Junta Directiva",
        "Garantia": "Garantizado con pagaré suscrito por el deudor y la firma de uno o más codeudores solidarios, así: Cuando los créditos extraordinarios superen a los depósitos totales (ahorros mas aportes personales) y supere también en veinte (20) SMLV su valor (hoy $14.754.340), se exigirá dos codeudores solidarios. En caso contrario se exigirá solo uno.",
    },
    {
        "id": 5,
        "Clase": "Vinc",
        "Titulo": "Crédito Extraordinario",
        "Picture": "./assets/img/SliderCred_0002_Vinculacion.jpg",
        "Objeto": "Vinculación",
        "Cupo": "Hasta cuatro (4) SMLV (sujeto a cumplimiento de requisitos).",
        "Antiguedad": "Mínima Un (1) mes como asociado al Fondo y un año de vinculación al Banco de Bogotá o a la Filial correspondiente.",
        "Plazo": "Hasta 12 meses",
        "Amortizacion": "Mensual, mensual y semestral ó fijo",
        "IntCorriente": "Aprobada por la Junta Directiva",
        "IntMora": "Aprobada por la Junta Directiva",
        "Garantia": "Garantizado con pagaré suscrito por el deudor y la firma de un codeudor.",
    },
    {
        "id": 6,
        "Clase": "Antic",
        "Titulo": "Anticipo",
        "Picture": "./assets/img/SliderCred_0001_Anticipo.jpg",
        "Objeto": "de prima semestral o antigüedad",
        "Cupo": "Hasta el 50% del sueldo mensual del asociado solicitante (sujeto a cumplimiento de requisitos), teniendo en cuenta lo estipulado en el artículo 3° del reglamento de crédito (descárguelo en la pestaña “Conócenos”).",
        "Antiguedad": "Mínima 12 meses como asociado al Fondo",
        "Plazo": "Hasta 6 meses",
        "Amortizacion": "Fijo a la siguiente prima.",
        "IntCorriente": "Aprobada por la Junta Directiva",
        "IntMora": "Aprobada por la Junta Directiva",
        "Garantia": "Ver artículo 9º. Del reglamento de Crédito (descárguelo en la pestaña “Conócenos”)",
    },
    {
        "id": 7,
        "Clase": "Promo",
        "Titulo": "Promociones",
        "Picture": "./assets/img/SliderCred_0000_Promociones.jpg",
        "Objeto": "Promociones",
        "Cupo": "Hasta cuatro (4) SMLV (sujeto a cumplimiento de requisitos).",
        "Antiguedad": "Mínima doce (12) meses consecutivos como asociado al Fondo",
        "Plazo": "Hasta 12 meses",
        "Amortizacion": "Mensual, mensual y semestral o fijo",
        "IntCorriente": "Aprobada por la Junta Directiva",
        "IntMora": "Aprobada por la Junta Directiva",
        "Garantia": "arantizado con pagaré suscrito por el deudor y la firma de un codeudor solidarios.",
        "Requisito": "Exclusivamente compra de artículos ofrecidos por los proveedores que el Fondo ha elegido por condiciones de precios, calidad y garantías.",
    },
];
var Historia = [
    {
        "id": 1,
        "anno": "1945",
        "Descripcion": "El Fondo de Empleados es una entidad de la economía solidaria fundada el 24 de agosto 1945, con la denominación “Fondo Privado de los Empleados del Banco de Bogotá”, con personería jurídica expedida por el entonces Ministerio de Gobierno bajo la Resolución 033 del 31 de enero de 1946.",
    },
    {
        "id": 2,
        "anno": "1990",
        "Descripcion": "El Fondo celebra sus 45 años de existencia, cambia su imagen y su denominación por (Fondo de Empleados del Banco de Bogotá) y reforma sus estatutos para ajustarlos al Decreto 1481 de 1989.",
    },
    {
        "id": 3,
        "anno": "2009",
        "Descripcion": "En diciembre de 2009, el Fondo de Empleados del Banco de Bogotá renovó su imagen, para mantenerla asociada con la del Banco de Bogotá.",
    },
    {
        "id": 4,
        "anno": "2018",
        "Descripcion": "En la actualidad, Fondo de Empleados del Banco de Bogotá tiene cubrimiento nacional y continúa cumpliendo con sus objetivos fundamentales: Fomentar el ahorro y otorgar préstamos a sus asociados con el fin de cubrir necesidades financieras y mejorar la calidad de vida.",
    },
];
var junta = [
    {
        "id": 1,
        "Clase": "JuntaDir",
        "Titulo": "Junta Directiva",
        "anno": "2018 - 2020",
        "Texto1": "<h1>Principales<h1><a>Julian Alfonso Sinisterra Reyes <br/>Fernando Baquero Gacharna <br/>Graciela Rey Barbosa <br/> Diego Alejandro Montoya Ossa</a>",
        "Texto2": "<h1>Suplentes<h1><a>Nohora Betty Muñoz Sossa <br/> Verónica Rocha Corredor <br/> Fernando Enrique Escobar Arango <br/> Carlos Fernando Nieto Martínez <br/> Luis Arturo Ortiz Rodríguez</a>",
    },
    {
        "id": 2,
        "Clase": "ControlSoc",
        "Titulo": "Comité de Control Social",
        "anno": "2018 - 2020",
        "Texto1": "<h1>Principales<h1><a>Guillermo Cardona Sánchez <br/> Gustavo Adolfo Prada Forero <br/> Sonia del Carmen Cely Amézquita</a>",
        "Texto2": "<h1>Suplentes<h1><a>Francisco Sánchez Zambrano <br/> Diana Carolina Romero Nieto <br/> Daniel Gustavo Aguilar Hernández</a>",
    },
    {
        "id": 3,
        "Clase": "Apelaciones",
        "Titulo": "Comité de apelaciones",
        "anno": "2018 - 2020",
        "Texto1": "<h1><h1/><a>Jaime Arias Hernández  - c.c. 19&#39;417,229 <br/> César Orlando León Torres - c.c. 79&#39;443,814 <br> Alvaro Laureano Guerrero Cerón -  c.c. 19&#39;422,652<a/>",
    },
    {
        "id": 4,
        "Clase": "RevFisc",
        "Titulo": "RevisorFiscal",
        "anno": "REVISAR AUDITORES LTDA No 830.093.979.4",
        "Texto1": "<h1>Principal<h1><a>María Patricia Castro Sánchez <br/>C.C. 51.990.309 <br/>patriciacastro@revisarauditores.com.co <br/>T.P. 133591 T.</a>",
        "Texto2": "<h1>Suplente<h1><a>Luis Armando Contreras Paez <br/> C.C. 79.461.524 <br/> direccionrf@revisarauditores.com.co <br/> T.P. 49964 T.</a>",
    },
];
var formulario = [
    {
        "id": 1,
        "Nombre": "Formulario de afiliación o actualización de datos",
        "Link": "/formulario",
        "router": "internal"
    },
    {
        "id": 2,
        "Nombre": "Autorización Central de Riesgos",
        "Link": "/formatos/autorizacion_central_riesgos.doc",
        "router": "link"
    },
    {
        "id": 3,
        "Nombre": "Autorización para el Tratamiento de Datos Personales",
        "Link": "/formatos/autorizacion_tratamiento_datos_personales.docx",
        "router": "link"
    },
    {
        "id": 4,
        "Nombre": "Exención GMF",
        "Link": "/formatos/exencion_gmf.doc",
        "router": "link"
    },
    {
        "id": 5,
        "Nombre": "Exención GMF Varios Proveedores",
        "Link": "/formatos/exencion_gmf_proveedores.doc",
        "router": "link"
    },
    {
        "id": 6,
        "Nombre": "Libranza",
        "Link": "/formatos/libranza.xlsx",
        "router": "link"
    },
    {
        "id": 7,
        "Nombre": "Solicitud y Autorización de Giro a Terceros",
        "Link": "/formatos/solicitud_autorizacion_giro_terceros.doc",
        "router": "link"
    },
    {
        "id": 8,
        "Nombre": "Formato Balance",
        "Link": "/formatos/Balance.xlsx",
        "router": "link"
    },
    {
        "id": 9,
        "Nombre": "Formato para Crédito Ordinario",
        "Link": "/formatos/credito_ordinario.xlsx",
        "router": "link"
    },
    {
        "id": 10,
        "Nombre": "Formato para Crédito Extraordinario",
        "Link": "/formatos/credito_extraordinario.xlsx",
        "router": "link"
    },
];
var servcred = [
    {
        "id": 1,
        "Clase": "LibDes",
        "Titulo": "Ordinario",
        "Picture": "./assets/img/SliderCred_0006_Consumo.jpg",
        "Plazo": "36 meses",
        "TasaEa": "7.27%",
        "TasaNa": "7.0411%",
        "Cupo": "Aportes + Ahorro + Intereses K",
        "Capacidad": "Hasta 1 vez",
    },
    {
        "id": 2,
        "Clase": "LibInv",
        "Titulo": "Extraordinario",
        "Picture": "./assets/img/SliderCred_0005_Libre.jpg",
        "Plazo": "36 meses    48 meses    60 meses",
        "TasaEa": "11.68%    13.96%    15.12%",
        "TasaNa": "11.1018%   13.1424%   14.1653%",
        "Cupo": "Aportes + Ahorro",
        "Capacidad": "Hasta 4 veces",
    },
    {
        "id": 3,
        "Clase": "Viv",
        "Titulo": "Complementario de vivienda",
        "Picture": "./assets/img/SliderCred_0004_Vivienda.jpg",
        "Plazo": "48 meses    60 meses",
        "TasaEa": "10.56%   12.82%",
        "TasaNa": "10.0840%   12.1212%",
        "Cupo": "Aportes + Ahorro",
        "Capacidad": "Hasta 6 veces",
    },
    {
        "id": 4,
        "Clase": "Estu",
        "Titulo": "Estudio",
        "Picture": "./assets/img/SliderCred_0003_Estudio.jpg",
        "Plazo": "6 meses",
        "TasaEa": "10.56%",
        "TasaNa": "10.0840%",
        "Cupo": "Aportes + Ahorro",
        "Capacidad": "Hasta 4 veces",
    },
    {
        "id": 5,
        "Clase": "Vinc",
        "Titulo": "Vinculación",
        "Picture": "./assets/img/SliderCred_0002_Vinculacion.jpg",
        "Plazo": "12 meses",
        "TasaEa": "11.68%",
        "TasaNa": "11.1018%",
        "Cupo": "Hasta 4 S.M.L.V",
    },
    {
        "id": 6,
        "Clase": "Antic",
        "Titulo": "Anticipo de prima",
        "Picture": "./assets/img/SliderCred_0001_Anticipo.jpg",
        "Plazo": "6 meses",
        "TasaEa": "10.56%",
        "TasaNa": "10.0840%",
        "Cupo": "Hasta el 50* del sueldo mensual",
    },
    {
        "id": 7,
        "Clase": "Promo",
        "Titulo": "Promoción",
        "Picture": "./assets/img/SliderCred_0000_Promociones.jpg",
        "Plazo": "12 meses 18 meses",
        "TasaEa": "11.68%",
        "TasaNa": "11.1018%",
        "Cupo": "Aportes + Ahorro",
        "Capacidad": "Hasta 4 veces",
    },
];
var contacto = [
    {
        "id": 1,
        "Clase": "Asoc",
        "Area": "Servicio al asociado",
        "Correo": "wgordil@bancodebogota.com.co",
        "Ext": "Ext - 1698",
    },
    {
        "id": 2,
        "Clase": "Cont",
        "Area": "Contabilidad y cartera",
        "Correo": "jgomez9@bancodebogota.com.co",
        "Ext": "Ext - 1649",
    },
    {
        "id": 3,
        "Clase": "Oper",
        "Area": "Operaciones",
        "Correo": "eherna7@bancodebogota.com.co",
        "Ext": "Ext - 1177",
    },
    {
        "id": 4,
        "Clase": "Segur",
        "Area": "Seguros y servicios",
        "Correo": "dmanza1@bancodebogota.com.co",
        "Ext": "Ext - 1697",
    },
    {
        "id": 5,
        "Clase": "Sist",
        "Area": "Sistemas",
        "Correo": "crami18@bancodebogota.com.co",
        "Ext": "Ext - 1384",
    },
    {
        "id": 6,
        "Clase": "Febdb",
        "Area": "Fondo de Empleados",
        "Correo": "Calle 36 No 7-41 of 302",
        "Ext": "PBX: 3320032 TELS: 3382578 - 3382941 - 2456395",
    },
];


/***/ }),

/***/ "./src/app/services/service-manager.service.ts":
/*!*****************************************************!*\
  !*** ./src/app/services/service-manager.service.ts ***!
  \*****************************************************/
/*! exports provided: ServiceManagerService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServiceManagerService", function() { return ServiceManagerService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ServiceManagerService = /** @class */ (function () {
    function ServiceManagerService(http) {
        this.http = http;
    }
    ServiceManagerService.prototype.getServicios = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].API_ENDPOINT + "/getServicios")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (response) { return response.json(); }));
    };
    ServiceManagerService.prototype.getBeneficios = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].API_ENDPOINT + "/getBeneficios")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (response) { return response.json(); }));
    };
    ServiceManagerService.prototype.getComites = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].API_ENDPOINT + "/getComites")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (response) { return response.json(); }));
    };
    ServiceManagerService.prototype.getVinculate = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].API_ENDPOINT + "/getVinculate")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (response) { return response.json(); }));
    };
    ServiceManagerService.prototype.getNovedades = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].API_ENDPOINT + "/getNovedades")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (response) { return response.json(); }));
    };
    ServiceManagerService.prototype.getConvenios = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].API_ENDPOINT + "/getConvenios")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (response) { return response.json(); }));
    };
    ServiceManagerService.prototype.getInfo = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].API_ENDPOINT + "/getInfo")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (response) { return response.json(); }));
    };
    ServiceManagerService.prototype.getContactos = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].API_ENDPOINT + "/getContactos")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (response) { return response.json(); }));
    };
    ServiceManagerService.prototype.getOpiniones = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].API_ENDPOINT + "/getOpiniones")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (response) { return response.json(); }));
    };
    ServiceManagerService.prototype.getDocuments = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].API_ENDPOINT + "/getDocumentos")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (response) { return response.json(); }));
    };
    ServiceManagerService.prototype.sendVinculacion = function (form_data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].API_ENDPOINT + "/form", form_data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (response) { return response.json(); }));
    };
    ServiceManagerService.prototype.baseUrl = function (url) {
        return _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].URL_SERVER + '/' + url;
    };
    ServiceManagerService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_1__["Http"]])
    ], ServiceManagerService);
    return ServiceManagerService;
}());



/***/ }),

/***/ "./src/app/servicios/servicios.component.css":
/*!***************************************************!*\
  !*** ./src/app/servicios/servicios.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".contenedor{\n\twidth: auto;\n    position: relative;\n}\n\n.iconos ul{\n\tmargin-left: auto;\n\tmargin-right: auto;\n    text-align: center;\n}\n\n.iconos li{\n    display: inline-block;\n    font-size: .8em;\n    text-align: center;\n    margin: 0 13px 0 13px;\n    margin-top: 20px;\n    margin-bottom: 50px;\n    cursor: pointer;\n}\n\n.Title{\n    text-align: center;\n}\n\nspan{\n    display: none;\n}\n\nli .JuntaDir{\n    background-image: url(/assets/img/assets.png);\n    background-position: -368px 0;\n    padding-top: 60px;\n    width: 62px;\n    height: 60px;\n    border:solid 2px #F0C730;\n    border-radius: 31px;\n}\n\nli .ControlSoc{\n    background-image: url(/assets/img/assets.png);\n    background-position: -368px -57px;\n    padding-top: 60px;\n    width: 62px;\n    height: 60px;\n    border:solid 2px #5698AA;\n    border-radius: 31px;\n\n}\n\nli .Apelaciones{\n    background-image: url(/assets/img/assets.png);\n    background-position: -368px -114px;\n    padding-top: 60px;\n    width: 62px;\n    height: 60px;\n    border:solid 2px #CC4229;\n    border-radius: 31px;\n}\n\nli .RevFisc{\n    background-image: url(/assets/img/assets.png);\n    background-position: -368px -169px;\n    padding-top: 60px;\n    width: 62px;\n    height: 60px;\n    border:solid 2px #28487E;\n    border-radius: 31px;\n}\n\nli .JuntaDir:hover, li .JuntaDir.selected {\n    background-color: #F0C730;\n    background-position-x: -482px;\n}\n\nli .ControlSoc:hover, li .ControlSoc.selected{\n    background-color: #5698AA;\n    background-position-x: -482px;\n\n}\n\nli .Apelaciones:hover, li .Apelaciones.selected{\n    background-color: #CC4229;\n    background-position-x: -482px;\n}\n\nli .RevFisc:hover, li .RevFisc.selected{\n    background-color: #28487E;\n    background-position-x: -482px;\n  \n}\n\n.previous {\n    font-size: 0.9em !important;\n    padding: 80px 0;\n}\n\n.previous .Objeto{\n    text-align: center;\n}\n\n.MainContainer{\n    z-index: 1000;\n    padding: 10px 0;\n}\n\n.MainContainer .Objeto {\n     width: 100%;\n     text-align: center;\n}\n\n.Objeto{\n    border-radius: 5px;\n    padding-bottom: 15px;\n}\n\n.Objeto h1{\n    text-align: center;\n    color: white;\n    margin-top: 5px;\n    margin-bottom: 0;\n    font-size: 1em;\n}\n\n.Objeto a{\n    text-align: center;\n    color: white;\n    font-size: 1em;\n    margin-bottom: 5px;\n}\n\n.Objeto img {\n    padding: 5px;\n    border-radius: 5px;\n    \n}\n\n.Credito{\n    background-color: transparent !important;\n    border: solid 2px;\n    padding: 10px;\n    margin-top: -15px;\n    border-radius: 5px;\n}\n\n.Credito h1{\n    font-size: 0.7em;\n    margin-top: 10px;\n    margin-bottom: 0;\n}\n\n.Credito a{\n    font-size: 0.7em;\n}\n\n.Slider .JuntaDir{\n    background-color: #F0C730;\n    border-color: #F0C730;\n}\n\n.Slider .ControlSoc{\n    background-color: #5698AA;\n    border-color: #5698AA;\n}\n\n.Slider .Apelaciones{\n    background-color: #CC4229;\n    border-color: #CC4229;\n}\n\n.Slider .RevFisc{\n    background-color: #28487E;\n    border-color: #28487E;\n}\n\n.contenedor .BackBanner{\n    left: 15px;\n    background-color: rgb(100 100 100);\n    border-radius: 150px;\n}\n\n.contenedor .NextBanner{\n    right: 15px;\n    background-color: rgb(100 100 100);\n    border-radius: 150px;\n}"

/***/ }),

/***/ "./src/app/servicios/servicios.component.html":
/*!****************************************************!*\
  !*** ./src/app/servicios/servicios.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"pageContent\">\n\t<div class=\"L-10 M-10 S-10 row pageBanner\">\n\t\t<div class=\"L-10 M-8 S-12 columns imagenBanner Servicios\" appImagesSizesBanner>\n\t\t\t<img src=\"./assets/img/BanServicios.jpg\">\n\t\t\t<div class=\"PageTitle\"><h2>Servicios</h2><h3>para que logres lo que te propones</h3></div>\n\t\t</div>\t\n\t\t<div class=\"L-2 M-2 S-12 columns bannerBotones\">\n\t\t\t<a class=\"L-12 M-12 S-6 columns OfVirtual\" href=\"https://servicios2.inube.com.co/linix/v6/860006643/login.php?nit=860006643\" target=\"_blank\">\n\t\t\t\t<div class=\"icono\"><img src=\"./assets/img/OfVirtIcon.png\"></div>\n\t\t\t\t<div class=\"texto\"><b>Oficina<b class=\"Resaltar\"> virtual</b></b></div>\n\t\t\t</a>\n\t\t\t<div class=\"L-12 M-12 S-6 columns Vinculate\" [routerLink]=\"['/vinculate']\">\n\t\t\t\t<div class=\"icono\"><img src=\"./assets/img/vinculIcon.png\"></div>\n\t\t\t\t<div class=\"texto\"><b>Vincúlate<b class=\"Resaltar\"> ahora</b></b></div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t<div class=\"L-10 M-10 S-10 row pageContainer\" id=\"ahorros\">\n\t\t<div class=\"ahorrosTitle\">\n\t\t\t<div class=\"Icon ServiciosAhorros\"></div>\n\t\t\t<div class=\"Title ServiciosAhorros\"><h1>Ahorros</h1></div>\n\t\t</div>\n\t\t<ng-container *ngFor=\"let ahorro of ahorros\">\n\t\t\t<div class=\"L-12 M-12 S-12 columns insideContent\">\n\t\t\t\t<img [src]=\"ahorro.image.url\" class=\"L-3 M-4 S-12 columns\">\n\t\t\t\t<div class=\"L-9 M-8 S-12 texto\" [innerHTML]=\"ahorro.description\"></div>\n\t\t\t</div>\n\t\t</ng-container>\n\t</div>\n\t<div class=\"L-10 M-10 S-10 row pageContainer\" id=\"creditos\">\n\t\t<div class=\"creditosTitle\">\n\t\t\t<div class=\"Icon ServiciosCreditos\"></div>\n\t\t\t<div class=\"Title ServiciosCreditos\"><h1>Créditos</h1></div>\n\t\t\t<div class=\"TextTitle\"><a>Escoge el crédito que necesitas para alcanzar tus metas</a></div>\n\t\t</div>\n\t\t<div class=\"L-12 M-12 S-12 row insideContent\">\n\t\t\t<app-creditos-slider *ngIf=\"Lista\" [data]=\"Lista\"></app-creditos-slider>\n\t\t</div>\n\t</div>\n\t<div class=\"L-10 M-10 S-10 row pageContainer\" id=\"serviciosfinancieros\">\n\t\t<div class=\"servFunerTitle\">\n\t\t\t<div class=\"Icon ServiciosServFun\"></div>\n\t\t\t<div class=\"Title ServiciosServFun\"><h1>Servicios funerarios</h1></div>\n\t\t\t<div class=\"TextTitle\"><a>Al vincularte al Fondo de Empleados del Banco de Bogotá, te afiliarás automáticamente al Plan Tradicional de la funeraria Los Olivos de forma totalmente gratuita. Este plan cobija a alguno de los siguientes grupos familiares:</a></div>\n\t\t</div>\n\t\t<ng-container *ngFor=\"let funerario of funerarios\">\n\t\t\t<div class=\"L-12 M-12 S-12 columns insideContent\">\n\t\t\t\t<img class=\"L-3 M-4 S-12 columns\" [src]=\"funerario.image.url\">\n\t\t\t\t<div class=\"L-9 M-8 S-12 texto\" [innerHTML]=\"funerario.description\"></div>\n\t\t\t</div>\n\t\t</ng-container>\n\t\t<div class=\"TextTitle\">*Si deseas conocer más información sobre el Plan Tradicional u otros planes, puedes consultarlo aquí: <a [href]=\"link_cirular\" target=\"_blank\">Circular Renovación Póliza Olivos 2018 – 2019</a></div>\n\t</div>\n</div>\t"

/***/ }),

/***/ "./src/app/servicios/servicios.component.ts":
/*!**************************************************!*\
  !*** ./src/app/servicios/servicios.component.ts ***!
  \**************************************************/
/*! exports provided: ServiciosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServiciosComponent", function() { return ServiciosComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _services_service_manager_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/service-manager.service */ "./src/app/services/service-manager.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ng_animate_scroll__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ng-animate-scroll */ "./node_modules/ng-animate-scroll/fesm5/ng-animate-scroll.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ServiciosComponent = /** @class */ (function () {
    function ServiciosComponent(service, route, animateScrollService) {
        this.service = service;
        this.route = route;
        this.animateScrollService = animateScrollService;
        this.ahorros = [];
        this.funerarios = [];
        this.clasesCred = [
            { id: 1, key: "LibDes" },
            { id: 2, key: "LibInv" },
            { id: 3, key: "Viv" },
            { id: 4, key: "Estu" },
            { id: 5, key: "Vinc" },
            { id: 6, key: "Antic" },
            { id: 7, key: "Promo" }
        ];
    }
    ServiciosComponent.prototype.ngAfterViewInit = function () {
        this.route.fragment.subscribe(function (data) {
            if (data) {
                try {
                    setTimeout(function () {
                        document.querySelector('#' + data).scrollIntoView();
                        //this.animateScrollService.scrollToElement(data, 200)	
                    }, 500);
                }
                catch (e) { }
            }
        });
    };
    ServiciosComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.link_cirular = _environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].URL_SERVER + '/formatos/circular_renovacion_poliza_olivos_2018_2019.pdf';
        this.service.getServicios()
            .subscribe(function (services) {
            _this.ahorros = services.ahorro.map(function (item) {
                item.image.url = _environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].URL_SERVER + item.image.url;
                item.image.thumb.url = _environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].URL_SERVER + item.image.thumb.url;
                return item;
            });
            _this.funerarios = services.funerarios.map(function (item) {
                item.image.url = _environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].URL_SERVER + item.image.url;
                item.image.thumb.url = _environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].URL_SERVER + item.image.thumb.url;
                return item;
            });
            var index = 0, i = 0;
            for (var _i = 0, _a = services.tipo_creditos; _i < _a.length; _i++) {
                var tipo = _a[_i];
                console.log(index, i);
                services.tipo_creditos[i].key = 'item_' + (index + 1);
                services.tipo_creditos[i].image.url = _environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].URL_SERVER + tipo.image.url;
                i++;
                if (index < 6) {
                    index++;
                }
                else {
                    index = 0;
                }
            }
            _this.clasesCred = services.tipo_creditos;
            var index_service = 0;
            _this.Lista = services.creditos.map(function (item, i) {
                if (_this.clasesCred.filter(function (i) { return i.id == item.type_credit_id; })[0]) {
                    var cred = {};
                    cred["id"] = (index_service + 1);
                    cred["Tipo"] = item.type_credit_id;
                    cred["Description"] = item.description;
                    cred["Titulo"] = item.sub_title;
                    cred["Objeto"] = item.name;
                    cred["TextIcon"] = item.name;
                    cred["Clase"] = _this.clasesCred.filter(function (i) { return i.id == item.type_credit_id; })[0].key;
                    cred["Icon"] = _this.clasesCred.filter(function (i) { return i.id == item.type_credit_id; })[0].image.url;
                    cred["Picture"] = _environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].URL_SERVER + item.image.url;
                    index_service++;
                    return cred;
                }
            }).filter(function (x) { return x != undefined; });
        });
    };
    ServiciosComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-servicios',
            template: __webpack_require__(/*! ./servicios.component.html */ "./src/app/servicios/servicios.component.html"),
            styles: [__webpack_require__(/*! ./servicios.component.css */ "./src/app/servicios/servicios.component.css")]
        }),
        __metadata("design:paramtypes", [_services_service_manager_service__WEBPACK_IMPORTED_MODULE_2__["ServiceManagerService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], ng_animate_scroll__WEBPACK_IMPORTED_MODULE_4__["NgAnimateScrollService"]])
    ], ServiciosComponent);
    return ServiciosComponent;
}());



/***/ }),

/***/ "./src/app/shared/convenios-page/convenios-page.component.css":
/*!********************************************************************!*\
  !*** ./src/app/shared/convenios-page/convenios-page.component.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/*.pageContainer{\n    background-color: rgb(240, 240, 240);\n}*/\n\n.contenedor{\n    width: auto;\n    position: relative;\n}\n\n.iconos ul{\n    margin-left: auto;\n    margin-right: auto;\n    text-align: center;\n}\n\n.iconos li{\n    display: inline-block;\n    font-size: .8em;\n    text-align: center;\n    margin: 0 13px 0 13px;\n    margin-top: 20px;\n    cursor: pointer;\n    vertical-align: top;\n    text-transform: capitalize;\n}\n\n.Title{\n    text-align: center;\n}\n\nspan{\n    text-align: center;\n    width: 65px;    \n}\n\nli .icon{\n    width: 60px;\n    height: 60px;\n    border-radius: 50%;\n    border:solid 2px #F0C730;\n    position: relative;\n    margin: 0 auto;\n\n}\n\nli .icon img{\n    width: 85%;\n    position: absolute;\n    right: 50%;\n    bottom: 50%;\n    -webkit-transform: translate(50%, 50%);\n            transform: translate(50%, 50%);\n}\n\nli .icon:hover img, li .icon.selected img{\n    -webkit-filter: brightness(100);\n            filter: brightness(100);\n}\n\nli.conv_1 .icon{\n    border:solid 2px #F0C730;\n    color: #F0C730;\n}\n\nli.conv_2 .icon{\n    border:solid 2px #5698AA;\n    color: #5698AA;\n}\n\nli.conv_3 .icon{\n    border:solid 2px #CC4229;\n    color: #CC4229;\n}\n\nli.conv_4 .icon{\n    border:solid 2px #28487E;\n    color: #28487E;\n}\n\nli.conv_5 .icon{\n    border:solid 2px #BFCA43;\n    color: #BFCA43;\n}\n\nli.conv_6 .icon{\n    border:solid 2px #E0992F;\n    color: #E0992F;\n}\n\nli.conv_1 .text-icon{\n    color: #F0C730;\n}\n\nli.conv_0 .text-icon{\n    color: #F0C730;\n}\n\nli.conv_2 .text-icon{\n    color: #5698AA;\n}\n\nli.conv_3 .text-icon{\n    color: #CC4229;\n}\n\nli.conv_4 .text-icon{\n    color: #28487E;\n}\n\nli.conv_5 .text-icon{\n    color: #BFCA43;\n}\n\nli.conv_6 .text-icon{\n    color: #E0992F;\n}\n\nli.conv_0 .icon:hover, li.conv_0 .icon.selected {\n    background-color: #F0C730;\n}\n\nli.conv_1 .icon:hover, li.conv_1 .icon.selected {\n    background-color: #F0C730;\n}\n\nli.conv_2 .icon:hover, li.conv_2 .icon.selected{\n    background-color: #5698AA;\n}\n\nli.conv_3 .icon:hover, li.conv_3 .icon.selected{\n    background-color: #CC4229;\n}\n\nli.conv_4 .icon:hover, li.conv_4 .icon.selected{\n    background-color: #28487E;\n}\n\nli.conv_5 .icon:hover, li.conv_5 .icon.selected{\n    background-color: #BFCA43;\n}\n\nli.conv_6 .icon:hover, li.conv_6 .icon.selected{\n    background-color: #E0992F;\n}\n\nli.FilteredConv {\n    cursor: pointer;\n    position: relative;\n    padding: 10px;\n}\n\nli.FilteredConv:hover .More {\n    display: block;\n}\n\n.FilteredConv img{\n    border: solid 1px grey;\n    border-radius: 15px;\n}\n\n.More {\n    display: none;\n    position: absolute;\n    top: 0;\n    left: 0;\n    padding: 10px;\n    width: 100%;\n    height: 100%;\n}\n\n.MoreContainer {\n    background-color: rgba(40,72,126,.9);\n    color: white !important;\n    border: solid 1px grey;\n    border-radius: 15px;\n    height: 99%;\n    padding: 10px;\n    }\n\n.Titulo h1{\n    color: white;\n}\n\n.descript{\n    border-top: solid 2px grey;\n    margin-top: -20px;\n    padding-top: 5px;\n    font-size: 0.8rem;\n}\n\n.descuento{\n    position: relative;\n    font-size: 2em;\n    text-align: center;\n    -webkit-transform: translateY(30%);\n            transform: translateY(30%)\n}\n\n.descuento h1{\n    color:white;\n    margin-top: 0;\n}"

/***/ }),

/***/ "./src/app/shared/convenios-page/convenios-page.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/shared/convenios-page/convenios-page.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"L-12 M-12 S-12 row contenedor\" *ngIf=\"Conv\">\n\t<div class=\"padding-title\">\n\t\t<div class=\"Icon Convenios\"></div>\n\t\t<div class=\"Title Convenios\"><h1 >Convenios y Alianzas</h1></div>\n\t</div>\n\t<div class=\"iconos\">\n\t\t<ul class=\"L-10 M-10 S-10 columns centered\">\n\t\t\t\n\t\t\t<li class=\"Title  {{item.class}}\" *ngFor=\"let item of Categorias\">\n\t\t\t\t<div [ngClass]=\"{selected: Position == item.id}\" (click)=\"ChangePosition(item.id)\" class=\"icon\">\n\t\t\t\t\t<img src=\"{{item.image}}\" alt=\"\">\n\t\t\t\t</div>\n\t\t\t\t<div class=\"text-icon\">{{item.name}}</div>\n\t\t\t</li>\n\t\t</ul>\n\t</div>\n\t<ul class=\"FilteredConvContainer\" >\n\t\t<li class=\"L-3 M-4 S-6 columns FilteredConv\" *ngFor=\"let conv of FilteredConvenios\" [routerLink]=\"['/convenios', conv.id]\"><img src=\"{{conv.picture}}\" alt=\"\">\n\t\t\t<div class=\"More\">\n\t\t\t\t<div class=\"MoreContainer\">\n\t\t\t\t\t<div class=\"Titulo\"><h1>{{conv.Title}}</h1></div>\n\t\t\t\t\t<div class=\"descript\" [innerHTML]=\"conv.Short_Desc | safe:'html'\"></div>\n\t\t\t\t\t<div class=\"descuento\" *ngIf=\"conv.Descuento\"><h1>{{conv.Descuento}}</h1></div>\n\t\t\t\t\t<div class=\"verMas\"></div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</li>\n\t</ul>\n</div>\n\n\n\n\n\n\n\n"

/***/ }),

/***/ "./src/app/shared/convenios-page/convenios-page.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/shared/convenios-page/convenios-page.component.ts ***!
  \*******************************************************************/
/*! exports provided: ConveniosPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConveniosPageComponent", function() { return ConveniosPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_service_manager_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/service-manager.service */ "./src/app/services/service-manager.service.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ConveniosPageComponent = /** @class */ (function () {
    function ConveniosPageComponent(service, route) {
        this.service = service;
        this.route = route;
        this.Categorias = [];
        this.Conv = [];
        this.Position = 0;
        this.max_colors = 6;
    }
    ConveniosPageComponent.prototype.ChangePosition = function (ev) {
        this.Position = ev;
        if (ev == "") {
            this.FilteredConvenios = this.Conv;
            return;
        }
        this.FilteredConvenios = this.Conv.filter(function (c) { return c.cat == ev; });
    };
    ConveniosPageComponent.prototype.ngAfterViewInit = function () {
        setTimeout(function () {
        }, 100);
    };
    ConveniosPageComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            window.scrollTo(0, 0);
        });
        this.service.getConvenios()
            .subscribe(function (data) {
            var index = 1;
            _this.Categorias = data.tipo_convenio.map(function (cat, i) {
                cat.class = 'conv_' + (index + 1);
                cat.image = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].URL_SERVER + '/' + cat.image.url;
                index = (index < (_this.max_colors - 1)) ? index + 1 : 0;
                return cat;
            });
            _this.Categorias.unshift({
                id: 0,
                image: '/assets/img/todos.svg',
                class: 'conv_0',
                name: 'Todos'
            });
            console.log(_this.Categorias);
            _this.Conv = data.convenios;
            _this.Conv = _this.Conv.map(function (item) {
                var short_desc = item.description.replace(/<\/?[^>]+(>|$)/g, "") < 100 ?
                    item.description.replace(/<\/?[^>]+(>|$)/g, "") :
                    item.description.replace(/<\/?[^>]+(>|$)/g, "").substring(0, 100) + '...';
                return {
                    "id": item.id,
                    "Title": item.title,
                    "Short_Desc": short_desc,
                    "Descuento": (item.discount) ? item.discount + '%' : null,
                    "picture": _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].URL_SERVER + item.image.url,
                    "cat": item.type_agreement_id
                };
            });
            _this.FilteredConvenios = _this.Conv;
        });
    };
    ConveniosPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-convenios-page',
            template: __webpack_require__(/*! ./convenios-page.component.html */ "./src/app/shared/convenios-page/convenios-page.component.html"),
            styles: [__webpack_require__(/*! ./convenios-page.component.css */ "./src/app/shared/convenios-page/convenios-page.component.css")],
        }),
        __metadata("design:paramtypes", [_services_service_manager_service__WEBPACK_IMPORTED_MODULE_1__["ServiceManagerService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])
    ], ConveniosPageComponent);
    return ConveniosPageComponent;
}());



/***/ }),

/***/ "./src/app/shared/creditos-slider/creditos-slider.component.css":
/*!**********************************************************************!*\
  !*** ./src/app/shared/creditos-slider/creditos-slider.component.css ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".Slider{\n    width: 85%;\n    margin: 0 auto;\n    padding: 0 2rem;\n}\n\n.Slider .slide{\n    padding: 4rem 0;\n    font-size: 0.75rem;\n    transition: all 0.28s ease-in-out;\n}\n\n.Slider .slide.slick-current{\n    padding: 0;\n    font-size: 0.8rem;\n}\n\n.Slider .slide.slick-current .Credito > div{ \n    max-height: inherit;\n    min-height: 500px;\n}\n\n.contenedor{\n\twidth: auto;\n    position: relative;\n    padding-bottom: 1rem;\n    min-height: 900px;\n}\n\n.iconos ul{\n\tmargin-left: auto;\n\tmargin-right: auto;\n    text-align: center;\n}\n\n.iconos li{\n    display: inline-block;\n    font-size: .8em;\n    text-align: center;\n    margin: 0 25px 0 25px;\n    margin-top: 20px;\n    margin-bottom: 50px;\n    cursor: pointer;\n}\n\n.Title{\n    text-align: center;\n    \n}\n\n.Title > div{\n    position: relative;\n}\n\n.Title > div img{\n    position: absolute;\n    right: 50%;\n    bottom: 50%;\n    -webkit-transform: translate(50%, 50%);\n            transform: translate(50%, 50%);\n    width: 80%;\n}\n\n.Title .label-item{\n    position: absolute;\n    right: 50%;\n    -webkit-transform: translateX(50%);\n            transform: translateX(50%);\n    padding: 0.5rem 0;\n    font-weight: 400;\n    width: 120px;\n}\n\n.Title span{\n    display: none;\n}\n\nli .icon-item:hover img, li .icon-item.selected img{\n    -webkit-filter: brightness(100);\n            filter: brightness(100);\n}\n\nli .icon-item.item_1{\n    padding-top: 60px;\n    width: 62px;\n    height: 60px;\n    border:solid 2px #F0C730;\n    border-radius: 31px;\n}\n\nli .icon-item.item_2{\n    padding-top: 60px;\n    width: 62px;\n    height: 60px;\n    border:solid 2px #5698AA;\n    border-radius: 31px;\n\n}\n\nli .icon-item.item_3{\n    padding-top: 60px;\n    width: 62px;\n    height: 60px;\n    border:solid 2px #CC4229;\n    border-radius: 31px;\n}\n\nli .icon-item.item_4{\n    padding-top: 60px;\n    width: 62px;\n    height: 60px;\n    border:solid 2px #28487E;\n    border-radius: 31px;\n}\n\nli .icon-item.item_5{\n    padding-top: 60px;\n    width: 62px;\n    height: 60px;\n    border:solid 2px #BFCA43;\n    border-radius: 31px;\n}\n\nli .icon-item.item_6{\n    padding-top: 60px;\n    width: 62px;\n    height: 60px;\n    border:solid 2px #E0992F;\n    border-radius: 31px;\n}\n\nli .icon-item.item_7{\n    padding-top: 60px;\n    width: 62px;\n    height: 60px;\n    border:solid 2px #6E2562;\n    border-radius: 31px;\n}\n\nli .icon-item.JuntaDir{\n    border-color: #F0C730;\n    padding-top: 60px;\n    width: 62px;\n    height: 60px;\n    border:solid 2px #F0C730;\n    border-radius: 31px;\n}\n\nli .icon-item.ControlSoc{\n    padding-top: 60px;\n    width: 62px;\n    height: 60px;\n    border:solid 2px #5698AA;\n    border-radius: 31px;\n}\n\nli .icon-item.Apelaciones{\n    padding-top: 60px;\n    width: 62px;\n    height: 60px;\n    border:solid 2px #CC4229;\n    border-radius: 31px;\n\n}\n\nli .icon-item.RevFisc{\n    padding-top: 60px;\n    width: 62px;\n    height: 60px;\n    border:solid 2px #28487E;\n    border-radius: 31px;\n}\n\nli .icon-item.item_11{\n    padding-top: 60px;\n    width: 62px;\n    height: 60px;\n    border:solid 2px #28487E;\n    border-radius: 31px;\n}\n\nli .icon-item.JuntaDir:hover, li .icon-item.JuntaDir.selected {\n    background-color: #F0C730;\n    background-position-x: -482px;\n}\n\nli .icon-item.ControlSoc:hover, li .icon-item.ControlSoc.selected{\n    background-color: #5698AA;\n    background-position-x: -482px;\n\n}\n\nli .icon-item.Apelaciones:hover, li .icon-item.Apelaciones.selected{\n    background-color: #CC4229;\n    background-position-x: -482px;\n}\n\nli .icon-item.RevFisc:hover, li .icon-item.RevFisc.selected{\n    background-color: #28487E;\n    background-position-x: -482px;\n  \n}\n\n.Title .label-item.JuntaDir{\n    color: #F0C730;\n\n}\n\n.Title .label-item.ControlSoc{\n    color: #5698AA;\n\n}\n\n.Title .label-item.Apelaciones{\n    color: #CC4229;\n\n}\n\n.Title .label-item.RevFisc{\n    color: #28487E;\n\n}\n\n.Title .label-item.item_1{\n    color: #F0C730;\n\n}\n\n.Title .label-item.item_2{\n    color: #5698AA;\n\n}\n\n.Title .label-item.item_3{\n    color: #CC4229;\n\n}\n\n.Title .label-item.item_4{\n    color: #28487E;\n\n}\n\n.Title .label-item.item_5{\n    color: #BFCA43;\n\n}\n\n.Title .label-item.item_6{\n    color: #E0992F;\n\n}\n\n.Title .label-item.item_7{\n    color: #6E2562;\n\n}\n\nli .icon-item.item_1:hover, li .icon-item.item_1.selected {\n    background-color: #F0C730;\n}\n\nli .icon-item.item_2:hover, li .icon-item.item_2.selected{\n    background-color: #5698AA;\n}\n\nli .icon-item.item_3:hover, li .icon-item.item_3.selected{\n    background-color: #CC4229;\n    \n}\n\nli .icon-item.item_4:hover, li .icon-item.item_4.selected{\n    background-color: #28487E;\n}\n\nli .icon-item.item_5:hover, li .icon-item.item_5.selected{\n    background-color: #BFCA43;\n}\n\nli .icon-item.item_6:hover, li .icon-item.item_6.selected{\n    background-color: #E0992F;\n}\n\nli .icon-item.item_7:hover, li .icon-item.item_7.selected{\n    background-color: #6E2562;\n}\n\n.previous {\n    font-size: 0.9em !important;\n    padding: 80px 0;\n}\n\n.previous .Objeto{\n    text-align: center;\n}\n\n.MainContainer{\n    z-index: 1000;\n    padding: 10px 0;\n}\n\n.MainContainer .Objeto {\n     width: 100%;\n     text-align: center;\n}\n\n.MainContainer .Credito{\n    height:560px;\n    margin-bottom: 30px;\n}\n\n.Objeto{\n    border-radius: 5px;\n    padding-bottom: 15px;\n}\n\n.Objeto h1{\n    text-align: center;\n    color: white;\n    margin-top: 5px;\n    margin-bottom: 0;\n    font-size: 1rem;\n}\n\n.Objeto a{\n    display: block;\n    text-align: center;\n    color: white;\n    font-size: 1rem;\n    margin-bottom: 5px;\n}\n\n.Objeto img {\n    padding: 5px;\n    border-radius: 5px;\n    \n}\n\n.Credito{\n    background-color: transparent !important;\n    border: solid 2px;\n    padding: 10px;\n    margin-top: -15px;\n    border-radius: 5px;\n \n}\n\n.Credito > div{\n    max-height: 290px;\n    min-height: 250px;\n    overflow: hidden;\n    padding-bottom: 1rem;\n}\n\n.previous .Credito > div{\n    height: 400px;\n    overflow: hidden;\n}\n\n.Credito h1, .Credito strong{\n    margin-top: 10px;\n    margin-bottom: 0;\n    font-weight: bold;\n}\n\n/*.Credito a, .Credito span{\n    font-size: 0.7rem;\n    display: block;\n    line-height: 1;\n}*/\n\n.Slider .item_1{\n    background-color: #F0C730;\n    border-color: #F0C730;\n}\n\n.Slider .item_2{\n    background-color: #5698AA;\n    border-color: #5698AA;\n}\n\n.Slider .item_3{\n    background-color: #CC4229;\n    border-color: #CC4229;\n}\n\n.Slider .item_4{\n    background-color: #28487E;\n    border-color: #28487E;\n}\n\n.Slider .item_5{\n    background-color: #BFCA43;\n    border-color: #BFCA43;\n}\n\n.Slider .item_6{\n    background-color: #E0992F;\n    border-color: #E0992F;\n}\n\n.Slider .item_7{\n    background-color: #6E2562;\n    border-color: #6E2562;\n}\n\n.Slider .JuntaDir{\n    background-color: #F0C730;\n    border-color: #F0C730;\n}\n\n.Slider .ControlSoc{\n    background-color: #5698AA;\n    border-color: #5698AA;\n}\n\n.Slider .Apelaciones{\n    background-color: #CC4229;\n    border-color: #CC4229;\n}\n\n.Slider .RevFisc{\n    background-color: #28487E;\n    border-color: #28487E;\n}\n\n.contenedor .BackBanner{\n    left: 15px;\n    background-color: rgb(100 100 100);\n    border-radius: 150px;\n}\n\n.contenedor .NextBanner{\n    right: 15px;\n    background-color: rgb(100 100 100);\n    border-radius: 150px;\n}\n\n@media screen and (max-width: 39.9375em) {\n    .Slider{\n        width: 100%;\n    }\n\n    .iconos li{\n        margin: 0 !important;\n        height: 100px;\n        width: 50%;\n        text-align: center;\n    }\n\n    .iconos li .icon-item {\n        margin: 0 auto;\n    }\n\n    .Slider .slide{\n        padding: 0;\n    }\n\n    .Slider .slide.slick-current .Credito > div{\n        min-height: auto;\n    }\n\n    \n}"

/***/ }),

/***/ "./src/app/shared/creditos-slider/creditos-slider.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/shared/creditos-slider/creditos-slider.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"L-12 M-12 S-12 row contenedor\" *ngIf=\"data\">\n\t<div class=\"iconos\">\n\t\t<ul class=\"L-10 M-10 S-10 columns centered\">\n\t\t\t<li class=\"Title\" *ngFor=\"let item of data\">\n\t\t\t\t<div [ngClass]=\"{selected: Position == item.id-1}\" (click)=\"ChangePosition(item.id-1)\" class=\"icon-item {{item.Clase}}\">\n\t\t\t\t\t<img src=\"{{item.Icon}}\" alt=\"\">\n\t\t\t\t\t<div class=\"label-item {{item.Clase}}\">{{ item.TextIcon }}</div>\n\t\t\t\t</div>\n\t\t\t\t\n\t\t\t</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\t<ngx-slick class=\"carousel Slider\" #slickModal=\"slick-modal\" [config]=\"slideConfig\" (afterChange)=\"afterChange($event)\">\n\t\t\t<div ngxSlickItem class=\"slide\"  *ngFor=\"let cred of data\" >\n\t\t\t\t<div class=\" Objeto {{cred.Clase}}\">\n\t\t\t\t\t<img src=\"{{cred.Picture}}\" alt=\"\">\n\t\t\t\t\t<h1>{{cred.Titulo}}</h1>\n\t\t\t\t\t<a>{{cred.Objeto}}</a>\n\t\t\t\t</div>\n\t\t\t\t<div class=\" Credito {{cred.Clase}}\" >\n\t\t\t\t\t<div [innerHTML]=\"cred.Description | safe:'html'\"></div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</ngx-slick>\n\t</div>\n\t<div class=\"BackBanner\"></div>\n\t<div class=\"NextBanner\"></div>\n</div>\t\n\t\t\n"

/***/ }),

/***/ "./src/app/shared/creditos-slider/creditos-slider.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/shared/creditos-slider/creditos-slider.component.ts ***!
  \*********************************************************************/
/*! exports provided: CreditosSliderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreditosSliderComponent", function() { return CreditosSliderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_device_detector__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-device-detector */ "./node_modules/ngx-device-detector/ngx-device-detector.umd.js");
/* harmony import */ var ngx_device_detector__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(ngx_device_detector__WEBPACK_IMPORTED_MODULE_1__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var CreditosSliderComponent = /** @class */ (function () {
    function CreditosSliderComponent(deviceService) {
        this.deviceService = deviceService;
        this.BackPosition = 6;
        this.Position = 0;
        this.FrontPosition = this.Position + 1;
    }
    CreditosSliderComponent.prototype.ngOnInit = function () {
        console.log(this.data);
        this.slideConfig = this.deviceService.isMobile() ?
            {
                "slidesToShow": 1,
                "slidesToScroll": 1,
                "centerMode": true,
                "centerPadding": '0px',
                "prevArrow": ".BackBanner",
                "nextArrow": ".NextBanner"
            }
            :
                {
                    "slidesToShow": 3,
                    "slidesToScroll": 1,
                    "centerMode": true,
                    "centerPadding": '2px',
                    "prevArrow": ".BackBanner",
                    "nextArrow": ".NextBanner"
                };
    };
    CreditosSliderComponent.prototype.afterChange = function (event) {
        this.Position = event.currentSlide;
    };
    CreditosSliderComponent.prototype.ChangePosition = function (index) {
        console.log(index);
        //this.Position = index;
        this.slickModal.slickGoTo(index);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('slickModal'),
        __metadata("design:type", Object)
    ], CreditosSliderComponent.prototype, "slickModal", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], CreditosSliderComponent.prototype, "data", void 0);
    CreditosSliderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-creditos-slider',
            template: __webpack_require__(/*! ./creditos-slider.component.html */ "./src/app/shared/creditos-slider/creditos-slider.component.html"),
            styles: [__webpack_require__(/*! ./creditos-slider.component.css */ "./src/app/shared/creditos-slider/creditos-slider.component.css")],
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewEncapsulation"].None
        }),
        __metadata("design:paramtypes", [ngx_device_detector__WEBPACK_IMPORTED_MODULE_1__["DeviceDetectorService"]])
    ], CreditosSliderComponent);
    return CreditosSliderComponent;
}());



/***/ }),

/***/ "./src/app/shared/directives/images-sizes-banner.directive.ts":
/*!********************************************************************!*\
  !*** ./src/app/shared/directives/images-sizes-banner.directive.ts ***!
  \********************************************************************/
/*! exports provided: ImagesSizesBannerDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ImagesSizesBannerDirective", function() { return ImagesSizesBannerDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ImagesSizesBannerDirective = /** @class */ (function () {
    function ImagesSizesBannerDirective(el) {
        this.el = el;
        this.position = 0;
    }
    ImagesSizesBannerDirective.prototype.checksize = function () {
        var _this = this;
        this.el.nativeElement.children[0].onload = function () {
            _this.divW = _this.el.nativeElement.clientWidth;
            _this.divH = _this.el.nativeElement.clientHeight;
            _this.divAspect = _this.divW / _this.divH;
            var h = _this.el.nativeElement.children[0].naturalHeight;
            var w = _this.el.nativeElement.children[0].naturalWidth;
            var aspectRatio = w / h;
            if (_this.divAspect >= aspectRatio) {
                _this.el.nativeElement.children[0].style.height = (_this.divW / aspectRatio) + "px";
                _this.el.nativeElement.children[0].style.width = _this.divW + "px";
                var mt = (_this.divH / 2) - ((_this.divW / aspectRatio) / 2);
                _this.el.nativeElement.children[0].style.marginTop = (mt) + "px";
                // this.el.nativeElement.children[0].style.marginLeft = "0px"
            }
            else {
                _this.el.nativeElement.children[0].style.height = _this.divH + "px";
                _this.el.nativeElement.children[0].style.width = (_this.divH * aspectRatio) + "px";
                var ml = (_this.divW / 2) - ((_this.divH * aspectRatio) / 2);
                // this.el.nativeElement.children[0].style.marginLeft = ml + "px"
                _this.el.nativeElement.children[0].style.marginTop = "0px";
            }
        };
    };
    ImagesSizesBannerDirective.prototype.ngAfterContentChecked = function () {
        this.checksize();
    };
    ImagesSizesBannerDirective.prototype.ngOnInit = function () {
        //this.el.nativeElement.style.height = this.el.nativeElement.clientWidth + "px";
        this.checksize();
    };
    ImagesSizesBannerDirective = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
            selector: '[appImagesSizesBanner]'
        }),
        __metadata("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]])
    ], ImagesSizesBannerDirective);
    return ImagesSizesBannerDirective;
}());



/***/ }),

/***/ "./src/app/shared/directives/images-sizes-square.directive.ts":
/*!********************************************************************!*\
  !*** ./src/app/shared/directives/images-sizes-square.directive.ts ***!
  \********************************************************************/
/*! exports provided: ImagesSizesSquareDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ImagesSizesSquareDirective", function() { return ImagesSizesSquareDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ImagesSizesSquareDirective = /** @class */ (function () {
    function ImagesSizesSquareDirective(el) {
        this.el = el;
        this.position = 0;
    }
    ImagesSizesSquareDirective.prototype.checksizeLoc = function () {
        var _this = this;
        this.el.nativeElement.children[0].onload = function () {
            _this.divW = _this.el.nativeElement.clientWidth;
            _this.divH = _this.divW;
            _this.divAspect = _this.divW / _this.divH;
            var h = _this.el.nativeElement.children[0].naturalHeight;
            var w = _this.el.nativeElement.children[0].naturalWidth;
            var aspectRatio = w / h;
            if (aspectRatio >= _this.divAspect) {
                _this.el.nativeElement.children[0].style.height = _this.divH + "px";
                _this.el.nativeElement.children[0].style.width = (_this.divH * aspectRatio) + "px";
                var ml = ((_this.divW / 2) - (_this.divH * aspectRatio) / 2);
                _this.el.nativeElement.children[0].style.marginLeft = ml + "px";
                _this.el.nativeElement.children[0].style.marginTop = "0px";
            }
            else {
                _this.el.nativeElement.children[0].style.width = _this.divW + "px";
                console.log(_this.el.nativeElement.children[0].style.width);
                _this.el.nativeElement.children[0].style.height = (_this.divW) + "px";
                console.log(_this.el.nativeElement.children[0].style.height);
                var mt = (_this.divH / 2) - ((_this.divW / 2));
                _this.el.nativeElement.children[0].style.marginTop = mt + "px";
                _this.el.nativeElement.children[0].style.marginLeft = "0px";
            }
        };
    };
    ImagesSizesSquareDirective.prototype.ngOnInit = function () {
        this.checksizeLoc();
    };
    ImagesSizesSquareDirective = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
            selector: '[appImagesSizesSquare]'
        }),
        __metadata("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]])
    ], ImagesSizesSquareDirective);
    return ImagesSizesSquareDirective;
}());



/***/ }),

/***/ "./src/app/shared/directives/square-div.directive.ts":
/*!***********************************************************!*\
  !*** ./src/app/shared/directives/square-div.directive.ts ***!
  \***********************************************************/
/*! exports provided: SquareDivDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SquareDivDirective", function() { return SquareDivDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SquareDivDirective = /** @class */ (function () {
    function SquareDivDirective(el) {
        this.el = el;
    }
    SquareDivDirective.prototype.CheckSize = function () {
        this.divW = this.el.nativeElement.clientWidth;
        this.el.nativeElement.style.height = this.divW + "px";
        console.log(this.divW);
    };
    SquareDivDirective.prototype.ngOnInit = function () {
        this.CheckSize();
    };
    SquareDivDirective = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
            selector: '[appSquareDiv]'
        }),
        __metadata("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]])
    ], SquareDivDirective);
    return SquareDivDirective;
}());



/***/ }),

/***/ "./src/app/shared/familiar.ts":
/*!************************************!*\
  !*** ./src/app/shared/familiar.ts ***!
  \************************************/
/*! exports provided: Familiar */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Familiar", function() { return Familiar; });
var Familiar = /** @class */ (function () {
    function Familiar() {
        this.type_occupation_id_familiar = "";
        this.type_family_id = "";
    }
    return Familiar;
}());



/***/ }),

/***/ "./src/app/shared/header/header.component.css":
/*!****************************************************!*\
  !*** ./src/app/shared/header/header.component.css ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".header{\n\tdisplay: inline-block;\n\t/*padding: 0 25px 0 25px;*/\n\tposition: relative;\n}\n\n.logo{\n\tdisplay: inline-block;\n\tbackground-image: url(\"/assets/img/logo.png\");\n\tbackground-repeat: none !important;\n\theight: 133px;\n\twidth: 350px;\n\t\n}\n\n.logo-header{\n\tmargin: 3rem 0;\n\n}\n\nul {\n\tmargin-top: 50px;\n\tdisplay: inline-block;\n\tfloat: right;\n\tposition: relative;\n\tz-index: 99;\n}\n\nli{\n\tdisplay: inline-block;\n\tmargin: 0 5px 0 5px;\n\tbackground-color: red;\n\theight: 40px;\n\twidth: 40px;\n\tborder-radius: 20px;\n\tposition: relative;\n\tcursor: pointer;\n\toutline: none;\n\ttransition: all 0.28s ease-out;\n}\n\nli:hover a{\n\tdisplay: block;\n}\n\nli:hover > .submenu{\n\tdisplay: block;\n\tpadding-top: 2rem;\n    margin-top: -2rem;\n}\n\na {\n\tdisplay: none;\n\tposition: absolute;\n    top: 13px;\n    left: 40px;\n\tcolor: white;\n\n}\n\nli span{\n\tdisplay: inline-block;\n\twidth: 40px;\n\theight: 40px;\n\tbackground-image: url(\"/assets/img/assets.svg\");\n\tbackground-size: 1024px;\n}\n\nli.inicio{\n\tbackground-color: #F0C730 ;\n}\n\nli.inicio:hover {\n\tdisplay: inline-block;\n\twidth: 90px;\n}\n\nli.servicios{\n\tbackground-color: #5698AA ;\n}\n\nli.servicios span{\n\tbackground-position: 0 -40px;\n}\n\nli.servicios:hover {\n\tdisplay: inline-block;\n\twidth: 115px;\n}\n\nli.beneficios{\n\tbackground-color: #E0992F ;\n}\n\nli.beneficios span{\n\tbackground-position: 0 -80px;\n}\n\nli.beneficios:hover {\n\tdisplay: inline-block;\n\twidth: 125px;\n}\n\nli.convenios{\n\tbackground-color: #28487E ;\n}\n\nli.convenios span{\n\tbackground-position: 0 -120px;\n}\n\nli.convenios:hover {\n\tdisplay: inline-block;\n\twidth: 200px;\n}\n\nli.conocenos{\n\tbackground-color: #BFCA43 ;\n}\n\nli.conocenos span{\n\tbackground-position: 0 -160px;\n}\n\nli.conocenos:hover {\n\tdisplay: inline-block;\n\twidth: 130px;\n}\n\nli.vinculate{\n\tbackground-color: #CC4229 ;\n}\n\nli.vinculate span{\n\tbackground-position: 0 -200px;\n}\n\nli.vinculate:hover {\n\tdisplay: inline-block;\n\twidth: 120px;\n}\n\nli.seguridad{\n\tbackground-color: #AC7F41 ;\n}\n\nli.seguridad span{\n\tbackground-position: 0 -240px;\n}\n\nli.seguridad:hover {\n\tdisplay: inline-block;\n\twidth: 125px;\n}\n\nli.contactanos{\n\tbackground-color: #6E2562 ;\n}\n\nli.contactanos span{\n\tbackground-position: 0 -280px;\n}\n\nli.contactanos:hover {\n\tdisplay: inline-block;\n\twidth: 140px;\n}\n\n/*li.contactanos a{\n\tdisplay: none\n}\n\nli.contactanos:hover a{\n\tdisplay: block;\n}\n*/\n\n.submenu{\n\tmargin: 0;\n\twidth: 170px;\n\tposition: absolute;\n\tright: 50%;\n\ttop: 45px;\n\t-webkit-transform: translateX(50%);\n\t        transform: translateX(50%);\n\tdisplay: none;\n}\n\n.submenu li{\n\twidth: 100%;\n\tmargin: 0;\n\tbackground: #389aac;\n\tpadding-top: 11px;\n\tmargin-bottom: 0.2rem;\n\t\n}\n\n.submenu li a{\n\tposition: static;\n\ttext-align: center;\n\ttext-decoration: none;\n}\n\n@media screen and (max-width: 39.9375em) {\n\t.logo{\n\t    display: block;\n\t    background-image: url(/assets/img/logo.png);\n\t    height: 100px;\n\t    width: 270px;\n\t    background-size: contain;\n\t    background-repeat: no-repeat;\n\t    margin: 0 auto;\n\n\t}\n\n\tul{\n\t    width: 100%;\n\t    margin: 0 auto 2rem;\n\t    float: none;\n\t    display: block;\n\t    text-align: center;\n\t    clear: both;\n\t}\n\n\tul li{\n\t\twidth: 140px;\n\t}\n\n\tul li:hover{\n\t\twidth: 140px;\t\n\t}\n\n\tul li span{\n\t\tfloat: left;\n\t}\n\n\ta{\n\t\tdisplay: block;\n\t}\n\n\t.submenu{\n\t\tdisplay: none !important\n\t}\n\n\n}\n\n\n\n"

/***/ }),

/***/ "./src/app/shared/header/header.component.html":
/*!*****************************************************!*\
  !*** ./src/app/shared/header/header.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"L-12 M-12 S-12 row header\">\n\t<div class=\"L-10 M-10 S-10 row centered container\">\n\t\t<div routerLink=\"/\" class=\"columns S-12\">\n\t\t\t<img src=\"assets/img/logo.svg\" class=\"logo-header\">\n\t\t</div>\n\t\t\n\t\t<div class=\"menu\">\n\t\t\t<ul>\n\t\t\t\t<li class=\"inicio\" type=\"submit\" [routerLink]=\"['']\" ><span></span><a>Inicio</a></li>\n\t\t\t\t<li class=\"servicios\" type=\"submit\" [routerLink]=\"['/servicios']\" >\n\t\t\t\t\t<span></span><a>Servicios</a>\n\t\t\t\t\t<ul class=\"submenu\">\n\t\t\t\t\t\t<li><a [routerLink]=\"['/servicios']\" fragment=\"ahorros\">Ahorro</a></li>\n\t\t\t\t\t\t<li><a [routerLink]=\"['/servicios']\" fragment=\"creditos\">Crédito</a></li>\n\t\t\t\t\t\t<li><a [routerLink]=\"['/servicios']\" fragment=\"serviciosfinancieros\">Servicios funerarios</a></li>\n\t\t\t\t\t</ul>\n\t\t\t\t</li>\n\t\t\t\t<li class=\"beneficios\" type=\"submit\" [routerLink]=\"['/beneficios']\"><span></span><a>Beneficios</a></li>\n\t\t\t\t<li class=\"convenios\" type=\"submit\" [routerLink]=\"['/convenios']\"><span></span><a>Convenios y Alianzas</a></li>\n\t\t\t\t<li class=\"conocenos\" type=\"submit\" [routerLink]=\"['/conocenos']\"><span></span><a>Conócenos</a></li>\n\t\t\t\t<li class=\"vinculate\" type=\"submit\" [routerLink]=\"['/vinculate']\"><span></span><a>Vincúlate</a></li>\n\t\t\t\t<li class=\"seguridad\" type=\"submit\" [routerLink]=\"['/seguridad']\"><span></span><a>Seguridad</a></li>\n\t\t\t\t<li class=\"contactanos\" type=\"submit\" [routerLink]=\"['/contactanos']\"><span></span><a>Contáctanos</a></li>\n\t\t\t</ul>\n\t\t</div>\n\t</div>\n</div>"

/***/ }),

/***/ "./src/app/shared/header/header.component.ts":
/*!***************************************************!*\
  !*** ./src/app/shared/header/header.component.ts ***!
  \***************************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HeaderComponent = /** @class */ (function () {
    function HeaderComponent() {
    }
    HeaderComponent.prototype.ngOnInit = function () {
    };
    HeaderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! ./header.component.html */ "./src/app/shared/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.css */ "./src/app/shared/header/header.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/shared/pie/pie.component.css":
/*!**********************************************!*\
  !*** ./src/app/shared/pie/pie.component.css ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/shared/pie/pie.component.html":
/*!***********************************************!*\
  !*** ./src/app/shared/pie/pie.component.html ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"L-12 M-12 S-12 columns footer\">\n\t<div class=\"footer\"><a>Fondo de Empleados, Banco de Bogotá. Todos los derechos reservados 2019. ©</a></div>\n</div>"

/***/ }),

/***/ "./src/app/shared/pie/pie.component.ts":
/*!*********************************************!*\
  !*** ./src/app/shared/pie/pie.component.ts ***!
  \*********************************************/
/*! exports provided: PieComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PieComponent", function() { return PieComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PieComponent = /** @class */ (function () {
    function PieComponent() {
    }
    PieComponent.prototype.ngOnInit = function () {
    };
    PieComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-pie',
            template: __webpack_require__(/*! ./pie.component.html */ "./src/app/shared/pie/pie.component.html"),
            styles: [__webpack_require__(/*! ./pie.component.css */ "./src/app/shared/pie/pie.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], PieComponent);
    return PieComponent;
}());



/***/ }),

/***/ "./src/app/shared/radio-group/radio-group.component.css":
/*!**************************************************************!*\
  !*** ./src/app/shared/radio-group/radio-group.component.css ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".content-checks{\n\tborder: 1px solid #cacaca;\n    padding: 0.5rem;\n    font-size: 0.8rem;\n    text-align: center;\n    min-height: 110px;\n    margin-bottom: 0.5rem;\n}\n\n.content-checks .checkbox{\n    width: 50%;\n    margin-top: 0.8rem;\n}\n\n.content-checks.inline-radio{\n    min-height: auto !important;\n}\n\n.content-checks.inline-radio .checkbox{\n    display: inline-block;\n    width: 100px;\n    vertical-align: middle;\n    margin-top: 0;\n}\n\n.content-checks .other-text{\n\tmargin-top: 0.5rem;\n}\n\n.content-checks .other-text input{\n\tmargin-bottom: 0;\n\theight: 35px;\n}\n\n.content-checks.input-invalid{\n    border-color: red;\n}"

/***/ }),

/***/ "./src/app/shared/radio-group/radio-group.component.html":
/*!***************************************************************!*\
  !*** ./src/app/shared/radio-group/radio-group.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"content-checks\" [ngClass]=\"{'input-invalid': isValid, 'inline-radio': inlineRadio}\">\n\t{{ title }}\n\t<div class=\"row checkbox\">\n\t\t<label class=\"columns L-6 M-6 S-12\" *ngFor=\"let option of listData\">\n\t\t\t{{ option.name }}\n\t\t\t<input \n\t\t\t\t(change)=\"onChange()\"\n\t\t\t\ttype=\"radio\" \n\t\t\t\t[name]=\"name + '_radiogroup'\"\n\t\t\t\t[(ngModel)]=\"value_radio\"\n\t\t\t\t[value]=\"option.value\">\n\t\t</label>\n\t</div>\n</div>"

/***/ }),

/***/ "./src/app/shared/radio-group/radio-group.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/shared/radio-group/radio-group.component.ts ***!
  \*************************************************************/
/*! exports provided: RadioGroupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RadioGroupComponent", function() { return RadioGroupComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var noop = function () { };
var RadioGroupComponent = /** @class */ (function () {
    function RadioGroupComponent() {
        this.group_radios = {};
        this.onChangeCallback = noop;
        this.onTouchedCallback = noop;
    }
    RadioGroupComponent_1 = RadioGroupComponent;
    RadioGroupComponent.prototype.ngOnInit = function () {
    };
    RadioGroupComponent.prototype.ngOnChanges = function (changes) {
        if (changes.isValid) {
            this.isValid = changes.isValid.currentValue;
        }
    };
    Object.defineProperty(RadioGroupComponent.prototype, "value", {
        get: function () {
            return this.result;
        },
        set: function (v) {
            if (v !== this.result) {
                this.result = v;
                this.onChangeCallback(this.result);
            }
        },
        enumerable: true,
        configurable: true
    });
    RadioGroupComponent.prototype.onChange = function (event) {
        this.result = this.value_radio;
        this.onChangeCallback(this.value_radio);
    };
    RadioGroupComponent.prototype.writeValue = function (value) {
        if (value !== this.result) {
            this.result = value;
        }
    };
    RadioGroupComponent.prototype.registerOnChange = function (fn) {
        this.onChangeCallback = fn;
    };
    RadioGroupComponent.prototype.registerOnTouched = function (fn) {
        this.onTouchedCallback = fn;
    };
    RadioGroupComponent.prototype.validate = function (c) {
        return (this.result) ? null : {
            inputError: {
                valid: false,
            },
        };
    };
    var RadioGroupComponent_1;
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], RadioGroupComponent.prototype, "inlineRadio", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], RadioGroupComponent.prototype, "listData", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], RadioGroupComponent.prototype, "title", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], RadioGroupComponent.prototype, "isValid", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], RadioGroupComponent.prototype, "name", void 0);
    RadioGroupComponent = RadioGroupComponent_1 = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'radio-group',
            template: __webpack_require__(/*! ./radio-group.component.html */ "./src/app/shared/radio-group/radio-group.component.html"),
            styles: [__webpack_require__(/*! ./radio-group.component.css */ "./src/app/shared/radio-group/radio-group.component.css")],
            providers: [
                {
                    provide: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NG_VALUE_ACCESSOR"],
                    useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["forwardRef"])(function () { return RadioGroupComponent_1; }),
                    multi: true
                },
                {
                    provide: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NG_VALIDATORS"],
                    useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["forwardRef"])(function () { return RadioGroupComponent_1; }),
                    multi: true,
                }
            ]
        }),
        __metadata("design:paramtypes", [])
    ], RadioGroupComponent);
    return RadioGroupComponent;
}());



/***/ }),

/***/ "./src/app/vinculate/vinculate.component.css":
/*!***************************************************!*\
  !*** ./src/app/vinculate/vinculate.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".insideContent  .creditosTitle{\n    margin-top: 20px;\n}\n\n.Formularios li a{\n    text-decoration: none;\n}\n\n.Formularios li{\n    width: 130px;\n    line-height: 1;\n    margin-bottom: 1rem;\n}\n\n.BgFormulario{\n    margin: 0 auto 0.5rem;\n}\n\n.contenedor{\n\twidth: auto;\n    position: relative;\n}\n\n.iconos ul{\n\tmargin-left: auto;\n\tmargin-right: auto;\n    text-align: center;\n}\n\n.iconos li{\n    display: inline-block;\n    font-size: .8em;\n    text-align: center;\n    margin: 0 13px 0 13px;\n    margin-top: 20px;\n    margin-bottom: 50px;\n    cursor: pointer;\n}\n\n.Title{\n    text-align: center;\n}\n\nol{\n    padding: 0\n}\n\nspan{\n    /*display: none;*/\n}\n\n.previous {\n    font-size: 0.9em !important;\n    padding: 80px 0;\n}\n\n.previous .Objeto{\n    text-align: center;\n}\n\n.MainContainer{\n    z-index: 1000;\n    padding: 10px 0;\n}\n\n.MainContainer .Objeto {\n     width: 100%;\n     text-align: center;\n}\n\n.MainContainer .Credito{\n    height:300px;\n    margin-bottom: 30px;\n}\n\n.Objeto{\n    border-radius: 5px;\n    padding-bottom: 15px;\n}\n\n.Objeto h1{\n    text-align: center;\n    color: white;\n    margin-top: 5px;\n    margin-bottom: 0;\n    font-size: 1em;\n}\n\n.Objeto a{\n    text-align: center;\n    color: white;\n    font-size: 1em;\n    margin-bottom: 5px;\n}\n\n.Objeto img {\n    padding: 5px;\n    border-radius: 5px;\n    \n}\n\n.Credito{\n    background-color: transparent !important;\n    border: solid 2px;\n    padding: 10px;\n    margin-top: -15px;\n    border-radius: 5px;\n}\n\n.Credito h1{\n    font-size: 0.7em;\n    margin-top: 10px;\n    margin-bottom: 0;\n}\n\n.Credito a{\n    font-size: 0.7em;\n}\n\n.Slider .LibDes{\n    background-color: #F0C730;\n    border-color: #F0C730;\n}\n\n.Slider .LibInv{\n    background-color: #5698AA;\n    border-color: #5698AA;\n}\n\n.Slider .Viv{\n    background-color: #CC4229;\n    border-color: #CC4229;\n}\n\n.Slider .Estu{\n    background-color: #28487E;\n    border-color: #28487E;\n}\n\n.Slider .Vinc{\n    background-color: #BFCA43;\n    border-color: #BFCA43;\n}\n\n.Slider .Antic{\n    background-color: #E0992F;\n    border-color: #E0992F;\n}\n\n.Slider .Promo{\n    background-color: #6E2562;\n    border-color: #6E2562;\n}\n\n.contenedor .BackBanner{\n    left: 15px;\n    background-color: rgb(100 100 100);\n    border-radius: 150px;\n}\n\n.contenedor .NextBanner{\n    right: 15px;\n    background-color: rgb(100 100 100);\n    border-radius: 150px;\n}\n\n@media screen and (max-width: 39.9375em) {\n    .Formularios li{\n        width: 109px;\n        font-size: 0.5rem;\n        margin: 0.5rem 0.3rem 1rem;\n    }\n\n    .Formularios li .texto{\n        font-size: 0.6rem;\n    }\n\n}"

/***/ }),

/***/ "./src/app/vinculate/vinculate.component.html":
/*!****************************************************!*\
  !*** ./src/app/vinculate/vinculate.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"pageContent\">\n\t<div class=\"L-10 M-10 S-10 row pageBanner\">\n\t\t<div class=\"L-10 M-8 S-12 columns imagenBanner Vinculate\" appImagesSizesBanner>\n\t\t\t<img src=\"./assets/img/BanVincul.jpg\">\n\t\t\t<div class=\"PageTitle\"><h2>Vincúlate</h2><h3>y disfruta de todos nuestros beneficios</h3></div>\n\t\t</div>\t\n\t\t<div class=\"L-2 M-2 S-12 columns bannerBotones\">\n\t\t\t<a class=\"L-12 M-12 S-6 columns OfVirtual\" href=\"https://servicios2.inube.com.co/linix/v6/860006643/login.php?nit=860006643\" target=\"_blank\">\n\t\t\t\t<div class=\"icono\"><img src=\"./assets/img/OfVirtIcon.png\"></div>\n\t\t\t\t<div class=\"texto\"><b>Oficina<b class=\"Resaltar\"> virtual</b></b></div>\n\t\t\t</a>\n\t\t\t<div class=\"L-12 M-12 S-6 columns Vinculate\" [routerLink]=\"['/vinculate']\">\n\t\t\t\t<div class=\"icono\"><img src=\"./assets/img/vinculIcon.png\"></div>\n\t\t\t\t<div class=\"texto\"><b>Vincúlate<b class=\"Resaltar\"> ahora</b></b></div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t<div class=\"L-10 M-10 S-10 row pageContainer\">\n\t\t<div>\n\t\t\t<div class=\"Icon VinculateInfo\"></div>\n\t\t\t<div class=\"Title VinculateInfo\"><h1>Información institucional</h1></div>\n\t\t</div>\n\t\t<ng-container *ngFor=\"let info of informacion_institucional\">\n\t\t\t<div class=\"L-12 M-12 S-12 columns insideContent\">\n\t\t\t\t<img class=\"L-3 M-4 S-12 columns aaa\" [src]=\"info.image.url\" alt=\"\">\n\n\t\t\t\t<div class=\"L-9 M-8 S-12 texto\">\n\t\t\t\t\t<div [innerHTML]=\"info.description | safe:'html'\"></div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</ng-container>\n\t\t\n\t\t<div class=\"L-12 M-12 S-12 columns centered Formularios\">\n\t\t\t<ul>\n\t\t\t\t<li *ngFor=\"let item of formularios\" >\n\t\t\t\t<ng-container [ngSwitch]=\"item.router\">\n\t\t\t\t\t<a [href]=\"item.Link\"  *ngSwitchCase=\"'link'\" target=\"_blank\">\n\t\t\t\t\t\t<div class=\"BgFormulario\"></div>\n\t\t\t\t\t\t<div class=\"texto\">{{item.name}}</div>\t\n\t\t\t\t\t</a>\n\t\t\t\t\t<a [routerLink]=\"[item.Link]\"  *ngSwitchCase=\"'internal'\">\n\t\t\t\t\t\t<div class=\"BgFormulario\"></div>\n\t\t\t\t\t\t<div class=\"texto\">{{item.name}}</div>\t\n\t\t\t\t\t</a>\n\t\t\t\t</ng-container>\n\t\t\t\t\t\n\t\t\t\t</li>\n\t\t\t</ul>\n\t\t</div>\t\n\t</div>\n\t<div class=\"L-10 M-10 S-10 row pageContainer\">\n\t\t<div>\n\t\t\t<div class=\"Icon VinculateTasas\"></div>\n\t\t\t<div class=\"Title VinculateTasas\"><h1>Tasas y Tarifas</h1></div>\n\t\t\t<div class=\"TextTitle\"><a>El proceso de afiliación al Fondo de Empleados no tendrá ningún costo asociado, tampoco lo tendrá la solicitud, evaluación, aprobación y desembolso de un crédito para un asociado ya que se encuentra cubierto en la tasa propia del crédito.</a></div>\n\t\t</div>\n\t\t<div class=\"L-12 M-12 S-12 columns insideContent\">\n\t\t\t<ng-container *ngFor=\"let tarifa of tarifas\">\n\t\t\t\t<img class=\"L-3 M-4 S-12 columns\" [src]=\"tarifa.image.url\" >\n\t\t\t\t<div class=\"L-9 M-8 S-12 texto\">\n\t\t\t\t\t<div [innerHTML]=\"tarifa.description | safe:'html'\"></div>\n\t\t\t\t</div>\n\t\t\t</ng-container>\n\t\t\t\n\t\t</div>\n\t\t<div class=\"L-10 M-10 S-12 row pageContainer\">\n\t\t<div class=\"L-12 M-12 S-12 row insideContent\">\n\t\t\t<div class=\"creditosTitle\">\n\t\t\t\t<div class=\"Title VinculateTasas\"><h1>Créditos</h1></div>\n\t\t\t\t<div class=\"TextTitle\"><a>Las tarifas y modalidades de créditos vigentes ofrecidos por el Fondo de Empleados son las siguientes:</a></div>\n\t\t\t</div>\n\t\t\t<app-creditos-slider *ngIf=\"data\" [data]=\"data\"></app-creditos-slider>\n\t\t\n\t\t</div>\n\t</div>\n\t</div>\n</div>\t"

/***/ }),

/***/ "./src/app/vinculate/vinculate.component.ts":
/*!**************************************************!*\
  !*** ./src/app/vinculate/vinculate.component.ts ***!
  \**************************************************/
/*! exports provided: VinculateComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VinculateComponent", function() { return VinculateComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_service_manager_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/service-manager.service */ "./src/app/services/service-manager.service.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var VinculateComponent = /** @class */ (function () {
    function VinculateComponent(service) {
        this.service = service;
        this.BackPosition = 7;
        this.Position = 0;
        this.FrontPosition = this.Position + 1;
        this.arrClases = [];
    }
    VinculateComponent.prototype.ChangePosition = function (ev) {
    };
    VinculateComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.service.getDocuments()
            .subscribe(function (data) {
            console.log(data);
            _this.formularios = data.documents.map(function (link) {
                link.Link = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].URL_SERVER + link.file.url;
                link.router = 'link';
                return link;
            });
            _this.formularios.unshift({
                "id": 1,
                "name": "Formulario de afiliación o actualización de datos",
                "Link": "/formulario",
                "router": "internal"
            });
            console.log(_this.formularios);
        });
        this.service.getVinculate()
            .subscribe(function (data) {
            _this.informacion_institucional = data.informacion;
            _this.tarifas = data.tarifas;
            _this.informacion_institucional = data.informacion.map(function (item) {
                item.image.url = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].URL_SERVER + item.image.url;
                item.image.thumb.url = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].URL_SERVER + item.image.thumb.url;
                return item;
            });
            _this.tarifas = data.tarifas.map(function (item) {
                item.image.url = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].URL_SERVER + item.image.url;
                item.image.thumb.url = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].URL_SERVER + item.image.thumb.url;
                return item;
            });
            _this.arrClases = data.tipo_creditos.map(function (item, i) {
                item.key = 'item_' + (i + 1);
                item.image.url = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].URL_SERVER + item.image.url;
                return item;
            });
            console.log(_this.arrClases);
            if (_this.arrClases.length > 0) {
                _this.data = data.creditos.map(function (item, i) {
                    return {
                        "id": (i + 1),
                        "Titulo": item.name,
                        "TextIcon": item.name,
                        "Clase": _this.arrClases[i]['key'],
                        "Icon": _this.arrClases[i]['image']['url'],
                        "Picture": _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].URL_SERVER + item.image.url,
                        "Description": item.description
                    };
                });
                _this.SliderLength = _this.data.length;
                _this.BackPosition = _this.data.length - 1;
            }
        });
    };
    VinculateComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-vinculate',
            template: __webpack_require__(/*! ./vinculate.component.html */ "./src/app/vinculate/vinculate.component.html"),
            styles: [__webpack_require__(/*! ./vinculate.component.css */ "./src/app/vinculate/vinculate.component.css")],
        }),
        __metadata("design:paramtypes", [_services_service_manager_service__WEBPACK_IMPORTED_MODULE_1__["ServiceManagerService"]])
    ], VinculateComponent);
    return VinculateComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
/* harmony import */ var zone_js_dist_zone_error__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! zone.js/dist/zone-error */ "./node_modules/zone.js/dist/zone-error.js");
/* harmony import */ var zone_js_dist_zone_error__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(zone_js_dist_zone_error__WEBPACK_IMPORTED_MODULE_0__);

// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var URL_SERVER = "https://app.febdb.com.co";
//const URL_SERVER = "http://localhost:3000";
var environment = {
    production: false,
    URL_SERVER: URL_SERVER,
    API_ENDPOINT: URL_SERVER + "/api",
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Applications/MAMP/htdocs/fondo_empleados/FondoEmpleadosBDB/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map