const URL_SERVER = "http//:localhost:3000";

export const environment = {
  production: true,
  URL_SERVER: URL_SERVER,
  API_ENDPOINT: URL_SERVER + "/api/",
};