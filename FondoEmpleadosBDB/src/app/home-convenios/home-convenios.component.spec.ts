import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeConveniosComponent } from './home-convenios.component';

describe('HomeConveniosComponent', () => {
  let component: HomeConveniosComponent;
  let fixture: ComponentFixture<HomeConveniosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeConveniosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeConveniosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
