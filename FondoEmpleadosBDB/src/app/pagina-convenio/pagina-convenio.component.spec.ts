import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaginaConvenioComponent } from './pagina-convenio.component';

describe('PaginaConvenioComponent', () => {
  let component: PaginaConvenioComponent;
  let fixture: ComponentFixture<PaginaConvenioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaginaConvenioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginaConvenioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
