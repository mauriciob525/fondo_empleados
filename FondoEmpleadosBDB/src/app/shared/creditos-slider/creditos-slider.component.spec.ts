import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditosSliderComponent } from './creditos-slider.component';

describe('CreditosSliderComponent', () => {
  let component: CreditosSliderComponent;
  let fixture: ComponentFixture<CreditosSliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreditosSliderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditosSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
