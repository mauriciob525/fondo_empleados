import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeOpinionesComponent } from './home-opiniones.component';

describe('HomeOpinionesComponent', () => {
  let component: HomeOpinionesComponent;
  let fixture: ComponentFixture<HomeOpinionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeOpinionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeOpinionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
