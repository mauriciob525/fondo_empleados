import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VinculateComponent } from './vinculate.component';

describe('VinculateComponent', () => {
  let component: VinculateComponent;
  let fixture: ComponentFixture<VinculateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VinculateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VinculateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
