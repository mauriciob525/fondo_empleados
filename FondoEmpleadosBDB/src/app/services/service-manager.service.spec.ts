import { TestBed } from '@angular/core/testing';

import { ServiceManagerService } from './service-manager.service';

describe('ServiceManagerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ServiceManagerService = TestBed.get(ServiceManagerService);
    expect(service).toBeTruthy();
  });
});
